#!/usr/bin/python
# -*- coding: UTF-8 -*- 
#
#
#########################################################
# Name: ctrl_run.py
# Porpose: Check OS type and configuration files
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (01) 17/01/2015
#########################################################

"""
All controls for establish the operative systems type and controls 
for verify the integrity and changes on the configuration files of 
videomass and parsing for configuration file has centralized here
Also, include the functions for parsing presets xml and for delete 
profiles into the presets list.
"""

import sys
import os
import shutil
import platform
from xml.dom.minidom import parseString

"""
work current directory (where are Videomsass?):
"""
PWD = os.getcwd()

"""
current user directory:
"""
DIRNAME = os.path.expanduser('~') # /home/user


########################################################################
# CONTROLS CONFIGURATION FILE AND PARSINGS
########################################################################

def control_config():
    """
    This function is for check existing videomass.conf and .xml files 
    in ~/.videomass. It is called in Videomass.py. If these files do not 
    exist or are deleted in ~/, this function restore them from assign paths
    established below. Check which operating system is in use to make 
    changes necessary. On MacOSX is also created the directory that contains 
    the ffmpeg presets if not exist. Also, assign the paths of icons and html 
    user manual.
    
    """
    copyerr = False

    # What is OS ??
    if sys.platform.startswith('darwin'):
        OS = 'darwin'

    elif sys.platform.startswith('linux2'):
        OS = 'linux2'
        
    elif sys.platform.startswith('win'):
        OS = 'windows'
        
    else:
        OS = sys.platform
        #err = u"Sistema operativo non supportato, eseguibile di ffmpeg non trovato."
        #wx.MessageBox("%s" % (err), "ERROR", wx.ICON_ERROR | wx.STAY_ON_TOP)
        
        

    """
    Assignment path where there is av_profile.xml and videomass.conf
    This depends if portable mode or installable mode:
    """
    if os.path.exists('%s/vdms_GUI' % (PWD)) or sys.platform.startswith('darwin'):
        """
        This paths are for portable mode
        """
        path_srcShare = '%s/share' % PWD
        """
        assignment path at the used icons:
        """
        videomass_icon = "%s/art/videomass.png" % PWD
        icon_play = '%s/art/icon_play.png' % PWD
        icon_analyze = '%s/art/icon_analyze.png' % PWD
        icon_presets = '%s/art/icon_presets.png' % PWD
        #icon_eyes = '%s/art/tecnoeyes.png' % PWD
        icon_switchvideomass = '%s/art/icon_switchvideomass.png' % PWD
        icon_process = '%s/art/icon_process.png' % PWD
        icon_help = '%s/art/icon_help.png' % PWD
        """
        assignment path at the contestual help for helping:
        """
        help_html = "file://%s/docs/HTML" % PWD
        
    else:
        """
        This paths are for installable mode
        """
        path_srcShare = '/usr/share/videomass/config'

        """
        assignment path at the used icons:
        """
        videomass_icon = "/usr/share/pixmaps/videomass.png"
        icon_play = '/usr/share/videomass/icons/icon_play.png'
        icon_analyze = '/usr/share/videomass/icons/icon_analyze.png'
        icon_presets = '/usr/share/videomass/icons/icon_presets.png'
        #icon_eyes = '/usr/share/videomass/icons/tecnoeyes.png'
        icon_switchvideomass = '/usr/share/videomass/icons/icon_switchvideomass.png'
        icon_process = '/usr/share/videomass/icons/icon_process.png'
        icon_help = '/usr/share/videomass/icons/icon_help.png'
        """
        assignment path at the contestual help for helping.
        This change if Slackware, debian, etc.
        """
        if os.path.exists('/usr/doc/videomass/HTML'): # Slackware
            help_html = 'file:///usr/doc/videomass/HTML'
            
        elif os.path.exists('/usr/share/doc/python-videomass/HTML'):# debian
            help_html = 'file:///usr/share/doc/python-videomass/HTML'
            
        else:
            print 'ERROR LOAD HTML PAGE: path not found'

    if os.path.exists('%s/.videomass' % DIRNAME):

        if os.path.isfile('%s/.videomass/videomass.conf' % DIRNAME):
            pass
        else:
            shutil.copyfile('%s/videomass.conf' % path_srcShare, 
                            '%s/.videomass/videomass.conf' % DIRNAME)
    else:
        
        try: # if exist folder ~/.videomass
            shutil.copytree(path_srcShare, '%s/.videomass' % DIRNAME)
            
        except OSError:
            copyerr = True

    return (videomass_icon, icon_play, icon_analyze, icon_presets, 
            icon_switchvideomass ,icon_process, icon_help, help_html, 
            OS, path_srcShare, copyerr
            )
    
#------------------------------------------------------------------#
def parser_fileconf():
    """
    This function is called by videomass.py for parsing of the file 
    in '~/.videomass/videomass.conf' and return values of the settings. 
    Changes of videomass.conf file depends on the settings make on setup.
    When Videomass.py call other function that need some these values,
    it pass them.
    """
    
    try:
        # Open in read the videomass.conf file 
        fileconf = open('%s/.videomass/videomass.conf' % (DIRNAME),'r').readlines()

        """ 
        parse the configuration file and create the portions lists (slice).
        The slice have this order: 
        fileconf[number of row][number of column - \n]
        """
        lista = fileconf # create new list for event changes
        fileconf_version = '%s' % fileconf[21][0:-1]
        vdms_version = '%s' % fileconf[24][0:-1]
        threads = '%s' % fileconf[27][10:-1]
        ffmpeg_log = '%s' % fileconf[30][13:-1]
        command_log = '%s' % fileconf[33][14:-1]
        path_log = '%s' % fileconf[36][7:-1]
        loglevel_type = '%s' % fileconf[48][0:-1]
        loglevel_batch = '%s' % fileconf[51][0:-1]
        
        ffmpeg_check = '%s' % fileconf[61][14:-1]
        ffmpeg_link = '%s' % fileconf[63][0:-1]
        
        ffprobe_check = '%s' % fileconf[66][15:-1]
        ffprobe_link = '%s' % fileconf[68][0:-1]
        
        ffplay_check = '%s' % fileconf[71][14:-1]
        ffplay_link = '%s' % fileconf[73][0:-1]
        writeline_exec = '%s' % fileconf[76][13:-1]
        # se l'ultima riga non avesse la \n questo è lo slicing: [73][:]

        fileconf = open('%s/.videomass/videomass.conf' % (DIRNAME),'r').close()
        
    except IndexError:
        """
        when execute a new version of program and the videomass.conf file has 
        been change during development the previous version of the
        videomass.conf file is not more compatible
        """
        return 'corrupted_fileconf'

    return (threads, ffmpeg_log, command_log, path_log, loglevel_type, 
            loglevel_batch, ffmpeg_link, ffprobe_link, ffplay_link, 
            ffmpeg_check, ffprobe_check, ffplay_check, lista, vdms_version,
            fileconf_version, writeline_exec
            )
    
########################################################################
# PARSINGS XML FILES AND FUNCTION FOR DELETING
########################################################################

def parser_xml(arg): # arg é il nome del file xml (vdms) 
    """
    This function has call by Videomass.py which send the filename.vdms
    for make parsing and read the presets memorized on this files.
    The presets are the files xml with extension vdms.
    The (arg) argument is a name of the vdms file to parse. The imported
    module need for parsing xml files used here is: xml.dom.minidom .
    It return a value consisting of dictionaries containing other dictionaries 
    in the form:
    {name: {filesupport:?, descript:?, command:?, extens:?}, else: {etc etc}}
    
    """
    File = open('%s/.videomass/%s.vdms' %(DIRNAME, arg),'r')
    datos = File.read()
    File.close()

    parser = parseString(datos) # fa il parsing del file xml ed esce: 
                            # <xml.dom.minidom.Document instance at 0x8141eec>
    dati = dict() 
    for presets in parser.getElementsByTagName("presets"):
        for preset in presets.getElementsByTagName("label"):
            name = preset.getAttribute("name")
            types = preset.getAttribute("type")
            parameters = None
            filesupport = None
            extension = None
            
            for presets in preset.getElementsByTagName("parameters"):
                for parameters in presets.childNodes:
                    params = parameters.data
                    
            for presets in preset.getElementsByTagName("filesupport"):
                for filesupport in presets.childNodes:
                    support = filesupport.data

            for presets in preset.getElementsByTagName("extension"):
                for extension in presets.childNodes:
                    ext = extension.data

            dati[name] = { "type": types, "parametri": params, 
                                    "filesupport": support, "estensione": ext }
    #return dati[name]

    return dati
    

#------------------------------------------------------------------#

def delete_profiles(array, filename):
    """
    Funzione usata nel modulo Videomass.py per cancellare singole voci 
    (i profili) dei presets selezionati nella listcontrol.
    
    dati é il dizionario iterabile passato dal modulo os_processing.parser_xml 
    (parsa il file xml) é composto da dizionari contenente altri dizionari 
    nella forma {name:{filesupport:?, descript:?, command:?, extens:?}, altro:{
    etc etc}
    
    array é solo una chiave del diz messa in lista e contiene: 
    name descr command  support ext 
    cioé 5 elementi. Array viene creata in Videomass.py nella funzione 
    def on_select()
    
    """
    
    dati = parser_xml(filename)
    #dirconf = os.path.expanduser('~/.videomass/%s.vdms' % (filename))
    dirconf = '%s/.videomass/%s.vdms' % (DIRNAME, filename)
    
    """
    Posso anche usare i dizionari al posto degli indici lista (riga 335 circa)
    (ho deciso per gli indici lista in questa versione), es: 
    
    #name_preset = array[0].encode("utf-8")
    #description = param["type"].encode("utf-8")
    #commands_ffmpeg = param["parametri"].encode("utf-8")  # not more use
    #file_support = param["filesupport"].encode("utf-8")   # not more use
    #file_extension = param["estensione"].encode("utf-8")  # not more use
    
    """
    
    param = dati[array[0]] # da il dizionario completo

    name_preset = array[0].encode("utf-8")
    description = array[1].encode("utf-8")

    """
    I can use 'with open(...)' or 'fileconf = open(...)' but i prefer open,
    put in list and close the file:
    """
    #fileconf = open('%s' % (dirconf),'r').readlines()
    data = open('%s' % (dirconf),'r')
    fileconf = data.readlines()
    data.close()
    
    """
    Nella versione precedente, usavo il sistema con il metodo .index() ma 
    mi causava degli errori. Non trova title=fileconf.index a volte (Vedi il 
    changelog) per errori di occorrenze.
    
    SOLUZIONE:
    title + 1 (oppure indice +1 -vedi sotto-) avanza sempre di uno rispetto a
    title, e sempre solo a quello. Questa soluzione dovrebbe impedire di 
    eliminare identiche occorrenze tipo <filesupport> </filesupport> nella 
    ricerca in fileconf
    
    """
    title = '    <label name="%s" type="%s">\n' % (name_preset, description)
    
    for indice, riga in enumerate(fileconf): # mi da l'indice (come index)
                                            # ma anche la stringa in riga
        if title in riga:
            
            for i in range(0,4): # ripete la stessa esecuzione per 4 volte
                del fileconf[indice + 1]
            # il for sopra, mi serializza l'esecuzione di 'del' per 4 volte.
            # Fa la stessa cosa del codice fra i trattini qui sotto ma é più 
            # elegante:
            #----------------------------------------------#
            #del fileconf [indice + 1] # label iniziale
            #del fileconf [indice + 1] # command ffmpeg
            #del fileconf [indice + 1] # file support
            #del fileconf [indice + 1] # estensione
            #----------------------------------------------#

            if fileconf [indice + 1] == "\n":
                del fileconf [indice + 1] # lo spazio
                
            del fileconf [indice] # per ultimo cancello il name e type
            
    #### TODO: c'é sempre una linea in più quando cancelli tutti i profili
    # questo non influisce sulla stabilità ma solo sulla formattazione del
    # testo. L'occhio vuole la sua parte!

    open('%s' % (dirconf),'w').writelines(fileconf)
