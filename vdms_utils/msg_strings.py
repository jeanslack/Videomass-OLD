#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# creation date: 30/04/2015
#
#########################################################
# Name: msg_string.py
# Porpose: Strings organization
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015-2017 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
#########################################################

def out_version_err(path_confdir):
    """
    Doc String file for update configuration msg 
    """
    
    msg = (u"""A new version of the catalog presets is
to be updated. This should be of interest
to all files with '.vdms' extension
localized in:  '%s'

Want you update automatically now ?
..You must restart Videomass after

WARNING: You may need to make a backup
of your data save stored on that files 
before continuing.
""" % path_confdir)
    
    return msg

def incompatible_fileconf(path_confdir):
    """
    Doc String file conf error msg 
    """
    
    msg = (u"""It appears that the 'videomass.conf' file
localized in:  '%s'

is not compatible with this version of Videomass.
These drawbacks can be encountered after
installing another version of Videomass and
can be resolved by replacing the 'videomass.conf'

Want you this replaced is performed
automatically now?
..You must restart Videomass after

WARNING: you'll need to reset your personal 
Videomass settings.
""" % path_confdir)
    
    return msg

def corrupted_fileconf(path_confdir):
    """
    Doc String file conf error msg 
    """
    
    msg = (u"""The folder for Videomass configuration,
localized in:  '%s'

contains damaged files.

Want you this restore is performed
automatically now?
..You must be restart Videomass after

WARNING: you'll need to reset your personal 
Videomass settings.
""" % path_confdir)
    
    return msg
