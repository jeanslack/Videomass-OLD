
Videomass 
=========

## Warning:
**This repository has been archived as an old consultation project.**   
If you are looking for new versions of Videomass2 and Videomass3, 
go to the links below:   
    
<https://github.com/jeanslack/Videomass2>   

<https://github.com/jeanslack/Videomass3>   

----------------------------------------------------   

* [Overview](#overview)
* [Official WebPage](http://jeanslack.github.io/Videomass)
* [Downloads](https://github.com/jeanslack/Videomass/releases)
* [System Requirements](#system-requirements)
* [Essential Dependencies](#essential-dependencies)
* [Use](#use)
* [Build Package](#build-package)
* [Make a debian packages](#make-a-debian-packages)
* [Make a Slackware package](#make-a-slackware-package)
* [OSX](#osx)
* [License](#license)

## Overview

**Videomass** is a cross-platform GUI for FFmpeg, simple and without too much 
pretense, it developed in wxPython toolkit. The videomass goal is to provide a 
functional interface for organizing your own ffmpeg profiles through presets 
management. You can import or save new presets with your personal profiles, 
delete, modify or restore new profiles.

## System Requirements

* Gnu/Linux
* OSX 10.7 or later

There is not yet a Windows implementation

## Essential Dependencies

**Required:**
- Python >= 2.6 (no python >= 3)   
- wxPython >= 2.8   

**Extra required:**
- ffmpeg >= 2.1 
- ffprobe (for multimedia streams analysis) (can be built-in into ffmpeg)
- ffplay (media player for media preview) (can be built-in into ffmpeg)

**Optionals:**
- libx264 (has to be explicitly enabled when compiling ffmpeg)
- libmp3lame (has to be explicitly enabled when compiling ffmpeg)
- libfdk-aac (has to be explicitly enabled when compiling ffmpeg)
- libfdk-aac (has to be explicitly enabled when compiling ffmpeg)
- xvidcore (has to be explicitly enabled when compiling ffmpeg)
- libvpx (has to be explicitly enabled when compiling ffmpeg)
- libvorbis (has to be explicitly enabled when compiling ffmpeg)
- wavpack (has to be explicitly enabled when compiling ffmpeg)

## Use

To start **videomass** with not installation, simply run the "videomass" file into 
unzipped sources folder, like this: `~$ python videomass` or `~$ ./videomass`. 
Also, try with click at videomass file with your mouse. In any case, be sure to check 
the execution permissions first. 

## Build Package

For building a redistributable package, using setup.py script in the sources folder.
There are several way to use it, this depends on the operating system used since each 
operating system has its own packaging tools. However, the common need tools and useful 
for simple python distibutions are *distutils* and *setuptools*.

- **distutils** is still the standard tool for packaging in python. It is included in the
standard library that can be easily imported.   

- **setuptools** which is not included in the standard library, must be
separately installed if not present.

## Make a debian packages

If you want make a *.deb* binary package installable in your debian system and 
compatible with others debian-based systems (with same version), you need install 
those following tools: `~# apt-get update && apt-get install python-all python-stdeb fakeroot`. 
This installs all the need dependencies, including python-setuptools.   

Then, go into Videomass unzipped folder with your user (not root) and type: 
`~$ python setup.py --command-packages=stdeb.command bdist_deb`. 
This should create a *python-videomass_version_all.deb* in the new deb_dist directory, 
installable with `~# dpkg -i python-videomass_version_all.deb` command.

See also [setup.py](https://github.com/jeanslack/Videomass/blob/master/setup.py) 
script for insights.

## Make a Slackware package

Is available a SlackBuild script to build a package *.tgz* for Slackware and Slackware based 
distributions. 
See here [videomass.SlackBuild](https://github.com/jeanslack/slackbuilds/tree/master/Videomass)

Remember: first install **pysetuptools** before proceed to build Videomass, if not present.
You can search on this site: [SlackBuild.org](http://slackbuilds.org/repository/14.1/python/pysetuptools/)

## OSX

**Common Installation with a dmg image:**

If you want install Videomass in your system, you can get the **dmg** file installer,
mount dmg image and drag the Videomass.app to the Applications folder. This installation 
includes everything you need to start the application.

This require: Mac OsX with intel 64 bit processors (Snow-Leopard, Lion and Mountain-Lion OS) 
There is no support for Power PC processors (Tiger, Leopard OS) in the current App.

You can download the dmg file at: [Videomass_OsX_Intel_x86_64.dmg](https://www.dropbox.com/sh/i0mkx4a5hx6yu0u/AADC-KMT4S-9DzNj9tCah29Fa?dl=0)

**As portable application (Run the sources code):**

You can run Videomass without install it, but this require extra installations in your system:

* Python 2.7.? (from hombrew)
* WxPthon 3.0 (from homebrew)
* ffmpeg >= 2.1 (compiled with support x264, lame, faac, vp8(libvpx), xvid, with both ffprobe and ffplay enables).

Then, download the Videomass tarball source code:   

<https://github.com/jeanslack/Videomass/releases> and see [Use](#use)   

However, ffmpeg, ffprobe, ffplay must be linked directly into setting dialog of videomass. 
Then, download ffmpeg, ffprobe and ffplay and put them into some folder of your home 
(or you make a link at installed bin to same folder). You can download them at the following 
addresses (note that does not include support for AAC) and they were compiled with static mode 
for Mac OS X Intel 64bit:

<http://www.evermeet.cx/ffmpeg/>   
<http://www.evermeet.cx/ffprobe/>   
<http://www.evermeet.cx/ffplay/>

**Build a OSX App**

For build the Videomass.app there you need Xcode and command-line-tools available to the app store. 
I have tested the building app with the following requirements:

* Mac book pro 2 Ghz intel core i7 with Mac OSX 10.7.5 Lion 64bit
* Xcode 4.6.3
* Command-line-tools
* Python 2.7.8 from hombrew
* WxPython 3.0 from homebrew
* ffmpeg >= 2.1 (with both ffprobe and ffplay enables)
* Py2app installed with pip tools (sudo pip install py2app)

See the comments in the script setup.py for more information.

After verify the require components we can go below:

Open a terminal in the path where is setup.py and run the script with: `python setup.py py2app` 
if there are no errors, go to the dist folder and launch the application with a double-click or 
move the app in the *Applications* folder.

## License

Copyright © 2013 - 2017 by jeanslack   
Author and Developer: Gianluca Pernigotto   
Mail: <jeanlucperni@gmail.com>   
License: GPL3 (see LICENSE file in the docs folder)
