#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# First release: Monday, July 7 10:00:47 2014
# 
#########################################################
# Name: setup.py
# Porpose: script for building Videomass.app and package for install.
# Platform: Mac OsX, Gnu/Linux
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2014-2017 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (01) September 24 2014
# Rev (02) January 21 2015
# Rev (03) May 04 2015
# Rev (03) Nov 10 2017
#########################################################

from distutils.core import setup
from setuptools import setup
import platform
from glob import glob
import sys
import os
import shutil
from vdms_utils.msg_info import current_release, descriptions_release

cr = current_release()
RLS_NAME = cr[0] # release name first letter is Uppercase
PRG_NAME = cr[1]
VERSION = cr[2]
RELEASE = cr[3]
COPYRIGHT = cr[4]
WEBSITE = cr[5]
AUTHOR = cr[6]
EMAIL = cr[7]
COMMENT = cr[8]

dr = descriptions_release()
LICENSE = dr[2] # short license
DESCRIPTION = dr[0]
LONG_DESCRIPTION = dr[1]

OSX_CLASSIFIERS = [
            'Development Status :: %s' % (VERSION),
            'Environment :: Graphic',
            'Environment :: MacOS X :: Cocoa',
            'Intended Audience :: Users',
            'License :: %s' %(LICENSE),
            'Natural Language :: Italian',
            'Operating System :: MacOS :: MacOS X',
            'Programming Language :: Python',
                    ]


def glob_files(pattern):
    """
    this is a simple function for globbing that iterate 
    for list files in dir
    """
    
    return [f for f in glob(pattern) if os.path.isfile(f)]



def LINUX_SLACKWARE(id_distro, id_version):
    """
    ------------------------------------------------
    setup of building package for Slackware
    ------------------------------------------------
    
    REQUIRED TOOLS: 
    pysetuptools
    
    USAGE: 
    Use in a SlackBuild combination
    """
    setup(name = PRG_NAME,
        version = VERSION,
        description = DESCRIPTION,
        long_description = LONG_DESCRIPTION,
        author = AUTHOR,
        author_email = EMAIL,
        url = WEBSITE,
        license = LICENSE,
        platforms = ['Gnu/Linux (%s %s)' % (id_distro, id_version)],
        packages = ['vdms_GUI','vdms_utils'],
        scripts = ['videomass']
        )

def LINUX_DEBIAN_UBUNTU(id_distro, id_version):
    """
    ------------------------------------------------
    setup of building package for Debian based distro
    ------------------------------------------------
    
    TOOLS: 
    apt-get install python-all python-stdeb fakeroot

    USAGE: 
    - for generate both source and binary packages :
        python setup.py --command-packages=stdeb.command bdist_deb
        
    - Or you can generate source packages only :
        python setup.py --command-packages=stdeb.command sdist_dsc
        
    RESOURCES:
    - look at there for major info:
        [https://pypi.python.org/pypi/stdeb]
        [http://shallowsky.com/blog/programming/python-debian-
         packages-w-stdeb.html]
    """
    
    # this is DATA_FILE structure: 
    # ('dir/file destination of the data', ['dir/file on current place sources']
    # even path must be relative-path
    DATA_FILES = [
        ('share/videomass/config', glob_files('share/*.vdms')),
        ('share/videomass/config', ['share/videomass.conf', 'share/README']),
        ('share/videomass/icons', glob_files('art/*.png')),
        ('share/applications', ['videomass.desktop']),
        ('share/pixmaps', ['art/videomass.png']),
        ('share/doc/python-videomass/HTML', glob_files('docs/HTML/*.html')),
                ]
    
    DEPENDENCIES = ['python', 'wxpython']
    EXTRA_DEPEND = {'ffmpeg':  ["ffmpeg"],}
    
    setup(name = PRG_NAME,
        version = VERSION,
        description = DESCRIPTION,
        long_description = LONG_DESCRIPTION,
        author = AUTHOR,
        author_email = EMAIL,
        url = WEBSITE,
        license = LICENSE,
        platforms = ['Gnu/Linux (%s %s)' % (id_distro, id_version)],
        packages = ['vdms_GUI','vdms_utils'],
        scripts = ['videomass'],
        data_files = DATA_FILES,
        install_requires = DEPENDENCIES,
        extras_require = EXTRA_DEPEND
        )
    
        
def OSX():
    """
    ------------------------------------------------
    py2app build script for videomass.py
    ------------------------------------------------
    -Usage for look options:
        python setup.py py2app --help

    -Usage for development and debug:
        python setup.py py2app -A
    for debug with terminal use: 
        ./dist/Videomass.app/Contents/MacOS/videomass

    -Usage for building a redistributable version standalone:
        python setup.py py2app

    -look at there for major info:
    http://pythonhosted.org/py2app/tutorial.html#building-for-deployment

                UTILS:
    ---------------------------------------------
    exemple for find all file and dir in a path:
    ---------------------------------------------
    [http://stackoverflow.com/questions/3207219/how-to-list-all-files-
     of-a-directory-in-python]
    os.listdir("/somedirectory/path")
    
    ---------------------------------------------
    Exemple for Find the executable:
    ---------------------------------------------
    sudo find / -name "py2applet" -type f
    
    -------------------------------------------------------
    Exemple for use py2applet (crea un modello di setup.py)
    -------------------------------------------------------
    in my machine the executable path place in:
    PATH_PY2APPLET = '/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/bin/py2applet'  
    PATH_PY2APPLET --make-setup videomass.py --iconfile 'videomass.icns'
    if you want, add its parent directory to your PATH environment variable:
    export PATH=$PATH:PATH_PY2APPLET

    """
    
    import py2app

    PWD = os.getcwd() # current work directory path
        
    PATH_ICON = '%s/videomass.icns' % PWD

    """
    this is DATA_FILE structure: 
    ('dir/file') > destination of the data, ['dir/file'] > on current 
    place sources even path must be relative-path
    """
    DATA_FILES = [('share', glob_files('share/*.vdms')),
            ('share', ['share/videomass.conf']), 
            ('docs/HTML', glob_files('docs/HTML/*.html')), 
            #('art', ['art/icon_analyze.png', 'art/icon_play.png', 
            #'art/icon_process.png', 'art/icon_help.png',
            #'art/icon_switchvideomass.png']),
            ('art', glob_files('art/*.png')), 
            ('', ['AUTHORS','BUGS','CHANGELOG','INSTALL','COPYING','TODO',
                  'README.md']),]

    #--------------- This is setup: --------------------#
    
    if os.path.exists('%s/Videomass.py' % PWD):
        pass
    else:
        os.rename("videomass","Videomass.py")
        #shutil.copyfile('%s/videomass' % PWD, '%s/Videomass.py' % PWD)
    
    setup(app = ['Videomass.py'],
        name = RLS_NAME,
        version = VERSION,
        options = dict(py2app = dict(argv_emulation = 0, iconfile = PATH_ICON,)),
        description = DESCRIPTION,
        long_description = LONG_DESCRIPTION,
        classifiers = OSX_CLASSIFIERS,
        author = AUTHOR,
        author_email = EMAIL,
        url = WEBSITE,
        license = LICENSE,
        data_files =  DATA_FILES,
        platforms=['MacOS X'],
        setup_requires = ["py2app"],
        )
    
##################################################

if sys.platform.startswith('darwin'):
    
    OSX()
    
elif sys.platform.startswith('linux2'):
    
    dist_name = platform.linux_distribution()[0]
    dist_version = platform.linux_distribution()[1]
    
    if dist_name == 'Slackware ':
        LINUX_SLACKWARE(dist_name, dist_version)
        
    elif dist_name == 'debian' or dist_name == 'Ubuntu':
        LINUX_DEBIAN_UBUNTU(dist_name, dist_version)
        
    else:
        print 'this platform is not yet implemented: %s %s' %(dist_name, 
															  dist_version)
        

else:
    print 'OS not supported'
###################################################
