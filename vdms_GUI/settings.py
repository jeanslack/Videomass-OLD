#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Tue Aug 27 14:49:44 2013
#
#########################################################
# Name: settings.py
# Porpose: setup videomass for conversions
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (03) 20/07/2014
# Rev (04) 12/01/2015
#########################################################
"""
Settings is called from videomass and arraymass. It write on the 
configuration file locate in home at ~/.videomass/videomass.conf 
and come parsing by os_processing parser_fileconf(). The first to
read is videomass than pass all parameters to others modules.
"""
import wx
import os

dirname = os.path.expanduser('~/') # /home/user/

class setting(wx.Dialog):
    """
    The main settings dialog. The __init__ constructor arguments are 
    passed by videomass or arraymass by calling this istance
    """
    def __init__(self, parent, threads, ffmpeg_log, command_log, path_log, 
                 ffmpeg_link, ffmpeg_check, ffprobe_link, ffprobe_check, 
                 ffplay_link, ffplay_check, full_list, writeline_exec):
        
        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        
        self.threads = threads
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.ffmpeg_link = ffmpeg_link
        self.ffmpeg_check = ffmpeg_check
        self.ffprobe_link = ffprobe_link
        self.ffprobe_check = ffprobe_check
        self.ffplay_link = ffplay_link
        self.ffplay_check = ffplay_check
        self.full_list = full_list
        self.writeline_exec = writeline_exec
        
        ntbk = wx.Notebook(self, wx.ID_ANY)
        ntbk_pane_1 = wx.Panel(ntbk, wx.ID_ANY)
        lab1_pane1 = wx.StaticText(ntbk_pane_1, wx.ID_ANY, (
                                "Click the button below for more info:"))
        btn_threads = wx.Button(ntbk_pane_1, wx.ID_ANY, "%s" % threads[9:])
        lab2_pane1 = wx.StaticText(ntbk_pane_1, wx.ID_ANY, (
                                         "Sets a new number of CPU threads:"))
        self.spinctrl_threads = wx.SpinCtrl(ntbk_pane_1, wx.ID_ANY, 
                                            "%s" % threads[9:], min=1, max=32, 
                                            style=wx.TE_PROCESS_ENTER
                                             )
        Boxsiz1_pane1_staticbox = wx.StaticBox(ntbk_pane_1, 
                                                        wx.ID_ANY, (
                                                   "Settings CPU Threads:"))
        self.check_cmdline = wx.CheckBox(ntbk_pane_1, wx.ID_ANY, (
            "Enable text writing in the command line reading tab\n"
            "Warning! use this function at your own risk."))
        Boxsiz2_pane1_staticbox = wx.StaticBox(ntbk_pane_1, 
                                             wx.ID_ANY, ("Other Settings:"))
        ntbk_pane_2 = wx.Panel(ntbk, wx.ID_ANY)
        self.check_ffmpeglog = wx.CheckBox(ntbk_pane_2, wx.ID_ANY, 
                                           ("   Generates ffmpeg log files "))
        self.check_cmdlog = wx.CheckBox(ntbk_pane_2, wx.ID_ANY, 
                                        ("   Save Videomass log commands"))
        lab3_pane2 = wx.StaticText(ntbk_pane_2, wx.ID_ANY, 
                               ("Where do you want to save the log commands?"))
        self.btn_log = wx.Button(ntbk_pane_2, wx.ID_SAVE, "")
        self.txt_pathlog = wx.TextCtrl(ntbk_pane_2, wx.ID_ANY, "")
        ntbk_pane3 = wx.Panel(ntbk, wx.ID_ANY)
        self.checkbox_exeffmpeg = wx.CheckBox(ntbk_pane3, wx.ID_ANY,(
                                       " Use a custom path to run FFmpeg"))
        self.btn_pathFFmpeg = wx.Button(ntbk_pane3, wx.ID_ANY, "Import")
        self.textctrl_path_ffmpeg = wx.TextCtrl(ntbk_pane3, wx.ID_ANY, "")
        self.checkbox_exeFFprobe = wx.CheckBox(ntbk_pane3, wx.ID_ANY, (
                                       " Use a custom path to run FFprobe"))
        self.btn_pathFFprobe = wx.Button(ntbk_pane3, wx.ID_ANY, "Import")
        self.textctrl_path_ffprobe = wx.TextCtrl(ntbk_pane3, wx.ID_ANY, "")
        self.checkbox_exeFFplay = wx.CheckBox(ntbk_pane3, wx.ID_ANY, (
                                       " Use a custom path to run FFplay"))
        self.btn_pathFFplay = wx.Button(ntbk_pane3, wx.ID_ANY, "Import")
        self.textctrl_path_ffplay = wx.TextCtrl(ntbk_pane3, wx.ID_ANY, "")
        btn_close = wx.Button(self, wx.ID_CANCEL, "Close")
        btn_ok = wx.Button(self, wx.ID_APPLY, "")

        #----------------------Properties----------------------#
        self.SetTitle("Setup - Videomass")
        btn_threads.SetMinSize((40, 27))
        btn_threads.SetForegroundColour(wx.Colour(255, 63, 21))
        btn_threads.SetToolTipString("View the number of threads of the cpu"
                                          )
        self.spinctrl_threads.SetToolTipString("Set here a new number of CPU threads"
                                               )
        self.check_cmdline.SetToolTipString("Allows the text writing to "
                                            "performs your custom parameters."
                                            )
        self.check_ffmpeglog.SetToolTipString("At each conversion process, is "
                              "also generated a log file of ffmpeg. Note that "
                              "can reach considerable size. This feature is "
                              "disabled in batch processes."
                                              )
        self.check_cmdlog.SetToolTipString("Generates a log file command in "
                              "the directory specified below. A log of the "
                              "command consists of a file containing the "
                              "parameters of the execution process."
                                           )
        self.btn_log.SetToolTipString("Open Path")
        self.txt_pathlog.SetMinSize((250, 21))
        self.txt_pathlog.SetToolTipString("Path generation file")
        
        self.checkbox_exeffmpeg.SetToolTipString("Enable custom search for "
                       "the executable FFmpeg. If the checkbox is disabled or "
                       "if the path field is empty, the search of the "
                       "executable is entrusted to the system.")
        
        self.btn_pathFFmpeg.SetToolTipString("Open path FFmpeg")
        self.textctrl_path_ffmpeg.SetMinSize((250, 21))
        self.textctrl_path_ffmpeg.SetToolTipString(
                                    "path to executable binary FFmpeg"
                                                    )
        self.checkbox_exeFFprobe.SetToolTipString("Path generation file")
        
        self.checkbox_exeffmpeg.SetToolTipString("Enable custom search for "
                        "the executable FFprobe. If the checkbox is disabled or "
                        "if the path field is empty, the search of the "
                        "executable is entrusted to the system.")
        
        self.btn_pathFFprobe.SetToolTipString("Open path FFprobe")
        self.textctrl_path_ffprobe.SetMinSize((250, 21))
        self.textctrl_path_ffprobe.SetToolTipString(
                                    "path to executable binary FFprobe"
                                                    )
        self.checkbox_exeFFplay.SetToolTipString("Path generation file"
                                                 )
        self.checkbox_exeffmpeg.SetToolTipString("Enable custom search for "
                        "the executable FFplay. If the checkbox is disabled or "
                        "if the path field is empty, the search of the "
                        "executable is entrusted to the system."
                                                  )
        self.btn_pathFFplay.SetToolTipString("Open path FFplay")
        self.textctrl_path_ffplay.SetMinSize((250, 21))
        self.textctrl_path_ffplay.SetToolTipString(
                                     "path to executable binary FFplay"
                                                    )
        
        #----------------------Handle Layout----------------------#
        sizer_main = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_main = wx.FlexGridSizer(2, 1, 0, 0)
        GridsizBottom = wx.GridSizer(1, 2, 0, 0)
        Boxsiz3_pane3 = wx.BoxSizer(wx.VERTICAL)
        Boxsiz4_pane3 = wx.BoxSizer(wx.VERTICAL)
        FlxGsiz3_pane3 = wx.FlexGridSizer(1, 2, 0, 0)
        FlxGsiz2_pane3 = wx.FlexGridSizer(1, 2, 0, 0)
        FlxGsiz1_pane3 = wx.FlexGridSizer(1, 2, 0, 0)
        Boxsiz1_pane2 = wx.BoxSizer(wx.VERTICAL)
        Boxsiz2_pane2 = wx.BoxSizer(wx.VERTICAL)
        Gidsiz1_pane2 = wx.GridSizer(2, 1, 0, 0)
        FlxGsiz_pane2 = wx.FlexGridSizer(1, 2, 0, 0)
        Boxsiz_pane1 = wx.BoxSizer(wx.VERTICAL)
        Boxsiz2_pane1_staticbox.Lower()
        StaticBsiz2_pane1 = wx.StaticBoxSizer(Boxsiz2_pane1_staticbox, wx.VERTICAL)
        Boxsiz1_pane1_staticbox.Lower()
        StaticBsiz1_pane1 = wx.StaticBoxSizer(Boxsiz1_pane1_staticbox, wx.VERTICAL)
        Gridsiz_pane1 = wx.GridSizer(4, 1, 0, 0)
        Gridsiz_pane1.Add(lab1_pane1, 0, wx.ALIGN_BOTTOM | wx.ALL, 5)
        Gridsiz_pane1.Add(btn_threads, 0, wx.ALL, 5)
        Gridsiz_pane1.Add(lab2_pane1, 0, wx.ALIGN_BOTTOM | wx.ALL, 5)
        Gridsiz_pane1.Add(self.spinctrl_threads, 0, wx.ALL, 5)
        StaticBsiz1_pane1.Add(Gridsiz_pane1, 1, 0, 0)
        Boxsiz_pane1.Add(StaticBsiz1_pane1, 1, wx.ALL | wx.EXPAND, 5)
        StaticBsiz2_pane1.Add(self.check_cmdline, 0, wx.ALL, 5)
        Boxsiz_pane1.Add(StaticBsiz2_pane1, 1, wx.ALL | wx.EXPAND, 5)
        ntbk_pane_1.SetSizer(Boxsiz_pane1)
        Boxsiz2_pane2.Add(self.check_ffmpeglog, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        Boxsiz2_pane2.Add(self.check_cmdlog, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        Gidsiz1_pane2.Add(lab3_pane2, 0, wx.ALIGN_BOTTOM | wx.ALL, 5)
        FlxGsiz_pane2.Add(self.btn_log, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        FlxGsiz_pane2.Add(self.txt_pathlog, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        Gidsiz1_pane2.Add(FlxGsiz_pane2, 1, wx.EXPAND, 0)
        Boxsiz2_pane2.Add(Gidsiz1_pane2, 0, wx.EXPAND, 0)
        Boxsiz1_pane2.Add(Boxsiz2_pane2, 1, wx.EXPAND, 0)
        ntbk_pane_2.SetSizer(Boxsiz1_pane2)
        Boxsiz4_pane3.Add(self.checkbox_exeffmpeg, 0, wx.LEFT | wx.TOP, 15)
        FlxGsiz1_pane3.Add(self.btn_pathFFmpeg, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 15)
        FlxGsiz1_pane3.Add(self.textctrl_path_ffmpeg, 0, wx.ALIGN_CENTER_VERTICAL, 15)
        Boxsiz4_pane3.Add(FlxGsiz1_pane3, 1, 0, 0)
        Boxsiz4_pane3.Add(self.checkbox_exeFFprobe, 0, wx.LEFT, 15)
        FlxGsiz2_pane3.Add(self.btn_pathFFprobe, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 15)
        FlxGsiz2_pane3.Add(self.textctrl_path_ffprobe, 0, wx.ALIGN_CENTER_VERTICAL, 15)
        Boxsiz4_pane3.Add(FlxGsiz2_pane3, 1, 0, 0)
        Boxsiz4_pane3.Add(self.checkbox_exeFFplay, 0, wx.LEFT, 15)
        FlxGsiz3_pane3.Add(self.btn_pathFFplay, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT, 15)
        FlxGsiz3_pane3.Add(self.textctrl_path_ffplay, 0, wx.ALIGN_CENTER_VERTICAL, 15)
        Boxsiz4_pane3.Add(FlxGsiz3_pane3, 1, 0, 0)
        Boxsiz3_pane3.Add(Boxsiz4_pane3, 1, 0, 0)
        ntbk_pane3.SetSizer(Boxsiz3_pane3)
        ntbk.AddPage(ntbk_pane_1, ("General Settings"))
        ntbk.AddPage(ntbk_pane_2, ("Log Preferences"))
        ntbk.AddPage(ntbk_pane3, ("Executable paths"))
        grid_sizer_main.Add(ntbk, 1, wx.EXPAND, 0)
        GridsizBottom.Add(btn_close, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 15)
        GridsizBottom.Add(btn_ok, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL | wx.EXPAND, 15)
        grid_sizer_main.Add(GridsizBottom, 1, wx.EXPAND, 0)
        grid_sizer_main.AddGrowableRow(0)
        grid_sizer_main.AddGrowableRow(1)
        grid_sizer_main.AddGrowableCol(0)
        sizer_main.Add(grid_sizer_main, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_main)
        sizer_main.Fit(self)
        self.Layout()
        # end wxGlade
        
        #----------------------Binding (EVT)----------------------#
        self.Bind(wx.EVT_CHECKBOX, self.on_direct_cmd, self.check_cmdline)
        self.Bind(wx.EVT_BUTTON, self.push_threads, btn_threads)
        self.Bind(wx.EVT_CHECKBOX, self.log_ffmpeg, self.check_ffmpeglog)
        self.Bind(wx.EVT_CHECKBOX, self.log_command, self.check_cmdlog)
        self.Bind(wx.EVT_BUTTON, self.save_path_log, self.btn_log)
        #self.Bind(wx.EVT_TEXT, self.text_save, self.txt_pathlog)
        self.Bind(wx.EVT_SPINCTRL, self.on_threads, self.spinctrl_threads)
        self.Bind(wx.EVT_CHECKBOX, self.exeFFmpeg, self.checkbox_exeffmpeg)
        self.Bind(wx.EVT_BUTTON, self.open_path_ffmpeg, self.btn_pathFFmpeg)
        self.Bind(wx.EVT_CHECKBOX, self.exeFFprobe, self.checkbox_exeFFprobe)
        self.Bind(wx.EVT_BUTTON, self.open_path_ffprobe, self.btn_pathFFprobe)
        self.Bind(wx.EVT_CHECKBOX, self.exeFFplay, self.checkbox_exeFFplay)
        self.Bind(wx.EVT_BUTTON, self.open_path_ffplay, self.btn_pathFFplay)
        self.Bind(wx.EVT_BUTTON, self.on_close, btn_close)
        self.Bind(wx.EVT_BUTTON, self.on_ok, btn_ok)
        

        
        
        #----------------------Propagations----------------------#
        self.config() # call function for initialize setting layout 
        

    def config(self):
        """
        Setto l'abilitazione/disabilitazione in funzione del file di conf.
        Setting enable/disable on according to the configuration file
        
        """
        #### set layout on configuration file
        if self.ffmpeg_log == 'true':
            self.check_ffmpeglog.SetValue(True) # set on
            
        if self.command_log == 'true':
            self.check_cmdlog.SetValue(True) # set on
            self.txt_pathlog.AppendText(self.path_log)
            self.btn_log.Enable(), self.txt_pathlog.Enable()
            
        elif self.command_log == 'false': 
            # Button and textctrl for file log set disable
            self.btn_log.Disable(), self.txt_pathlog.Disable()
            self.txt_pathlog.SetValue("")
            
        if self.ffmpeg_check == 'false':
            self.btn_pathFFmpeg.Disable()
            self.textctrl_path_ffmpeg.Disable()
            self.textctrl_path_ffmpeg.SetValue("")
            self.checkbox_exeffmpeg.SetValue(False)
        else:
            self.textctrl_path_ffmpeg.AppendText(self.ffmpeg_link)
            self.checkbox_exeffmpeg.SetValue(True)
            
        if self.ffprobe_check == 'false':
            self.btn_pathFFprobe.Disable()
            self.textctrl_path_ffprobe.Disable()
            self.textctrl_path_ffprobe.SetValue("")
            self.checkbox_exeFFprobe.SetValue(False)
        else:
            self.textctrl_path_ffprobe.AppendText(self.ffprobe_link)
            self.checkbox_exeFFprobe.SetValue(True)
            
        if self.ffplay_check == 'false':
            self.btn_pathFFplay.Disable()
            self.textctrl_path_ffplay.Disable()
            self.textctrl_path_ffplay.SetValue("")
            self.checkbox_exeFFplay.SetValue(False)
        else:
            self.textctrl_path_ffplay.AppendText(self.ffplay_link)
            self.checkbox_exeFFplay.SetValue(True)
            
        if self.writeline_exec == 'true':
            self.check_cmdline.SetValue(True)

    #----------------------Event handler (callback)----------------------#
    def on_direct_cmd(self, event):
        """
        enable or disable direct text command editable
        """
        if self.check_cmdline.GetValue() is True:
            self.full_list[76] = 'WRITE_LINE = true\n'
            
        elif self.check_cmdline.GetValue() is False:
            self.full_list[76] = 'WRITE_LINE = false\n'
            
    #----------------------Event handler (callback)----------------------#
    def push_threads(self, event):
        """
        Show a dialog with number threads
        """
        wx.MessageBox("You have set %s threads (CPU tasks) in the process of "
                      "ffmpeg " % self.threads[9:], "Number of threads set", 
                      wx.ICON_INFORMATION, self)
        
    #--------------------------------------------------------------------#
    def log_ffmpeg(self, event):
        """
        put in list
        """
        if self.check_ffmpeglog.GetValue() is True:
            self.full_list[30] = 'FFMPEG_LOG = true\n'
            
        elif self.check_ffmpeglog.GetValue() is False:
            self.full_list[30] = 'FFMPEG_LOG = false\n'
            
    #--------------------------------------------------------------------#
    def log_command(self, event):
        
        if self.check_cmdlog.GetValue() is True:
            self.full_list[33] = 'COMMAND_LOG = true\n'
            self.btn_log.Enable(), self.txt_pathlog.Enable()
            
        elif self.check_cmdlog.GetValue() is False:
            self.full_list[33] = 'COMMAND_LOG = false\n'
            self.txt_pathlog.SetValue("")
            self.btn_log.Disable(), self.txt_pathlog.Disable()

    #--------------------------------------------------------------------#
    def save_path_log(self, event):
        
        dialdir = wx.DirDialog(self, "Where do you want to save the log file?")
            
        if dialdir.ShowModal() == wx.ID_OK:
            self.txt_pathlog.SetValue("")
            self.txt_pathlog.AppendText(dialdir.GetPath())
            self.full_list[36] = 'PATH = %s\n' % (dialdir.GetPath())
            dialdir.Destroy()

    #--------------------------------------------------------------------#
    #def text_save(self, event):
        
        ##save = self.txt_pathlog.GetValue()
        ##self.full_list[28] = 'PATH = %s\n' % (save)
        #event.Skip()

    #--------------------------------------------------------------------#
    def on_threads(self, event):
        
        sett = self.spinctrl_threads.GetValue()
        self.full_list[27] = 'THREADS = -threads %s\n' % sett
        
    #----------------------ffmpeg path checkbox--------------------------#
    def exeFFmpeg(self, event):
        
        if self.checkbox_exeffmpeg.GetValue() is True:
            self.btn_pathFFmpeg.Enable()
            self.textctrl_path_ffmpeg.Enable()
            self.textctrl_path_ffmpeg.SetValue("")
            self.full_list[61] = 'FFMPEG_PATH = true\n'

        elif self.checkbox_exeffmpeg.GetValue() is False:
            self.btn_pathFFmpeg.Disable()
            self.textctrl_path_ffmpeg.Disable()
            self.textctrl_path_ffmpeg.SetValue("")
            self.full_list[61] = 'FFMPEG_PATH = false\n'
            self.full_list[63] = 'ffmpeg\n'

    #----------------------ffmpeg path open dialog----------------------#
    def open_path_ffmpeg(self, event):
        
        dialogfile = wx.FileDialog(self, 
                        "Indicates where the binary ffmpeg", "", "", 
                        "ffmpeg binarys (*ffmpeg)|*ffmpeg| All files (*.*)|*.*", 
                        wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
            
        if dialogfile.ShowModal() == wx.ID_OK:
            self.textctrl_path_ffmpeg.SetValue("")
            self.textctrl_path_ffmpeg.AppendText(dialogfile.GetPath())
            self.full_list[63] = '%s\n' % (dialogfile.GetPath())
            
            dialogfile.Destroy()

    #----------------------ffprobe path checkbox--------------------------#
    def exeFFprobe(self, event):
        
        if self.checkbox_exeFFprobe.GetValue() is True:
            self.btn_pathFFprobe.Enable()
            self.textctrl_path_ffprobe.Enable()
            self.textctrl_path_ffprobe.SetValue("")
            self.full_list[66] = 'FFPROBE_PATH = true\n'

        elif self.checkbox_exeFFprobe.GetValue() is False:
            self.btn_pathFFprobe.Disable()
            self.textctrl_path_ffprobe.Disable()
            self.textctrl_path_ffprobe.SetValue("")
            self.full_list[66] = 'FFPROBE_PATH = false\n'
            self.full_list[68] = 'ffprobe\n'

    #----------------------ffprobe path open dialog----------------------#
    def open_path_ffprobe(self, event):
        
        dialfile = wx.FileDialog(self, 
                    "Indicates where the binary ffprobe", "", "", 
                    "ffprobe binarys (*ffprobe)|*ffprobe| All files (*.*)|*.*", 
                    wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
            
        if dialfile.ShowModal() == wx.ID_OK:
            self.textctrl_path_ffprobe.SetValue("")
            self.textctrl_path_ffprobe.AppendText(dialfile.GetPath())
            self.full_list[68] = '%s\n' % (dialfile.GetPath())
            dialfile.Destroy()

    #----------------------ffplay path checkbox--------------------------#
    def exeFFplay(self, event):
        
        if self.checkbox_exeFFplay.GetValue() is True:
            self.btn_pathFFplay.Enable()
            self.textctrl_path_ffplay.Enable()
            self.textctrl_path_ffplay.SetValue("")
            self.full_list[71] = 'FFPLAY_PATH = true\n'

        elif self.checkbox_exeFFplay.GetValue() is False:
            self.btn_pathFFplay.Disable()
            self.textctrl_path_ffplay.Disable()
            self.textctrl_path_ffplay.SetValue("")
            self.full_list[71] = 'FFPLAY_PATH = false\n'
            self.full_list[73] = 'ffplay\n'

    #----------------------ffplay path open dialog----------------------#
    def open_path_ffplay(self, event):
        
        dialfile = wx.FileDialog(self, 
                    "Indicates where the binary ffplay", "", "", 
                    "ffplay binarys (*ffplay)|*ffplay| All files (*.*)|*.*", 
                    wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
            
        if dialfile.ShowModal() == wx.ID_OK:
            self.textctrl_path_ffplay.SetValue("")
            self.textctrl_path_ffplay.AppendText(dialfile.GetPath())
            self.full_list[73] = '%s\n' % (dialfile.GetPath())
            
            dialfile.Destroy()

    #--------------------------------------------------------------------#
    def on_close(self, event):
        
        event.Skip()

    #--------------------------------------------------------------------#
    def on_ok(self, event):
        
        if self.check_cmdlog.GetValue() is True and \
                                    self.txt_pathlog.GetValue() == "":
                                        
            wx.MessageBox("Warning, The log command has no set path name "
                    "Warning", wx.ICON_EXCLAMATION, self)
        else:
            fileconf = open('%s/.videomass/videomass.conf' % (dirname),'w')
            
            for i in self.full_list:
                overwrite = fileconf.write('%s' % i)
                
            fileconf.close()
            
            wx.MessageBox(u"\nTo make the changes you need to restart the program")
            
            #self.Destroy() # WARNING on mac not close corretly, on linux ok
            self.Close()
        event.Skip()
        
