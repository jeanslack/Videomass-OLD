#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Fri Aug  2 21:15:59 2013
#
#########################################################
# Name: videocut.py
# Porpose:cut a preset video sequence 
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (05) 20/07/2014
# Rev (06) 15/03/2015
#########################################################

import wx
import os
import webbrowser
#import wx.lib.masked as masked # not work on macOSX
from os_processing import proc_std
from epilogue import Formula

dirname = os.path.expanduser('~/') # /home/user/

# create need list, dict
diction_command = {"ext_input":"", "InputDir":"" ,"OutputDir":"", 
                "Pathname":"", "Dirname":"", "Filename":"", 
                "cutstart":"", "cutlengh":""
                }

class VideoCut(wx.Dialog):
    """
    Cut portions of movie by setting the start time and time progress. 
    The saved movie maintains same audio/video codecs and features of 
    the original movie.
    """
    def __init__(self, parent, helping, ffmpeg_link, threads, ffmpeg_log, 
                command_log, path_log, loglevel_type):

        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        
        self.helping = helping
        self.ffmpeg_link = ffmpeg_link
        self.threads = threads
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.parent = parent
        self.parent.Hide()
        
        self.start_hour_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=23, style=wx.TE_PROCESS_ENTER)
        self.start_minute_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=59, style=wx.TE_PROCESS_ENTER)
        self.start_second_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=59, style=wx.TE_PROCESS_ENTER)
        self.sizer_1_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Start Time (h:m:s)"))
        self.stop_hour_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=23, style=wx.TE_PROCESS_ENTER)
        self.stop_minute_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=59, style=wx.TE_PROCESS_ENTER)
        self.stop_second_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=59, style=wx.TE_PROCESS_ENTER)
        sizer_6_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Time Progress (h:m:s)"))
        btn_open = wx.Button(self, wx.ID_OPEN, "")
        self.text_open = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER | wx.TE_READONLY)
        self.btn_save = wx.Button(self, wx.ID_SAVE, "")
        self.text_save = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER | wx.TE_READONLY)
        siz4_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Import/Export Media Movie"))
        btn_close = wx.Button(self, wx.ID_CANCEL, "Close")
        btn_help = wx.Button(self, wx.ID_HELP, "")
        self.button_ok = wx.Button(self, wx.ID_OK, "Start")
        self.btn_save.Disable()
        
        diction_command["InputDir"] = ""
        diction_command["OutputDir"] = ""

        #----------------------Properties----------------------#
        self.SetTitle("Cut Video Sequences")
        self.start_hour_ctrl.SetMinSize((45, 20))
        self.start_minute_ctrl.SetMinSize((45, 20))
        self.start_second_ctrl.SetMinSize((45, 20))
        self.start_hour_ctrl.SetToolTipString("Start sequence movie"
                                              "(time hours format)"
                                              )
        self.start_minute_ctrl.SetToolTipString("Start sequence movie"
                                                "(time minute format)"
                                                )
        self.start_second_ctrl.SetToolTipString("Start sequence movie"
                                                "(time seconds format)"
                                                )
        self.stop_hour_ctrl.SetMinSize((45, 20))
        self.stop_minute_ctrl.SetMinSize((45, 20))
        self.stop_second_ctrl.SetMinSize((45, 20))
        self.stop_hour_ctrl.SetToolTipString("Sets the total amount of time "
                                             "progress, starting from the start "
                                             "time extraction  "
                                             )
        self.stop_minute_ctrl.SetToolTipString("Sets the total amount of time "
                                             "progress, starting from the start "
                                             "time extraction  "
                                             )
        self.stop_second_ctrl.SetToolTipString("Sets the total amount of time "
                                             "progress, starting from the start "
                                             "time extraction  "
                                             )
        self.text_open.SetMinSize((230, 21))
        self.text_open.SetToolTipString("Imported path-name (read only)")
        self.text_save.SetMinSize((230, 21))
        self.text_save.SetToolTipString("Exported path-name (read only)")
        self.button_ok.Disable()
        self.button_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
        self.button_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        self.init_hour = '00'
        self.init_minute = '00'
        self.init_second = '00'
        self.cut_hour = '00'
        self.cut_minute = '00'
        self.cut_second = '00'
        
        #----------------------Layout----------------------#
        grid_sizer_5 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_3 = wx.GridSizer(1, 3, 0, 0)
        grid_sizer_2 = wx.GridSizer(2, 1, 0, 0)
        siz4_staticbox.Lower()
        sizer_4 = wx.StaticBoxSizer(siz4_staticbox, wx.VERTICAL)
        grid_sizer_1 = wx.FlexGridSizer(2, 2, 0, 0)
        grid_sizer_4 = wx.GridSizer(2, 1, 0, 0)
        sizer_6_staticbox.Lower()
        sizer_6 = wx.StaticBoxSizer(sizer_6_staticbox, wx.HORIZONTAL)
        self.sizer_1_staticbox.Lower()
        sizer_1 = wx.StaticBoxSizer(self.sizer_1_staticbox, wx.HORIZONTAL)
        sizer_1.Add(self.start_hour_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_1.Add(self.start_minute_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_1.Add(self.start_second_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_4.Add(sizer_1, 1, wx.ALL | wx.EXPAND, 15)
        sizer_6.Add(self.stop_hour_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_6.Add(self.stop_minute_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_6.Add(self.stop_second_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_4.Add(sizer_6, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_2.Add(grid_sizer_4, 1, wx.EXPAND, 0)
        grid_sizer_1.Add(btn_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_1.Add(self.text_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_1.Add(self.btn_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_1.Add(self.text_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_4.Add(grid_sizer_1, 1, wx.EXPAND, 0)
        grid_sizer_2.Add(sizer_4, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_5.Add(grid_sizer_2, 1, wx.EXPAND, 0)
        grid_sizer_3.Add(btn_close, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer_3.Add(btn_help, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer_3.Add(self.button_ok, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer_5.Add(grid_sizer_3, 1, wx.ALL | wx.EXPAND, 15)
        self.SetSizer(grid_sizer_5)
        grid_sizer_5.Fit(self)
        self.Layout()

        #----------------------Binding (EVT)----------------------#
        self.Bind(wx.EVT_SPINCTRL, self.start_hour, self.start_hour_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.start_minute, self.start_minute_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.start_second, self.start_second_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.stop_hour, self.stop_hour_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.stop_minute, self.stop_minute_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.stop_second, self.stop_second_ctrl)
        #self.Bind(wx.EVT_SPINCTRL, self.on_time_init, self.time_start)
        #self.Bind(wx.EVT_SPINCTRL, self.on_time_finish, self.time_lengh)
        self.Bind(wx.EVT_BUTTON, self.on_open, btn_open)
        self.Bind(wx.EVT_TEXT, self.enter_open, self.text_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.btn_save)
        self.Bind(wx.EVT_TEXT, self.enter_save, self.text_save)
        self.Bind(wx.EVT_BUTTON, self.on_close, btn_close)
        self.Bind(wx.EVT_BUTTON, self.on_help, btn_help)
        self.Bind(wx.EVT_BUTTON, self.on_ok, self.button_ok)

    #----------------------Event handler (callback)----------------------#
    def start_hour(self, event):
        self.init_hour = '%s' % self.start_hour_ctrl.GetValue()
        if len(self.init_hour) == 1:
            self.init_hour = '0%s' % self.start_hour_ctrl.GetValue()
    #------------------------------------------------------------------#
    def start_minute(self, event):
        self.init_minute = '%s' % self.start_minute_ctrl.GetValue()
        if len(self.init_minute) == 1:
            self.init_minute = '0%s' % self.start_minute_ctrl.GetValue()
    #------------------------------------------------------------------#
    def start_second(self, event):
        self.init_second = '%s' % self.start_second_ctrl.GetValue()
        if len(self.init_second) == 1:
            self.init_second = '0%s' % self.start_second_ctrl.GetValue()
    #------------------------------------------------------------------#
    def stop_hour(self, event):
        self.cut_hour = '%s' % self.stop_hour_ctrl.GetValue()
        if len(self.cut_hour) == 1:
            self.cut_hour = '0%s' % self.stop_hour_ctrl.GetValue()
    #------------------------------------------------------------------#
    def stop_minute(self, event):
        self.cut_minute = '%s' % self.stop_minute_ctrl.GetValue()
        if len(self.cut_minute) == 1:
            self.cut_minute = '0%s' % self.stop_minute_ctrl.GetValue()
    #------------------------------------------------------------------#
    def stop_second(self, event):
        self.cut_second = '%s' % self.stop_second_ctrl.GetValue()
        if len(self.cut_second) == 1:
            self.cut_second = '0%s' % self.stop_second_ctrl.GetValue()
    #------------------------------------------------------------------#
    def on_open(self, event):
        
        dialfile = wx.FileDialog(self, "Importing Movie", ".", "", "*.*", 
                                    wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
                                    )
        
        if dialfile.ShowModal() == wx.ID_OK:
            self.text_open.SetValue(""), self.btn_save.Enable()
            self.text_save.SetValue("")
            self.text_open.AppendText(dialfile.GetPath())
            fileName, fileExtension = os.path.splitext(dialfile.GetPath())
            diction_command["ext_input"] = fileExtension.split(".")[-1]
            #diction_command["Pathname"] = "%s" % (dialfile.GetPath())
            #diction_command["Dirname"] = "%s" % (dialfile.GetDirectory())
            #diction_command["Filename"] = "%s" % (dialfile.GetFilename()
            dialfile.Destroy()
    #------------------------------------------------------------------#
    def enter_open(self, event):
        diction_command["InputDir"] = self.text_open.GetValue()
    #------------------------------------------------------------------#
    def on_save(self, event):
        
        ext = diction_command["ext_input"]
        dialsave = wx.FileDialog(self, "Exports The Movie Sequence", 
                                    "", "", "%s files (*.%s)|*.%s" % (
                                    ext,ext,ext), wx.SAVE | wx.OVERWRITE_PROMPT | 
                                    wx.FD_CHANGE_DIR
                                    )
        if dialsave.ShowModal() == wx.ID_OK:
            path = dialsave.GetPath()
            if path.endswith(ext) != True:
                path = "%s.%s" % (path, ext)
            self.text_save.SetValue("")
            self.text_save.AppendText(path)
            dialsave.Destroy()
    #------------------------------------------------------------------#
    def enter_save(self, event):
        diction_command["OutputDir"] = self.text_save.GetValue()
        
        if self.text_save.GetValue() == '':
            self.button_ok.Disable()
            self.button_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
            self.button_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        else:
            self.button_ok.Enable()
            self.button_ok.SetBackgroundColour(wx.Colour(180, 214, 255))
            self.button_ok.SetForegroundColour(wx.Colour(130, 51, 27))
    #------------------------------------------------------------------#
    def on_close(self, event):
        #self.Destroy()
        self.parent.Show()
        event.Skip() # need if destroy from parent
    #------------------------------------------------------------------#

    def on_help(self, event):
        #wx.MessageBox("L'help contestuale é ancora in fase di sviluppo .")
        webbrowser.open('%s/14-Tagliare_video.html' % self.helping)
    #------------------------------------------------------------------#
    def on_ok(self, event):
        
        logname = 'Videomass_VideoCut.log'
        
        if self.ffmpeg_log == 'true':
            report = '-report'
        else:
            report = ''
            
        diction_command["cutstart"] = "%s:%s:%s" % (self.init_hour, self.init_minute, 
                                                    self.init_second
                                                    )
        diction_command["cutlengh"] = "%s:%s:%s" % (self.cut_hour, self.cut_minute, 
                                                    self.cut_second
                                                    )
        command = ("%s %s -i '%s' -loglevel %s -ss %s -t %s "
                    "-vcodec copy -acodec copy %s -y '%s'" % (self.ffmpeg_link,
                    report, self.text_open.GetValue(), self.loglevel_type, 
                    diction_command["cutstart"], diction_command["cutlengh"], 
                    self.threads, self.text_save.GetValue())
                    )
        formula = ("FORMULATIONS:\n\nConversion Mode:\nVideo Format:\
                    \nStart cutting:\nSequence Duration:\nImporting:\
                    \nExporting:"
                   )
        dictions = ("\n\n%s\nvideo copy (%s)\n%s\n%s\n%s\n%s" % (
                    "Single File Export",diction_command["ext_input"], 
                    diction_command["cutstart"],diction_command["cutlengh"], 
                    diction_command["InputDir"],diction_command["OutputDir"])
                    )
        ending = Formula(self, formula, dictions)
        
        if ending.ShowModal() == wx.ID_OK:
            dial = proc_std(self, 'Videomass - Run single process', command,
                            self.path_log, logname, self.command_log,)
            
