#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Fri Jun 20 21:51:19 2014
#
#########################################################
# Name: normalize.py
# Porpose: normalize volume of the audio files 
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (03) 04/08/2014
# Rev (04) 14/01/2015
# Rev (04) 14/03/2015
#########################################################


import wx
import string
import os
import wx.lib.agw.floatspin as FS
import webbrowser
from epilogue import Formula
from os_processing import proc_normalize_batch_thread, control_errors,\
                                ProgressDialog, proc_std, proc_volumedetect

dirname = os.path.expanduser('~/') # /home/user/


# create need list, dict:
diction_command = {"Batch":"single file conversion","ext_input":"", 
                "InputDir":"" ,"OutputDir":"", "Pathname":"", "Dirname":"", 
                "Filename":"", "Audioext":"", "Normalize":"-1.0", "limited":"0.0",
                "overwrite":""}
array = []

class Normalize(wx.Dialog):
    """
    Audio Normalization. Also, normalize multiple files with same 
    extension in batch mode.
    """
    def __init__(self, parent, helping, ffmpeg_link, threads, ffmpeg_log, 
                command_log, path_log, loglevel_type):
        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        self.helping = helping
        self.ffmpeg_link = ffmpeg_link
        self.threads = threads
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.parent = parent
        self.parent.Hide()
        
        self.button_open = wx.Button(self, wx.ID_OPEN, "")
        self.text_path_open = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER | wx.TE_READONLY)
        self.button_save = wx.Button(self, wx.ID_SAVE, "")
        self.text_path_save = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER | wx.TE_READONLY)
        #self.checkbox_overwrite = wx.CheckBox(self, wx.ID_ANY, ("Overwrite imported files"))
        self.siz1_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Import/Export Audio Media File"))
        self.checkbox_batch = wx.CheckBox(self, wx.ID_ANY, ("Batch-Mode"))
        self.label_group = wx.StaticText(self, wx.ID_ANY, ("Group:"))
        self.text_group_ext = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER)
        siz2_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Options"))
        self.checkbox_adv_opt = wx.CheckBox(self, wx.ID_ANY, ("Enable Advanced Options"))
        self.button_analyzes = wx.Button(self, wx.ID_ANY, ("Analyze"))
        self.label_limited = wx.StaticText(self, wx.ID_ANY, ("   dB Volume Max:"))
        self.text_dbMax = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_READONLY)
        self.label_dbMedium = wx.StaticText(self, wx.ID_ANY, ("dB Volume Middle:"))
        self.text_dbMedium = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_READONLY)
        label_normalize = wx.StaticText(self, wx.ID_ANY, (
                        "   Normalize maximum amplitude in dB: ")
                                                        )
        self.spin_ctrl_audionormalize = FS.FloatSpin(self, wx.ID_ANY,  
                        min_val=-99.0, max_val=0.0, increment=1.0, value=-1.0, 
                        agwStyle=FS.FS_LEFT
                        )
        self.label_limit = wx.StaticText(self, wx.ID_ANY, (
            "   Threshold Limit in dB: ")
                                                        )
        self.spin_ctrl_audionormalize.SetFormat("%f")
        self.spin_ctrl_audionormalize.SetDigits(1)

        self.spin_ctrl_limit = FS.FloatSpin(self, wx.ID_ANY, min_val=-99.0, 
                    max_val=0.0, increment=1.0, value=0.0, agwStyle=FS.FS_LEFT
                    )
        self.spin_ctrl_limit.SetFormat("%f")
        self.spin_ctrl_limit.SetDigits(1)
        
        
        siz3_staticbox = wx.StaticBox(self, wx.ID_ANY,("Normalization Parameters"))
        btn_close = wx.Button(self, wx.ID_CANCEL, "Close")
        btn_help = wx.Button(self, wx.ID_HELP, "")
        self.button_ok = wx.Button(self, wx.ID_OK, "Start")
        
        #----------------------Set Properties----------------------#
        self.SetTitle("Audio Track Volume Normalization")

        self.text_path_open.SetMinSize((200, 20))
        self.text_path_open.SetToolTipString("Imported path-name")

        self.text_path_save.SetMinSize((200, 20))
        self.text_path_save.SetToolTipString("Exported path-name")
        #self.checkbox_overwrite.SetToolTipString("Overwrites all files "
                                                 #"imported. If enabled, "
                                                 #"disable the export path.")
        self.checkbox_batch.SetToolTipString("Apply the process to a files "
                                             "group with same extensions")
        self.text_group_ext.SetMinSize((50, 21))
        self.text_group_ext.SetToolTipString("Write here a files group "
                                 "extension to convert with batch mode. Do not "
                                 "include punctuation marks '.' and blanks"
                                             )
        self.checkbox_adv_opt.SetToolTipString("Enable advanced parameters")
        self.button_analyzes.SetBackgroundColour(wx.Colour(255, 115, 106))# color
        self.button_analyzes.SetToolTipString(u"Shows the maximum peak and "
                                       "average volume of an audio track. "
                                       "This feature is disabled if you enable "
                                       "Batch-Mode."
                                              )
        self.text_dbMax.SetToolTipString("dB maximum volume of the scanned file")
        self.text_dbMedium.SetToolTipString("dB average volume of the scanned "
                                            "file in dB"
                                             )
        self.spin_ctrl_audionormalize.SetMinSize((70, 20))
        self.spin_ctrl_audionormalize.SetToolTipString("Select with many db "
                                 "you want to increase or decrease the volume. "
                                 "The upper limit is 0.0, the minimum limit is "
                                 "-99.0. The default setting is -1.0 and is "
                                 "good for most of the processes"
                                                        )
        self.spin_ctrl_limit.SetMinSize((70, 20))
        self.spin_ctrl_limit.SetToolTipString("Normalization is applied only "
                                           "to files that have a maximum "
                                           "volume below the threshold set here"
                                               )
        self.button_ok.SetMinSize((150, 27))
        
        # setting default
        self.button_ok.Disable()
        self.button_ok.SetFont(wx.Font(9, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))
        self.button_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
        self.button_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        self.label_limit.Disable(), self.spin_ctrl_limit.Disable()
        self.button_save.Disable(), self.text_path_save.Disable()
        self.text_group_ext.Disable(), self.label_group.Disable()
        self.text_group_ext.SetValue(""), #self.checkbox_overwrite.Disable()
        diction_command["InputDir"] = ""
        diction_command["OutputDir"] = ""
        
        if 'batch' in array:
            diction_command ["Batch"] = "single file conversion"
            array.remove('batch')

        #----------------------Layout----------------------#
        grid_sizer_1 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_controlbutton = wx.FlexGridSizer(1, 3, 0, 0)
        grid_sizer_2 = wx.FlexGridSizer(1, 2, 0, 0)
        siz3_staticbox.Lower()
        sizer_3 = wx.StaticBoxSizer(siz3_staticbox, wx.VERTICAL)
        sizer_pane3_audio_column2 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_in_column2 = wx.FlexGridSizer(3, 1, 0, 0)
        grid_sizer_6 = wx.FlexGridSizer(1, 2, 0, 0)
        grid_sizer_audionormalize = wx.GridSizer(3, 1, 0, 0)
        grid_sizer_slider_normalize = wx.FlexGridSizer(1, 2, 0, 0)
        grid_sizer_text_normalize = wx.FlexGridSizer(2, 2, 0, 0)
        grid_sizer_3 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_5_options = wx.FlexGridSizer(2, 1, 0, 0)
        siz2_staticbox.Lower()
        sizer_2 = wx.StaticBoxSizer(siz2_staticbox, wx.VERTICAL)
        grid_sizer_5 = wx.GridSizer(1, 3, 0, 0)
        self.siz1_staticbox.Lower()
        sizer_1 = wx.StaticBoxSizer(self.siz1_staticbox, wx.VERTICAL)
        grid_sizer_4 = wx.FlexGridSizer(2, 2, 0, 0)
        grid_sizer_4.Add(self.button_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_4.Add(self.text_path_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_4.Add(self.button_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_4.Add(self.text_path_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_1.Add(grid_sizer_4, 1, wx.EXPAND, 0)
        #sizer_1.Add(self.checkbox_overwrite, 0, wx.ALL, 15)
        grid_sizer_3.Add(sizer_1, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_5.Add(self.checkbox_batch, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_5.Add(self.label_group, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_5.Add(self.text_group_ext, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_2.Add(grid_sizer_5, 1, wx.EXPAND, 0)
        sizer_2.Add(self.checkbox_adv_opt, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_5_options.Add(sizer_2, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_3.Add(grid_sizer_5_options, 1, wx.EXPAND, 0)
        grid_sizer_2.Add(grid_sizer_3, 1, wx.EXPAND, 0)
        
        grid_sizer_audionormalize.Add(self.button_analyzes, 0, wx.ALL, 15)
        grid_sizer_text_normalize.Add(self.label_limited, 0, wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_text_normalize.Add(self.text_dbMax, 0, wx.BOTTOM, 15)
        grid_sizer_text_normalize.Add(self.label_dbMedium, 0, wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_text_normalize.Add(self.text_dbMedium, 0, wx.BOTTOM, 15)
        grid_sizer_audionormalize.Add(grid_sizer_text_normalize, 1, wx.EXPAND, 0)
        grid_sizer_slider_normalize.Add(label_normalize, 0, wx.TOP | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_slider_normalize.Add(self.spin_ctrl_audionormalize, 0, wx.TOP | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_audionormalize.Add(grid_sizer_slider_normalize, 1, wx.EXPAND, 0)
        grid_sizer_in_column2.Add(grid_sizer_audionormalize, 1, wx.EXPAND, 5)
        grid_sizer_6.Add(self.label_limit, 0, wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_6.Add(self.spin_ctrl_limit, 0, wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_in_column2.Add(grid_sizer_6, 1, wx.EXPAND, 0)
        
        sizer_pane3_audio_column2.Add(grid_sizer_in_column2, 1, wx.EXPAND, 0)
        sizer_3.Add(sizer_pane3_audio_column2, 1, wx.EXPAND, 0)
        grid_sizer_2.Add(sizer_3, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_1.Add(grid_sizer_2, 1, wx.EXPAND, 0)
        grid_sizer_controlbutton.Add(btn_close, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_controlbutton.Add(btn_help, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_controlbutton.Add(self.button_ok, 0, wx.ALL, 15)
        grid_sizer_1.Add(grid_sizer_controlbutton, 1, wx.ALL | wx.ALIGN_RIGHT, 15)
        self.SetSizer(grid_sizer_1)
        grid_sizer_1.Fit(self)
        self.Layout()

        #----------------------Binding (EVT)----------------------#
        self.Bind(wx.EVT_BUTTON, self.on_open, self.button_open)
        self.Bind(wx.EVT_TEXT, self.enter_open, self.text_path_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.button_save)
        self.Bind(wx.EVT_TEXT, self.enter_save, self.text_path_save)
        #self.Bind(wx.EVT_CHECKBOX, self.on_overwrite, self.checkbox_overwrite)
        self.Bind(wx.EVT_CHECKBOX, self.on_batch, self.checkbox_batch)
        self.Bind(wx.EVT_TEXT, self.enter_group_ext, self.text_group_ext)
        self.Bind(wx.EVT_CHECKBOX, self.on_adv_opt, self.checkbox_adv_opt)
        self.Bind(wx.EVT_BUTTON, self.on_audio_analyzes, self.button_analyzes)
        self.Bind(wx.EVT_SPINCTRL, self.enter_normalize, self.spin_ctrl_audionormalize)#
        self.Bind(wx.EVT_SPINCTRL, self.enter_limit, self.spin_ctrl_limit)#
        self.Bind(wx.EVT_BUTTON, self.on_close, btn_close)
        self.Bind(wx.EVT_BUTTON, self.on_help, btn_help)
        self.Bind(wx.EVT_BUTTON, self.on_ok, self.button_ok)
        
    def enable_button(self):
        self.button_ok.Enable()
        self.button_ok.SetBackgroundColour(wx.Colour(180, 214, 255))
        self.button_ok.SetForegroundColour(wx.Colour(130, 51, 27))
        
    def disable_button(self):
        self.button_ok.Disable()
        self.button_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
        self.button_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        self.text_path_open.SetValue(""),self.text_path_save.SetValue("")
        self.button_save.Disable(), self.text_path_save.Disable()
        
    #------------------------------------------------------------------#
    def on_open(self, event):  
        """
        Open choice dialog input
        """
        wildcard = ("Audio source (*.wav; *.aiff; *.mp3; *.m4a; *.ogg; "
                    "*.flac; *.ac3; *.aac;)|*.wav;*.aiff;*.mp3;*.m4a;"
                    "*.ogg;*.flac;*.ac3;*.aac;|" "All files (*.*)|*.*"
                    )
        
        dialfile = wx.FileDialog(self, "Import audio file", 
                        "", "", wildcard, wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
                                    
        
        dialdir = wx.DirDialog(self, "Input Select directory for the batch process")
        
        if self.checkbox_batch.GetValue() is False:
            
            if dialfile.ShowModal() == wx.ID_OK:
                
                self.text_path_open.SetValue("")
                self.text_path_open.AppendText(dialfile.GetPath())
                basename_path = dialfile.GetFilenames()
                filenames = string.join(basename_path)
                file_name, file_ext = os.path.splitext(filenames)
                diction_command["Audioext"] = file_ext
                self.button_save.Enable(), self.text_path_save.Enable()
                #self.checkbox_overwrite.Enable()
                dialfile.Destroy()
                
        else:
            if dialdir.ShowModal() == wx.ID_OK:
                self.text_path_open.SetValue("")
                self.text_path_open.AppendText(dialdir.GetPath())
                self.button_save.Enable(), self.text_path_save.Enable()
                #self.checkbox_overwrite.Enable()
                dialdir.Destroy()
                
    #------------------------------------------------------------------#
    def enter_open(self, event):  
        """
        This is a text widget input
        """
        diction_command["InputDir"] = self.text_path_open.GetValue() 
        
    #------------------------------------------------------------------#
    def on_save(self, event):  
        """
        Open choice dialog output
        """
        ext = diction_command["Audioext"].replace('.','')
        
        if self.checkbox_batch.GetValue() is False:
            dialsave = wx.FileDialog(self, "Export audio file", "", "", 
                                     "%s files (*.%s)|*.%s" % (ext,ext,ext), 
                                     wx.SAVE | wx.OVERWRITE_PROMPT | 
                                     wx.FD_CHANGE_DIR
                                     )
            if dialsave.ShowModal() == wx.ID_OK:
                path = dialsave.GetPath()
                if path.endswith(ext) != True:
                    path = "%s.%s" % (path, ext)
                self.text_path_save.SetValue("")
                self.text_path_save.AppendText(path)
                dialsave.Destroy()
                self.enable_button()
        else:
            dialdir = wx.DirDialog(self, 
                              "Output Select directory for the batch process"
                                   )
            if dialdir.ShowModal() == wx.ID_OK:
                    self.text_path_save.SetValue("")
                    self.text_path_save.AppendText(dialdir.GetPath())
                    dialdir.Destroy()
                    self.enable_button()
                    
    #------------------------------------------------------------------#
    def enter_save(self, event):  
        """
        This is a text widget output
        """
        diction_command["OutputDir"] = "%s" %(self.text_path_save.GetValue())
        
    #------------------------------------------------------------------#
    #def on_overwrite(self, event):
        
        #if self.checkbox_overwrite.GetValue() is True:
            
            #self.button_save.Disable(), self.text_path_save.Disable()
            #self.text_path_save.SetValue(""), self.enable_button() 
            #diction_command["OutputDir"] = self.text_path_open.GetValue()
            #self.text_path_save.AppendText(self.text_path_open.GetValue())
            
        #elif self.checkbox_overwrite.GetValue() is False:
            
            #self.button_save.Enable(), self.text_path_save.Enable()
            #self.button_ok.Disable()
            #self.button_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
            #self.button_ok.SetForegroundColour(wx.Colour(34, 31, 30))
            #diction_command["OutputDir"] = ""
            
    #------------------------------------------------------------------#
    def on_batch(self, event): 
        
        on_batch = "batch"
        
        if self.checkbox_batch.GetValue() is True:
            # change the layout of open/save file/dir
            self.siz1_staticbox.SetLabel('Import/Export Audio Media Directory')
            self.button_open.SetBackgroundColour(wx.Colour(255, 196, 143))
            self.button_save.SetBackgroundColour(wx.Colour(255, 196, 143))
            self.text_group_ext.Enable(), self.label_group.Enable() # estensione gruppo
            self.text_group_ext.SetValue(""), self.text_path_open.SetValue("")
            self.text_path_save.SetValue("")
            
            # disable analyzes tools
            self.button_analyzes.Disable()
            self.text_dbMax.SetValue(""), self.text_dbMedium.SetValue("")
            self.text_dbMax.Disable(), self.text_dbMedium.Disable()
            self.label_limited.Disable(), self.label_dbMedium.Disable()
            self.button_analyzes.SetBackgroundColour(wx.Colour(223, 220, 217))#norm
            
            # disable button_ok
            self.disable_button(), #self.checkbox_overwrite.Disable()
            #self.checkbox_overwrite.SetValue(False)
            
            # set dictionary and list
            diction_command["OutputDir"] = ""
            diction_command["Batch"] = "group files conversion"
            diction_command["Audioext"] = ""
            array.append(on_batch)
            
        elif self.checkbox_batch.GetValue() is False:
            # change the layout of open/save file/dir
            self.siz1_staticbox.SetLabel("Import/Export Audio Media File")
            self.button_open.SetBackgroundColour(wx.Colour(223, 220, 217))
            self.button_save.SetBackgroundColour(wx.Colour(223, 220, 217))
            self.text_group_ext.Disable(), self.label_group.Disable() # estensione gruppo
            self.text_group_ext.SetValue(""), self.text_path_open.SetValue("")
            self.text_path_save.SetValue("")
            
            # enable analyzes tools
            self.button_analyzes.Enable()
            self.text_dbMax.SetValue(""), self.text_dbMedium.SetValue("")
            self.text_dbMax.Enable(), self.text_dbMedium.Enable()
            self.label_limited.Enable(), self.label_dbMedium.Enable()
            self.button_analyzes.SetBackgroundColour(wx.Colour(255, 115, 106))# color
            
            # disable button_ok
            self.disable_button(), #self.checkbox_overwrite.Disable()
            #self.checkbox_overwrite.SetValue(False)
            
            # set dictionary and list
            diction_command["Audioext"] = ""
            diction_command["OutputDir"] = ""
            if "batch" in array:
                diction_command["Batch"] = "single file conversion"
                array.remove("batch")
                
    #------------------------------------------------------------------#
    def enter_group_ext(self, event):  
        
        diction_command['ext_input'] = self.text_group_ext.GetValue()
        diction_command["Audioext"] = self.text_group_ext.GetValue()
        
    #------------------------------------------------------------------#
    def on_adv_opt(self, event):
        if self.checkbox_adv_opt.GetValue() is True:
            self.label_limit.Enable(), self.spin_ctrl_limit.Enable()
            
        elif self.checkbox_adv_opt.GetValue() is False:
            self.label_limit.Disable(), self.spin_ctrl_limit.Disable() 
            self.spin_ctrl_limit.SetValue(0.0)
            diction_command["limited"] = "0.0"
            
    #------------------------------------------------------------------#
    def on_audio_analyzes(self, event):  
        
        filepath = self.text_path_open.GetValue()
        # I call os_processing.proc_volumedetect:
        alias_vol = proc_volumedetect(filepath, self.ffmpeg_link)
        self.text_dbMax.SetValue(""), self.text_dbMedium.SetValue("")
        
        if  alias_vol == None:
            return
        else:
            #max_volume = alias_vol[0]
            #mean_volume = alias_vol[1]
            maxvol_append = alias_vol[2]
            medvol_append = alias_vol[3]
            #raw_list = alias_vol[4]
            self.text_dbMax.AppendText(maxvol_append)
            self.text_dbMedium.AppendText(medvol_append)
                        
            #normalize = self.spin_ctrl_audionormalize.GetValue()
            #maxvol = raw_list[max_volume + 1]
            #result = float(maxvol) - float(normalize)
            #result_str = str(result)
            #diction_command["Normalize"] = "-af volume=%sdB" % (result_str[1:])
            
    #------------------------------------------------------------------#
    def enter_normalize(self, event): 
        diction_command["Normalize"] = self.spin_ctrl_audionormalize.GetValue()
        
    #------------------------------------------------------------------#
    def enter_limit(self, event):
        diction_command["limited"] = self.spin_ctrl_limit.GetValue() 
        
    #------------------------------------------------------------------#
    def on_close(self, event):
        #self.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()
        self.parent.Show()
        event.Skip() # need if destroy from parent
        
    #------------------------------------------------------------------#
    def on_help(self, event):
        webbrowser.open('%s/08-Normalizzazione_audio.html' % self.helping)
        
    #------------------------------------------------------------------#
    def on_ok(self, event):
        
        logname = 'Videomass_Normalizer.log'
        #overwrite = ""
        
        if self.ffmpeg_log == 'true':
            report = '-report'
        else:
            report = ''
            
        #if self.checkbox_overwrite.GetValue() is True:
                #overwrite = "-y"
        #else:

        if 'batch' in array:########### BATCH MODE ACTIVE
        
            #if self.text_path_open.GetValue() == self.text_path_save.GetValue():
                #wx.MessageBox("Warning, you're overwriting all the imported "
                                #"files. if you want to allow overwriting must "
                                #"first check the checkbox on, otherwise change "
                                #"the name or destination directory.", 'Warning', 
                                #wx.ICON_EXCLAMATION, self)
                #return
            
            if self.text_group_ext.GetValue() == "":
                wx.MessageBox("Please, write in the box 'Group' the extension "
                              "of the files to be processed.", 'Warning', 
                              wx.ICON_EXCLAMATION, self)
                return

            valupdate = self.update_dict()
            ending = Formula(self, valupdate[0], valupdate[1])
            
            if ending.ShowModal() == wx.ID_OK:
                log_command = "%s -i '%s' -loglevel error -af volume=%sdB %s '%s'" % (
                                            self.ffmpeg_link,
                                            self.text_path_open.GetValue(), 
                                            diction_command["Normalize"], 
                                            self.threads,
                                            self.text_path_save.GetValue(),
                                                        )
                status = control_errors(self.text_path_open.GetValue(),
                                            diction_command["ext_input"],
                                            log_command, logname,
                                        )
                if  status[0] == 'no_error':
                    proc_normalize_batch_thread(
                                    status[2], self.text_path_open.GetValue(), 
                                    self.threads, diction_command["ext_input"], 
                                    self.text_path_save.GetValue(), 
                                    diction_command["limited"],
                                    diction_command["Normalize"], logname,
                                    self.ffmpeg_link
                                    )
                    dial = ProgressDialog(self, 'Videomass Batch Process', 
                                        status[1], diction_command["ext_input"],
                                        diction_command["Audioext"],
                                        self.text_path_save.GetValue(),
                                        self.path_log, self.command_log,
                                        logname
                                            )
        else:  ########### STANDARD MODE
            inputfile = self.text_path_open.GetValue() 
            outputfile = diction_command["OutputDir"]
            limited = diction_command["limited"]
            normalize = diction_command["Normalize"]
            
            alias_vol = proc_volumedetect(inputfile, self.ffmpeg_link) # chiamo os_processing.proc_volumedetect
            raw_list = alias_vol[4] # list
            index_maxvol = alias_vol[0] # index int of raw_list
            maxvol = raw_list[index_maxvol + 1] # next item of index
            
            if float(maxvol) < limited: # if vol max is inferior at..
                result = float(maxvol) - float(normalize)
                result_str = str(result)
                command = ("%s %s -i '%s' -loglevel %s "
                        "-af volume=%sdB %s -y '%s'" % (self.ffmpeg_link, 
                        report, inputfile, self.loglevel_type, 
                        result_str[1:], self.threads, outputfile)
                        )
            valupdate = self.update_dict()
            ending = Formula(self, valupdate[0], valupdate[1])
            
            if ending.ShowModal() == wx.ID_OK:
                dial = proc_std(self, 'Videomass - Run single process', command,
                                self.path_log, logname, self.command_log,
                                )
                
#------------------------------------------------------------------#
    def update_dict(self):
        """
        This method is required for update all diction_command
        dictionary values before send at epilogue
        """
        formula = (u"FORMULATIONS:\n\nConversion Mode:\nOutput Audio Format:\
                   \nInput File Type:\nOutput File Type\nImporting dir/file:\
                   \nExporting dir/file:\nExtension Group:\ndB Amplitude/Norm.:\
                   \ndB Limitation:"
                   )
        dictions = ("\n\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s" % (
                    diction_command["Batch"],diction_command["Audioext"], 
                    diction_command["Audioext"], diction_command["Audioext"],
                    diction_command["InputDir"],diction_command["OutputDir"], 
                    diction_command['ext_input'],diction_command["Normalize"], 
                    diction_command["limited"])
                    )
        return formula, dictions
    
