#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Fri Aug  9 06:53:14 2013
#
#########################################################
# Name: videograbframe.py
# Porpose: extract images (frames) from each video
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (05) 20/07/2014
# Rev (06) 15/03/2015
#########################################################

import wx
import os
import webbrowser
#import wx.lib.masked as masked
from os_processing import proc_std
from epilogue import Formula

dirname = os.path.expanduser('~/') # /home/user/

# create need list, dict:
diction_command = {"InputDir":"" ,"OutputDir":"", "cutstart":"", 
                "cutlengh":"", "frame":""
                   }

class VideoFrame(wx.Dialog):
    """
    Extract one range  of jpg images by setting the start time, 
    progress time and amount of frames of the movie. 
    """
    def __init__(self, parent, helping, ffmpeg_link, threads, ffmpeg_log, 
                command_log, path_log, loglevel_type):

        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        
        self.helping = helping
        self.ffmpeg_link = ffmpeg_link
        self.threads = threads
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.parent = parent
        self.parent.Hide()
        
        #self.time_start = masked.TimeCtrl(self, wx.ID_ANY, "0")
        #siz1_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Tempo Inizio (h:m:s)"))
        #self.time_lengh = masked.TimeCtrl(self, wx.ID_ANY, "")
        #siz2_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Tempo Avanzamento (h:m:s)"))
        
        self.start_hour_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=23, style=wx.TE_PROCESS_ENTER)
        self.start_minute_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=59, style=wx.TE_PROCESS_ENTER)
        self.start_second_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=59, style=wx.TE_PROCESS_ENTER)
        siz1_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Start Time (h:m:s)"))
        
        self.stop_hour_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=23, style=wx.TE_PROCESS_ENTER)
        self.stop_minute_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=59, style=wx.TE_PROCESS_ENTER)
        self.stop_second_ctrl = wx.SpinCtrl(self, wx.ID_ANY, "0", min=0, max=59, style=wx.TE_PROCESS_ENTER)
        siz2_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Time Progress (h:m:s)"))
        
        
        self.combo_box = wx.ComboBox(self, wx.ID_ANY, choices=[("0.2"), ("0.5"), 
                ("1"), ("1.5"), ("2")], style=wx.CB_DROPDOWN | wx.CB_READONLY
                )
        siz3_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Frame-Rate "
                                "(frequency frames capture)"))
        btn_open = wx.Button(self, wx.ID_OPEN, "")
        self.text_open = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER)
        self.btn_save = wx.Button(self, wx.ID_SAVE, "")
        self.text_save = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER)
        siz4_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Import/Export Media Movie"))
        btn_close = wx.Button(self, wx.ID_CANCEL, "Close")
        btn_help = wx.Button(self, wx.ID_HELP, "")
        self.btn_ok = wx.Button(self, wx.ID_OK, "Start")
        
        self.btn_save.Disable()

        #----------------------Properties----------------------#
        self.SetTitle("Saves Video Images")
        self.SetSize((449, 529))
        self.start_hour_ctrl.SetMinSize((45, 20))
        self.start_minute_ctrl.SetMinSize((45, 20))
        self.start_second_ctrl.SetMinSize((45, 20))
        self.start_hour_ctrl.SetToolTipString("Select movie point where to "
                                 "start extracting images (time hours format)"
                                              )
        self.start_minute_ctrl.SetToolTipString("Select movie point where to "
                                 "start extracting images (time minute format)"
                                                )
        self.start_second_ctrl.SetToolTipString("Select movie point where to "
                                 "start extracting images (time seconds format)"
                                                 )
        self.stop_hour_ctrl.SetMinSize((45, 20))
        self.stop_minute_ctrl.SetMinSize((45, 20))
        self.stop_second_ctrl.SetMinSize((45, 20))
        self.stop_hour_ctrl.SetToolTipString("Sets the total amount of time "
                                             "progress, starting from the start "
                                             "time extraction  "
                                             )
        self.combo_box.SetMinSize((100, 23))
        self.combo_box.SetSelection(0)
        self.combo_box.SetToolTipString("Choose from this list the frequency "
                                      "of the extracted images. More is low, "
                                      "the lower will be the extracted images."
                                        )
        self.text_open.SetMinSize((230, 21))
        self.text_open.SetToolTipString("Imported path-name (read only)")
        self.text_save.SetMinSize((230, 21))
        self.text_save.SetToolTipString("Exported path-name (read only)")
        self.btn_ok.Disable()
        self.btn_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
        self.btn_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        self.init_hour = '00'
        self.init_minute = '00'
        self.init_second = '00'
        self.cut_hour = '00'
        self.cut_minute = '00'
        self.cut_second = '00'

        #----------------------handle layout----------------------#
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_1 = wx.GridSizer(2, 1, 0, 0)
        grid_sizer_4 = wx.GridSizer(2, 1, 0, 0)
        grid_sizer_6 = wx.GridSizer(1, 3, 0, 0)
        siz4_staticbox.Lower()
        sizer_8 = wx.StaticBoxSizer(siz4_staticbox, wx.VERTICAL)
        grid_sizer_5 = wx.FlexGridSizer(2, 2, 0, 0)
        grid_sizer_2 = wx.GridSizer(2, 1, 0, 0)
        siz3_staticbox.Lower()
        sizer_7 = wx.StaticBoxSizer(siz3_staticbox, wx.VERTICAL)
        grid_sizer_3 = wx.GridSizer(1, 2, 0, 0)
        siz2_staticbox.Lower()
        sizer_6 = wx.StaticBoxSizer(siz2_staticbox, wx.HORIZONTAL)
        siz1_staticbox.Lower()
        sizer_5 = wx.StaticBoxSizer(siz1_staticbox, wx.HORIZONTAL)
        #sizer_5.Add(self.time_start, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_5.Add(self.start_hour_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_5.Add(self.start_minute_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_5.Add(self.start_second_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_3.Add(sizer_5, 1, wx.ALL | wx.EXPAND, 15)
        #sizer_6.Add(self.time_lengh, 0, wx.TOP | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_6.Add(self.stop_hour_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_6.Add(self.stop_minute_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_6.Add(self.stop_second_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_3.Add(sizer_6, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_2.Add(grid_sizer_3, 1, wx.EXPAND, 0)
        sizer_7.Add(self.combo_box, 0, wx.TOP | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_2.Add(sizer_7, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_1.Add(grid_sizer_2, 1, wx.EXPAND, 0)
        grid_sizer_5.Add(btn_open, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_5.Add(self.text_open, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_5.Add(self.btn_save, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_5.Add(self.text_save, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_8.Add(grid_sizer_5, 1, wx.EXPAND, 0)
        grid_sizer_4.Add(sizer_8, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_6.Add(btn_close, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_6.Add(btn_help, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_6.Add(self.btn_ok, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_4.Add(grid_sizer_6, 1, wx.EXPAND, 0)
        grid_sizer_1.Add(grid_sizer_4, 1, wx.EXPAND, 0)
        sizer_1.Add(grid_sizer_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()

        #----------------------Binding (EVT)----------------------#
        self.Bind(wx.EVT_SPINCTRL, self.start_hour, self.start_hour_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.start_minute, self.start_minute_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.start_second, self.start_second_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.stop_hour, self.stop_hour_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.stop_minute, self.stop_minute_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.stop_second, self.stop_second_ctrl)
        #self.Bind(wx.EVT_SPINCTRL, self.on_time_init, self.time_start)
        #self.Bind(wx.EVT_SPINCTRL, self.on_time_finish, self.time_lengh)
        self.Bind(wx.EVT_COMBOBOX, self.on_choice, self.combo_box)
        self.Bind(wx.EVT_BUTTON, self.on_open, btn_open)
        self.Bind(wx.EVT_TEXT, self.enter_open, self.text_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.btn_save)
        self.Bind(wx.EVT_TEXT, self.enter_save, self.text_save)
        self.Bind(wx.EVT_BUTTON, self.on_close, btn_close)
        self.Bind(wx.EVT_BUTTON, self.on_help, btn_help)
        self.Bind(wx.EVT_BUTTON, self.on_ok, self.btn_ok)

    #----------------------Event handler (callback)----------------------#
    def start_hour(self, event):
        self.init_hour = '%s' % self.start_hour_ctrl.GetValue()
        if len(self.init_hour) == 1:
            self.init_hour = '0%s' % self.start_hour_ctrl.GetValue()
    #------------------------------------------------------------------#
    def start_minute(self, event):
        self.init_minute = '%s' % self.start_minute_ctrl.GetValue()
        if len(self.init_minute) == 1:
            self.init_minute = '0%s' % self.start_minute_ctrl.GetValue()
    #------------------------------------------------------------------#
    def start_second(self, event):
        self.init_second = '%s' % self.start_second_ctrl.GetValue()
        if len(self.init_second) == 1:
            self.init_second = '0%s' % self.start_second_ctrl.GetValue()
    #------------------------------------------------------------------#
    def stop_hour(self, event):
        self.cut_hour = '%s' % self.stop_hour_ctrl.GetValue()
        if len(self.cut_hour) == 1:
            self.cut_hour = '0%s' % self.stop_hour_ctrl.GetValue()
    #------------------------------------------------------------------#
    def stop_minute(self, event):
        self.cut_minute = '%s' % self.stop_minute_ctrl.GetValue()
        if len(self.cut_minute) == 1:
            self.cut_minute = '0%s' % self.stop_minute_ctrl.GetValue()
    #------------------------------------------------------------------#
    def stop_second(self, event):
        self.cut_second = '%s' % self.stop_second_ctrl.GetValue()
        if len(self.cut_second) == 1:
            self.cut_second = '0%s' % self.stop_second_ctrl.GetValue()
    #------------------------------------------------------------------#
    def on_choice(self, event):
        diction_command["frame"] = "%s" %(self.combo_box.GetValue())
    #------------------------------------------------------------------#
    def on_open(self, event):
        dialfile = wx.FileDialog(self, "Importing Movie", ".", "", "*.*", 
                                    wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
                                  )
        if dialfile.ShowModal() == wx.ID_OK:
            self.text_open.SetValue("")
            self.text_open.AppendText(dialfile.GetPath())
            self.btn_save.Enable()
            #diction_command["Pathname"] = "%s" % (dialfile.GetPath())
            #diction_command["Dirname"] = "%s" % (dialfile.GetDirectory())
            #diction_command["Filename"] = "%s" % (dialfile.GetFilename()
            dialfile.Destroy()
    #------------------------------------------------------------------#
    def enter_open(self, event):
        
        diction_command["InputDir"] = self.text_open.GetValue()
    #------------------------------------------------------------------#
    def on_save(self, event):
        dialdir = wx.DirDialog(self, "Where do you want to save images?")
        
        if dialdir.ShowModal() == wx.ID_OK:
            self.text_save.SetValue("")
            self.text_save.AppendText(dialdir.GetPath())
            dialdir.Destroy()
    #------------------------------------------------------------------#
    def enter_save(self, event):
        diction_command["OutputDir"] = self.text_save.GetValue()
        
        if self.text_save.GetValue() == '':
            self.btn_ok.Disable()
            self.btn_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
            self.btn_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        else:
            self.btn_ok.Enable()
            self.btn_ok.SetBackgroundColour(wx.Colour(180, 214, 255))
            self.btn_ok.SetForegroundColour(wx.Colour(130, 51, 27))
    #------------------------------------------------------------------#
    def on_close(self, event):
        #self.Destroy()
        self.parent.Show()
        event.Skip() # need if destroy from parent
    #------------------------------------------------------------------#
    def on_help(self, event):
        
        #wx.MessageBox("L'help contestuale é ancora in fase di sviluppo .")
        webbrowser.open('%s/10-Estrarre_immagini.html' % self.helping)
    #------------------------------------------------------------------#
    def on_ok(self, event):
        logname = 'Videomass_VideoGrabFrame.log'
        
        if self.ffmpeg_log == 'true':
            report = '-report'
        else:
            report = ''
            
        diction_command["cutstart"] = "%s:%s:%s" % (self.init_hour, self.init_minute, 
                                                    self.init_second
                                                    )
        diction_command["cutlengh"] = "%s:%s:%s" % (self.cut_hour, self.cut_minute, 
                                                    self.cut_second
                                                    )
        diction_command["frame"] = self.combo_box.GetValue()
        fileout = "image%d.jpg"
        command = ("%s %s -i '%s' -loglevel %s -ss %s -t %s -r %s "
                "-an %s -y '%s/%s'" % (self.ffmpeg_link, report, 
                    self.text_open.GetValue(), self.loglevel_type, 
                    diction_command["cutstart"], diction_command["cutlengh"], 
                    self.combo_box.GetValue(), self.threads, 
                    self.text_save.GetValue(), fileout)
                     )
        formula = ("FORMULATIONS:\n\nConversion Mode:\
                \nStart cutting:\nSequence Duration:\nFrame\nInput dir/file:\
                \nOutput dir/file:"
                    )
        dictions = ("\n\n%s\n%s\n%s\n%s\n%s\n%s" % ("Single file", 
                    diction_command["cutstart"],diction_command["cutlengh"],
                    diction_command["frame"],diction_command["InputDir"],
                    diction_command["OutputDir"])
                    )
        ending = Formula(self,formula,dictions)
        
        if ending.ShowModal() == wx.ID_OK:
            dial = proc_std(self, 'Videomass - Run single process', command,
                            self.path_log, logname, self.command_log,
                            )
            
