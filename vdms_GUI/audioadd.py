#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Friday Aug 23 10:37:47 2013
#
#########################################################
# Name: audioadd.py
# Porpose: add audio track on video
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (04) 20/07/2014
# Rev (05) 11/03/2015
# Rev (06) 30/04/2015
#########################################################

import wx
import os
import webbrowser
from os_processing import proc_std
import audiodialogs
from epilogue import Formula

dirname = os.path.expanduser('~/') # /home/user/

diction_command = {"ext_input":"", "ImportFile":"", "InputDir":"",
                "OutputDir":"", "Pathname":"", "Dirname":"", 
                "Filename":"", "Audioext":"ogg", "Audio":"", 
                "AudioCodec":"-acodec libvorbis", "AudioChannel":"-ac 2", 
                "AudioRate":"-ar 44100", "AudioBitrate":"-aq 3"
                }

class AudioAdd(wx.Dialog):
    """
    Add new audio stream in some video format and permit to convert 
    audio in some other format
    """
    def __init__(self, parent, helping, ffmpeg_link, threads, ffmpeg_log, 
            command_log, path_log, loglevel_type):

        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        
        self.helping = helping
        self.ffmpeg_link = ffmpeg_link
        self.threads = threads
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.parent = parent
        self.parent.Hide()
        
        self.sel = None # for radiobox GetSelection()
        
        self.nb1 = wx.Notebook(self, wx.ID_ANY, style=0)
        self.nb1_p1 = wx.Panel(self.nb1, wx.ID_ANY)
        btn_import = wx.Button(self.nb1_p1, wx.ID_ANY, "Import")
        self.text_import = wx.TextCtrl(self.nb1_p1, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER)
        s1 = wx.StaticBox(self.nb1_p1, wx.ID_ANY, ("Importing audio track"))
        btn_open = wx.Button(self.nb1_p1, wx.ID_OPEN, "")
        self.text_open = wx.TextCtrl(self.nb1_p1, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER | wx.TE_READONLY)
        self.btn_save = wx.Button(self.nb1_p1, wx.ID_SAVE, "")
        self.text_save = wx.TextCtrl(self.nb1_p1, wx.ID_ANY, "", style=wx.TE_PROCESS_ENTER | wx.TE_READONLY)
        s2 = wx.StaticBox(self.nb1_p1, wx.ID_ANY, ("Import/Export Media Movie"))
        self.nb1_p2 = wx.Panel(self.nb1, wx.ID_ANY)
        self.rbx_a = wx.RadioBox(self.nb1_p2, wx.ID_ANY, 
                            ("Convert imported audio stream:"), choices=[
                            ("Not convert the audio track imported"),
                            ("Wav (Raw, No_MultiChannel)"), 
                            ("Flac (Lossless, No_MultiChannel)"), 
                            ("Aac (Lossy, MultiChannel)"), 
                            ("Alac (Lossless, m4v, No_MultiChannel)"), 
                            ("Ac3 (Lossy, MultiChannel)"), 
                            ("Ogg (Lossy, No_MultiChannel)"), 
                            ("Mp3 (Lossy, No_MultiChannel)"),], 
                            majorDimension=0, style=wx.RA_SPECIFY_ROWS
                                        )
        btn_close = wx.Button(self, wx.ID_CANCEL, "Close")
        btn_help = wx.Button(self, wx.ID_HELP, "")
        btn_ok = wx.Button(self, wx.ID_OK, "Start")
        
        diction_command["InputDir"] = ""
        diction_command["OutputDir"] = ""
        diction_command["ImportFile"] = ""
        self.btn_save.Disable()
        self.nb1_p2.Disable()

        #----------------------Properties----------------------#
        self.SetTitle("Add Audio Stream to a Movie")
        btn_import.SetBackgroundColour(wx.Colour(122, 239, 255))
        btn_import.SetToolTipString("Importing audio track for the video")
        self.text_import.SetMinSize((230, 21))
        self.text_import.SetToolTipString("Audio track path-name")
        self.text_open.SetMinSize((230, 21))
        self.text_open.SetToolTipString("Imported path-name (read only, not editable)")
        self.text_save.SetMinSize((230, 21))
        self.text_save.SetToolTipString("Exported path-name (read only, not editable)")
        self.rbx_a.SetToolTipString("Audio codec selection")
        self.rbx_a.SetSelection(0)
        btn_ok.SetBackgroundColour(wx.Colour(180, 214, 255))
        btn_ok.SetForegroundColour(wx.Colour(130, 51, 27))

        #----------------------Layout----------------------#
        gr_s5 = wx.FlexGridSizer(2, 1, 0, 0)
        gr_s3 = wx.GridSizer(1, 3, 0, 0)
        siz5 = wx.BoxSizer(wx.VERTICAL)
        gr_s2 = wx.GridSizer(2, 1, 0, 0)
        s2.Lower()
        siz4 = wx.StaticBoxSizer(s2, wx.VERTICAL)
        gr_s1 = wx.FlexGridSizer(2, 2, 0, 0)
        gr_s4 = wx.GridSizer(2, 1, 0, 0)
        s1.Lower()
        siz1 = wx.StaticBoxSizer(s1, wx.VERTICAL)
        gr_s6 = wx.FlexGridSizer(1, 2, 0, 0)
        gr_s6.Add(btn_import, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        gr_s6.Add(self.text_import, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        siz1.Add(gr_s6, 1, wx.EXPAND, 0)
        gr_s4.Add(siz1, 1, wx.ALL | wx.EXPAND, 15)
        gr_s2.Add(gr_s4, 1, wx.EXPAND, 0)
        gr_s1.Add(btn_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        gr_s1.Add(self.text_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        gr_s1.Add(self.btn_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        gr_s1.Add(self.text_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        siz4.Add(gr_s1, 1, wx.EXPAND, 0)
        gr_s2.Add(siz4, 1, wx.ALL | wx.EXPAND, 15)
        self.nb1_p1.SetSizer(gr_s2)
        siz5.Add(self.rbx_a, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 30)
        self.nb1_p2.SetSizer(siz5)
        self.nb1.AddPage(self.nb1_p1, ("Files Management"))
        self.nb1.AddPage(self.nb1_p2, ("Audio Codec Selection"))
        gr_s5.Add(self.nb1, 1, wx.ALL | wx.EXPAND, 5)
        gr_s3.Add(btn_close, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        gr_s3.Add(btn_help, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        gr_s3.Add(btn_ok, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        gr_s5.Add(gr_s3, 1, wx.ALL | wx.EXPAND, 15)
        self.SetSizer(gr_s5)
        gr_s5.Fit(self)
        self.Layout()

        #----------------------Binders (EVT)----------------------#
        self.Bind(wx.EVT_BUTTON, self.on_import, btn_import)
        self.Bind(wx.EVT_TEXT, self.enter_import, self.text_import)
        self.Bind(wx.EVT_BUTTON, self.on_open, btn_open)
        self.Bind(wx.EVT_TEXT, self.enter_open, self.text_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.btn_save)
        self.Bind(wx.EVT_TEXT, self.enter_save, self.text_save)
        self.Bind(wx.EVT_RADIOBOX, self.choice_audio, self.rbx_a)
        self.Bind(wx.EVT_BUTTON, self.on_close, btn_close)
        self.Bind(wx.EVT_BUTTON, self.on_help, btn_help)
        self.Bind(wx.EVT_BUTTON, self.on_ok, btn_ok)

    #----------------------Event handler (callback)----------------------#
    def on_import(self, event):
        """
        Import some audio format included in the wildcard to add in 
        some other movie. The audio was imported can be convert in 
        other audio format
        """
        wildcard = ("Audio source (*.wav; *.aiff; *.mp3; *.m4a; *.ogg; "
                    "*.flac; *.ac3; *.aac;)|*.wav;*.aiff;*.mp3;*.m4a;"
                    "*.ogg;*.flac;*.ac3;*.aac;|" "All files (*.*)|*.*"
                    )
        dialfile = wx.FileDialog(self, "Import the audio track", ".", "", 
                                 wildcard, wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
                                  )
        
        if dialfile.ShowModal() == wx.ID_OK:
            self.text_import.SetValue("")
            self.text_import.AppendText(dialfile.GetPath())
            #diction_command["Pathname"] = "%s" % (dialfile.GetPath())
            #diction_command["Dirname"] = "%s" % (dialfile.GetDirectory())
            #diction_command["Filename"] = "%s" % (dialfile.GetFilename()
            dialfile.Destroy()
            
    #------------------------------------------------------------------#
    def enter_import(self, event):
        
        diction_command["ImportFile"] = self.text_import.GetValue()
        
    #------------------------------------------------------------------#
    def on_open(self, event):
        """
        Import some movie with a video format included in the wildcard.
        The video extension of the imported media is used for assignement
        in the video output. This controls the audio conversion only to 
        formats compatible with the type of video format imported
        """
        wildcard = ("Video source (*.avi; *.mkv; *.webm; *.mpg; *.mp4; "
                    "*.m4v; *.flv; *.ogg; *.mov; *.wmv)|*.avi;*.mkv;*.webm;"
                    "*.mpg;*.mp4;*.m4v;*.flv;*.ogg;*.mov;*.wmv|" "All files "
                    "(*.*)|*.*")
        
        dialfile = wx.FileDialog(self, "Select a video file to add sound ",
                         ".", "", wildcard, wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        
        if dialfile.ShowModal() == wx.ID_OK:
            self.btn_save.Enable(), self.nb1_p2.Enable(), 
            self.text_open.SetValue(""), self.text_save.SetValue(""), 
            self.text_open.AppendText(dialfile.GetPath())
            fileName, fileExtension = os.path.splitext(dialfile.GetPath())
            diction_command["ext_input"] = fileExtension.split(".")[-1]
            ext = diction_command["ext_input"]
            
            #diction_command["Pathname"] = "%s" % (dialfile.GetPath())
            #diction_command["Dirname"] = "%s" % (dialfile.GetDirectory())
            #diction_command["Filename"] = "%s" % (dialfile.GetFilename()

            if ext == "avi" or ext == "AVI":
                self.rbx_a.EnableItem(0,enable=True)# not convert
                self.rbx_a.EnableItem(1,enable=True)# wav
                self.rbx_a.EnableItem(5,enable=True)# ac3
                self.rbx_a.EnableItem(7,enable=True)# mp3
                self.rbx_a.EnableItem(2,enable=False)
                self.rbx_a.EnableItem(3,enable=False)
                self.rbx_a.EnableItem(4,enable=False)
                self.rbx_a.EnableItem(6,enable=False)
                self.rbx_a.SetSelection(1)
                diction_command["AudioCodec"] = "-acodec pcm_s16le"
                diction_command["Audioext"] = "wav"
                diction_command["AudioBitrate"] = ""
                diction_command["AudioChannel"] = "-ac 2"
                diction_command["AudioRate"] = "-ar 44100"
                
            elif ext == "mp4" or ext == "MP4":
                self.rbx_a.EnableItem(0,enable=True)# not convert
                self.rbx_a.EnableItem(1,enable=False)
                self.rbx_a.EnableItem(5,enable=True)# ac3
                self.rbx_a.EnableItem(7,enable=True)# mp3
                self.rbx_a.EnableItem(2,enable=False)
                self.rbx_a.EnableItem(3,enable=True)# aac
                self.rbx_a.EnableItem(4,enable=False)
                self.rbx_a.EnableItem(6,enable=False)
                self.rbx_a.SetSelection(3)
                diction_command["AudioCodec"] = "-acodec libfaac"
                diction_command["Audioext"] = "aac"
                diction_command["AudioBitrate"] = "-ab 192k"
                diction_command["AudioChannel"] = "-ac 2"
                diction_command["AudioRate"] = "-ar 44100"
                
            elif ext == "webm" or ext == "WEBM":
                self.rbx_a.EnableItem(0,enable=True)# not convert
                self.rbx_a.EnableItem(1,enable=False)# wav
                self.rbx_a.EnableItem(5,enable=False)# ac3
                self.rbx_a.EnableItem(7,enable=False)# mp3
                self.rbx_a.EnableItem(2,enable=False)# flac
                self.rbx_a.EnableItem(3,enable=False)# aac
                self.rbx_a.EnableItem(4,enable=False)# alac, m4a
                self.rbx_a.EnableItem(6,enable=True)# ogg
                self.rbx_a.SetSelection(6)
                diction_command["AudioCodec"] = "-acodec libvorbis"
                diction_command["Audioext"] = "ogg"
                diction_command["AudioBitrate"] = "-aq 6"
                diction_command["AudioChannel"] = "-ac 2"
                diction_command["AudioRate"] = "-ar 44100"
                
            elif ext == "mkv" or ext == "MKV":
                self.rbx_a.EnableItem(0,enable=True)# not convert
                self.rbx_a.EnableItem(1,enable=True)# wav
                self.rbx_a.EnableItem(5,enable=True)# ac3
                self.rbx_a.EnableItem(7,enable=True)# mp3
                self.rbx_a.EnableItem(2,enable=True)# flac
                self.rbx_a.EnableItem(3,enable=True)# aac
                self.rbx_a.EnableItem(4,enable=False)# alac, m4a
                self.rbx_a.EnableItem(6,enable=True)# ogg
                self.rbx_a.SetSelection(6)
                diction_command["AudioCodec"] = "-acodec libvorbis"
                diction_command["Audioext"] = "ogg"
                diction_command["AudioBitrate"] = "-aq 6"
                diction_command["AudioChannel"] = "-ac 2"
                diction_command["AudioRate"] = "-ar 44100"
                
            elif ext == "flv" or ext == "FLV":
                self.rbx_a.EnableItem(0,enable=True)# not convert
                self.rbx_a.EnableItem(1,enable=False)# wav
                self.rbx_a.EnableItem(5,enable=True)# ac3
                self.rbx_a.EnableItem(7,enable=True)# mp3
                self.rbx_a.EnableItem(2,enable=False)# flac
                self.rbx_a.EnableItem(3,enable=True)# aac
                self.rbx_a.EnableItem(4,enable=False)# alac, m4a
                self.rbx_a.EnableItem(6,enable=False)# ogg
                self.rbx_a.SetSelection(7)
                diction_command["AudioCodec"] = "-acodec libmp3lame"
                diction_command["Audioext"] = "mp3"
                diction_command["AudioBitrate"] = "-ab 260k"
                diction_command["AudioChannel"] = "-ac 2"
                diction_command["AudioRate"] = "-ar 44100"
                
            elif ext == "m4v" or ext == "M4V":
                self.rbx_a.EnableItem(0,enable=True)# not convert
                self.rbx_a.EnableItem(1,enable=False)# wav
                self.rbx_a.EnableItem(5,enable=False)# ac3
                self.rbx_a.EnableItem(7,enable=False)# mp3
                self.rbx_a.EnableItem(2,enable=False)# flac
                self.rbx_a.EnableItem(3,enable=True)# aac
                self.rbx_a.EnableItem(4,enable=True)# alac, m4a
                self.rbx_a.EnableItem(6,enable=False)# ogg
                self.rbx_a.SetSelection(4)
                diction_command["AudioCodec"] = "-acodec libfaac"
                diction_command["Audioext"] = "aac"
                diction_command["AudioBitrate"] = "-ab 192k"
                diction_command["AudioChannel"] = "-ac 2"
                diction_command["AudioRate"] = "-ar 44100"
                
            elif ext == "ogg" or ext == "OGG":
                self.rbx_a.EnableItem(0,enable=True)# not convert
                self.rbx_a.EnableItem(1,enable=False)# wav
                self.rbx_a.EnableItem(5,enable=False)# ac3
                self.rbx_a.EnableItem(7,enable=False)# mp3
                self.rbx_a.EnableItem(2,enable=True)# flac
                self.rbx_a.EnableItem(3,enable=False)# aac
                self.rbx_a.EnableItem(4,enable=False)# alac, m4a
                self.rbx_a.EnableItem(6,enable=True)# ogg
                self.rbx_a.SetSelection(6)
                diction_command["AudioCodec"] = "-acodec libvorbis"
                diction_command["Audioext"] = "ogg"
                diction_command["AudioBitrate"] = "-aq 6"
                diction_command["AudioChannel"] = "-ac 2"
                diction_command["AudioRate"] = "-ar 44100"
                
            else:
                wx.MessageBox("The imported video format "
                        "is not still controlled by videomass and is not "
                        "guaranteed the success of the process. In any case, "
                        "you can proceed to do some tests", "Warning", wx.OK | 
                        wx.ICON_EXCLAMATION, self)
                
                self.rbx_a.EnableItem(0,enable=True)# not convert
                self.rbx_a.EnableItem(1,enable=True)# wav
                self.rbx_a.EnableItem(5,enable=True)# ac3
                self.rbx_a.EnableItem(7,enable=True)# mp3
                self.rbx_a.EnableItem(2,enable=True)# flac
                self.rbx_a.EnableItem(3,enable=True)# aac
                self.rbx_a.EnableItem(4,enable=True)# alac, m4a
                self.rbx_a.EnableItem(6,enable=True)# ogg
                self.rbx_a.SetSelection(7)
                diction_command["AudioCodec"] = "-acodec libmp3lame"
                diction_command["Audioext"] = "mp3"
                diction_command["AudioBitrate"] = "-ab 260k"
                diction_command["AudioChannel"] = "-ac 2"
                diction_command["AudioRate"] = "-ar 44100"
            
            self.sel = self.rbx_a.GetSelection()
            dialfile.Destroy()
            
    #------------------------------------------------------------------#
    def enter_open(self, event):
        diction_command["InputDir"] = self.text_open.GetValue()
        #event.Skip()

    #------------------------------------------------------------------#
    def on_save(self, event):
        """
        The output video is saved in the same format imported video
        """
        ext = diction_command["ext_input"]

        dialsave = wx.FileDialog(self, "Export Final Movie", 
                           "", "", "%s files (*.%s)|*.%s" % (ext,ext,ext), 
                           wx.SAVE | wx.OVERWRITE_PROMPT | wx.FD_CHANGE_DIR)
        
        if dialsave.ShowModal() == wx.ID_OK:
            path = dialsave.GetPath()
            if path.endswith(ext) != True:
                path = "%s.%s" % (path, ext)
            
            self.text_save.SetValue("")
            self.text_save.AppendText(path)
            dialsave.Destroy()
            
    #------------------------------------------------------------------#
    def enter_save(self, event):
        diction_command["OutputDir"] = self.text_save.GetValue()
        
    #------------------------------------------------------------------#
    def choice_audio(self, event):
        """
        The user choice the audio codec supported by some video imported
        """
        audio = self.rbx_a.GetStringSelection()
        
        if audio == "Not convert the audio track imported":
            diction_command["AudioCodec"] = "-acodec copy"
            diction_command["Audioext"] = ""
            diction_command["AudioChannel"] = ""
            diction_command["AudioRate"] = ""
            diction_command["AudioBitrate"] = ""

        elif audio == "Wav (Raw, No_MultiChannel)":
            self.audio_("wav", "Audio WAV Codec Parameters", 
                        "-acodec pcm_s16le", "wav")

        elif audio == "Flac (Lossless, No_MultiChannel)":
            self.audio_("flac", "Audio FLAC Codec Parameters", 
                        "-acodec flac", "flac")

        elif audio == "Aac (Lossy, MultiChannel)":
            self.audio_("aac", "Audio AAC Codec Parameters", 
                        "-acodec libfaac", "aac")

        elif audio == "Alac (Lossless, m4v, No_MultiChannel)":
            self.audio_("alac", "Audio ALAC (m4a) Codec Parameters", 
                        "-acodec alac", "m4a")

        elif audio == "Ac3 (Lossy, MultiChannel)":
            self.audio_("ac3", "Audio AC3 Codec Parameters", 
                        "-acodec ac3", "ac3")

        elif audio == "Ogg (Lossy, No_MultiChannel)":
            self.audio_("ogg", "Audio OGG Codec Parameters", 
                        "-acodec libvorbis", "ogg")

        elif audio == "Mp3 (Lossy, No_MultiChannel)": 
            self.audio_("mp3", "Audio MP3 Codec Parameters", 
                        "-acodec libmp3lame", "mp3")
        
    #-------------------------------------------------------------------#
    def audio_(self, audio_type, title, codec, ext):
        """
        Run a dialogs for audio Choice parameters
        """
        audiodialog = audiodialogs.AudioSettings(self,audio_type,title)
        retcode = audiodialog.ShowModal()
        
        if retcode == wx.ID_OK:
            #self.text_save.SetValue("")
            data = audiodialog.GetValue()
            diction_command["AudioCodec"] = codec
            diction_command["Audioext"] = ext
            diction_command["AudioChannel"] = data[0]
            diction_command["AudioRate"] = data[1]
            diction_command["AudioBitrate"] = data[2]
        else:
            self.rbx_a.SetSelection(self.sel)
        
        audiodialog.Destroy()

    #------------------------------------------------------------------#
    def on_close(self, event):
        self.parent.Show()
        #self.Destroy()
        event.Skip() # need if destroy from parent

    #------------------------------------------------------------------#
    def on_help(self, event):
        
        #wx.MessageBox("Spiacente, ma l'help contestuale é ancora in fase di "
                    # "sviluppato ...Abbiate pazienza !")
        webbrowser.open('%s12-Aggiungi_audio.html' % self.helping)
        
    #------------------------------------------------------------------#
    def on_ok(self, event):
        
        logname = 'Videomass_AudioAdd.log'
        
        if self.ffmpeg_log == 'true':
            report = '-report'
            
        else:
            report = ''

        if self.text_open.GetValue() == "": # inputdir vuoto
            wx.MessageBox("No files selected for conversion !","Warning", 
                          wx.OK | wx.ICON_EXCLAMATION, self)
            
        elif self.text_save.GetValue() == "": # outputdir vuoto
            wx.MessageBox("No name/path to save !", "Warning", wx.OK | 
                          wx.ICON_EXCLAMATION, self)
            
        elif self.text_import.GetValue() == "": # inputdir vuoto
            wx.MessageBox("No imported audio file !", "Warning", wx.OK | 
                             wx.ICON_EXCLAMATION, self)
            
        else:
            command = ("%s %s -i '%s' -i '%s' -loglevel %s -vcodec "
                    "copy %s %s %s %s %s -y '%s'" % (self.ffmpeg_link,
            report, self.text_import.GetValue(), self.text_open.GetValue(), 
            self.loglevel_type, diction_command["AudioCodec"], 
            diction_command["AudioBitrate"], diction_command["AudioChannel"], 
            diction_command["AudioRate"], self.threads, 
            self.text_save.GetValue())
                    )
            formula = (u"FORMULATIONS:\n\nConversion Mode:\
                       \nAudio format:\nAudio codec:\nAudio channel:\
                       \nAudio rate:\nAudio bit-rate:\nImported audio file:\
                       \nInput dir/file:\nOutput dir/file:")
            dictions = ("\n\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s" % (
                       "Single file only", diction_command["Audioext"], 
                       diction_command["AudioCodec"],
                       diction_command["AudioChannel"], 
                       diction_command["AudioRate"],
                       diction_command["AudioBitrate"],
                       diction_command['ImportFile'],
                       diction_command["InputDir"],
                       diction_command["OutputDir"]))
            
            ending = Formula(self, formula, dictions)
            
            if ending.ShowModal() == wx.ID_OK:
                dial = proc_std(self, 'Videomass - Run single process', command,
                                self.path_log, logname, self.command_log,
                                )
                
