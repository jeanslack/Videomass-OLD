#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Tue Aug 27 11:34:47 2013
#
#
#########################################################
# Name: video_sci
# Porpose: Intarface for edit and set parameters for video conversions
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (08) 04/08/2014
# Rev (09) 14/01/2015
# Rev (10) 10/03/2015
# Rev (11) 29/04/2015
#########################################################

import wx
import os
import string
import webbrowser
import wx.lib.agw.floatspin as FS
from os_processing import proc_batch_thread, ProgressDialog, proc_std,\
                          proc_volumedetect, play_input, control_errors
from epilogue import Formula
import  mediainfo, audiodialogs, infoprg, presets_addnew
                    
dirname = os.path.expanduser('~') # /home/user

# global list used for batch mode checkbox and others
array = []

"""
This series of dictionaries are used for define the generated choices 
from the events that pass at construction command string.
Some keys of diction_command (FormatChoice, VideoFormat and VideoCodec) 
has not empty values why start with codec MKV set default
"""
diction_command = {"FormatChoice":"MKV (h264)", "VideoFormat":"mkv", 
"VideoCodec":"-vcodec libx264", "ext_input":"", "Passing":"single", 
"testing":"", "InputDir":"", "OutputDir":"", "VideoSize":"", "VideoAspect":"", 
"VideoRate":"", "Presets":"", "Profile":"", "Tune":"", "Bitrate":"", "CRF":"", 
"Audio":"", "AudioCodec":"", "AudioChannel":"", "AudioRate":"", "Normalize":"",
"AudioBitrate":"", "scale":"", "deinterlace":"",
"interlace":"", "Batch":"single file conversion", "file":""}

diction_format = {"AVI (XVID mpeg4)":"avi", "AVI (FFmpeg mpeg4)":"avi", 
"AVI (ITU h264)":"avi", "MP4 (mpeg4)":"mp4", "MP4 (HQ h264/AVC)":"mp4", 
"M4V (HQ h264/AVC)":"m4v", "MKV (h264)":"mkv", "OGG theora":"ogg", 
"WebM (HTML5)":"webm", "FLV (HQ h264/AVC)":"flv"}

diction_codec = {"AVI (XVID mpeg4)":"-vcodec mpeg4 -vtag xvid", 
"AVI (FFmpeg mpeg4)":"-vcodec mpeg4", "AVI (ITU h264)":"-vcodec libx264",
"MP4 (mpeg4)":"-vcodec mpeg4", "MP4 (HQ h264/AVC)":"-vcodec libx264", 
"M4V (HQ h264/AVC)":"-vcodec libx264", "MKV (h264)":"-vcodec libx264",
"OGG theora":"-vcodec libtheora", "WebM (HTML5)":"-vcodec libvpx", 
"FLV (HQ h264/AVC)":"-vcodec libx264"}

# set widget colours in some case:
azure = '#d9ffff' # rgb form (wx.Colour(217,255,255))
orange = '#ff5f1a' # rgb form (wx.Colour(255,95,26))
yellow = '#faff35'
red = '#ff3a1f'

class Video_Sci(wx.Frame):
    """
    Interface for editing parameters to the videos media conversions
    """
    def __init__(self, parent, helping, ffmpeg_link, threads, command_log,
                path_log, loglevel_type, icon_play, icon_analyze, 
                icon_process, icon_help, icon_presets, videomass_icon, 
                OS, ffprobe_link, ffplay_link, ffmpeg_log, full_list, 
                path_srcShare, loglevel_batch):
        
        wx.Frame.__init__(self, None, -1, style=wx.DEFAULT_FRAME_STYLE)
        
        self.parent = parent
        self.ffmpeg_link = ffmpeg_link
        self.helping = helping
        self.threads = threads
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.icon_play = icon_play
        self.icon_analyze = icon_analyze
        self.icon_process = icon_process
        self.icon_help = icon_help
        self.icon_presets = icon_presets
        self.videomass_icon = videomass_icon
        self.OS = OS
        self.ffprobe_link = ffprobe_link
        self.ffplay_link = ffplay_link
        self.ffmpeg_log = ffmpeg_log
        self.full_list = full_list
        self.path_srcShare = path_srcShare
        self.loglevel_batch = loglevel_batch

        self.parent.Hide()

        self.panel_base = wx.Panel(self, wx.ID_ANY)
        self.menu_bar()
        self.tool_bar()
        self.sb = self.CreateStatusBar(1)
        self.notebook_1 = wx.Notebook(self.panel_base, wx.ID_ANY, style=0)
        self.notebook_1_pane_1 = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.combobox_videoformat = wx.ComboBox(self.notebook_1_pane_1, wx.ID_ANY,
        choices=[("AVI (XVID mpeg4)"), ("AVI (FFmpeg mpeg4)"), 
        ("AVI (ITU h264)"), ("MP4 (mpeg4)"), ("MP4 (HQ h264/AVC)"), 
        ("M4V (HQ h264/AVC)"), ("MKV (h264)"), ("OGG theora"), ("WebM (HTML5)"), 
        ("FLV (HQ h264/AVC)")], style=wx.CB_DROPDOWN | wx.CB_READONLY
        )
        self.sizer_combobox_formatv_staticbox = wx.StaticBox(self.notebook_1_pane_1, 
        wx.ID_ANY, ("Video Profile Selection")
        )
        self.button_open = wx.Button(self.notebook_1_pane_1, wx.ID_OPEN, "")
        self.text_path_open = wx.TextCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
        "", style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
        )
        self.button_save = wx.Button(self.notebook_1_pane_1, wx.ID_SAVE, "")
        self.text_path_save = wx.TextCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
        "", style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
        )
        self.sizer_dir_staticbox = wx.StaticBox(self.notebook_1_pane_1, 
        wx.ID_ANY, ('Import/Export Media Movie')
        )
        self.checkbox_batch = wx.CheckBox(self.notebook_1_pane_1, wx.ID_ANY, 
        ("Batch Mode")
        )
        self.checkbox_batch.SetValue(False) # setto in modo spento
        self.label_group = wx.StaticText(self.notebook_1_pane_1, wx.ID_ANY, 
        ("Group:")
        )
        self.text_group_ext = wx.TextCtrl(self.notebook_1_pane_1, wx.ID_ANY, "", 
        style=wx.TE_PROCESS_ENTER
        )
        self.checkbox_num_pass = wx.CheckBox(self.notebook_1_pane_1, wx.ID_ANY, 
        ("2-pass encoding.")
        )
        self.checkbox_num_pass.SetValue(False) # setto in modo spento
        self.checkbox_test = wx.CheckBox(self.notebook_1_pane_1, wx.ID_ANY, 
        ("Video Testing")
        )
        self.checkbox_test.SetValue(False) # setto in modo spento
        self.label_sec = wx.StaticText(self.notebook_1_pane_1, wx.ID_ANY, 
        ("Sec.  :")
        )
        self.spin_ctrl_test = wx.SpinCtrl(self.notebook_1_pane_1, wx.ID_ANY, "", 
        min=10, max=10000, style=wx.TE_PROCESS_ENTER
        )
        self.sizer_automations_staticbox = wx.StaticBox(self.notebook_1_pane_1, 
        wx.ID_ANY, ("Automations")
        )
        self.spin_ctrl_bitrate = wx.SpinCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
        "1500", min=0, max=25000, style=wx.TE_PROCESS_ENTER
        )
        self.sizer_bitrate_staticbox = wx.StaticBox(self.notebook_1_pane_1, 
        wx.ID_ANY, ("Video Bit-Rate Value")
        )
        self.slider_CRF = wx.Slider(self.notebook_1_pane_1, wx.ID_ANY, 1, 0, 51, 
        style=wx.SL_HORIZONTAL | wx.SL_AUTOTICKS | wx.SL_LABELS
        )
        self.sizer_crf_staticbox = wx.StaticBox(self.notebook_1_pane_1, 
        wx.ID_ANY, ("Video CRF Value")
        )
        self.notebook_1_pane_2 = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.checkbox_videosize = wx.CheckBox(self.notebook_1_pane_2, 
                                    wx.ID_ANY, ("Set Video Size"))
        self.label_width_1 = wx.StaticText(self.notebook_1_pane_2, wx.ID_ANY, 
                                    ("Video  Width        "))
        #self.label_sep = wx.StaticText(self.notebook_1_pane_2, wx.ID_ANY, 
                                    #("/"))
        self.label_height = wx.StaticText(self.notebook_1_pane_2, wx.ID_ANY, 
                    ("        Video  Heigth"))
        
        self.spin_size_width = wx.SpinCtrl(self.notebook_1_pane_2, wx.ID_ANY, 
        "0", min=0, max=10000, style=wx.TE_PROCESS_ENTER
        )
        self.label_x = wx.StaticText(self.notebook_1_pane_2, wx.ID_ANY, ("X"))
        self.spin_size_height = wx.SpinCtrl(self.notebook_1_pane_2, wx.ID_ANY,
        "0", min=0, max=10000, style=wx.TE_PROCESS_ENTER
        )
        self.checkbox_scale = wx.CheckBox(self.notebook_1_pane_2, 
                            wx.ID_ANY, ("Enable Scaling (Resizing)"))##
        self.checkbox_scale.SetValue(False)##
        self.label_width_2 = wx.StaticText(self.notebook_1_pane_2, wx.ID_ANY, 
                                        ("Video Width"), style=wx.ALIGN_CENTRE)###
        
        self.spin_size_scaling = wx.SpinCtrl(self.notebook_1_pane_2, wx.ID_ANY,
                        "0", min=0, max=10000, style=wx.TE_PROCESS_ENTER)##
        self.sizer_videosize_staticbox = wx.StaticBox(self.notebook_1_pane_2, 
        wx.ID_ANY, ("Video Size (Optional)")
        )
        self.checkbox_deinterlace = wx.CheckBox(self.notebook_1_pane_2, 
                                        wx.ID_ANY, (u"Deinterlaces")
                                        )
        self.radio_box_deinterlace = wx.RadioBox(self.notebook_1_pane_2, 
                            wx.ID_ANY, (u"Modality"), choices=[("complex"), 
                            ("simple")], majorDimension=0, 
                            style=wx.RA_SPECIFY_ROWS
                            )
        self.checkbox_interlace = wx.CheckBox(self.notebook_1_pane_2, 
                                        wx.ID_ANY, ("Interlaces   ")
                                        )
        self.radio_box_interlace = wx.RadioBox(self.notebook_1_pane_2, 
                                    wx.ID_ANY, ("Parameters interlacc."), 
                                    choices=[("Default"), ("Scan"), ("Lowpass")], 
                                    majorDimension=0, style=wx.RA_SPECIFY_ROWS
                                    )
        self.sizer_1_staticbox = wx.StaticBox(self.notebook_1_pane_2, 
                            wx.ID_ANY, (u"De-interlaced/Progressive video"))
        self.combobox_Vaspect = wx.ComboBox(self.notebook_1_pane_2, wx.ID_ANY,
        choices=[("Set default"), ("4:3"), ("16:9")], style=wx.CB_DROPDOWN | 
        wx.CB_READONLY
        )
        self.sizer_videoaspect_staticbox = wx.StaticBox(self.notebook_1_pane_2, 
                                        wx.ID_ANY, ("Video Aspect (Optional)")
                                        )
        self.combobox_Vrate = wx.ComboBox(self.notebook_1_pane_2, wx.ID_ANY, 
        choices=[("Set default"), ("25 fps (50i) PAL"), ("29.97 fps (60i) NTSC"),
        ("30 fps (30p) Progessive")], style=wx.CB_DROPDOWN | wx.CB_READONLY
        )
        self.sizer_videorate_staticbox = wx.StaticBox(self.notebook_1_pane_2, 
                                        wx.ID_ANY, ("Video Rate (Optional)")
                                                    )
        self.notebook_1_pane_3 = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.rdb_a = wx.RadioBox(self.notebook_1_pane_3, wx.ID_ANY, (
        "Audio Codec Selecting"), choices=[("Default"), 
        ("Wav (Raw, No_MultiChannel)"), ("Flac (Lossless, No_MultiChannel)"), 
        ("Aac (Lossy, MultiChannel)"), ("Alac (Lossless, m4v, No_MultiChannel)"),
        ("Ac3 (Lossy, MultiChannel)"), ("Ogg (Lossy, No_MultiChannel)"),
        ("Mp3 (Lossy, No_MultiChannel)"), ("Copy audio source"),
        ("Set manually (for experts)"), ("Encoding video with no audio")], 
                                    majorDimension=0, style=wx.RA_SPECIFY_ROWS
                                    )
        self.rdb_a.EnableItem(0,enable=True),self.rdb_a.EnableItem(1,enable=True)
        self.rdb_a.EnableItem(2,enable=True),self.rdb_a.EnableItem(3,enable=True)
        self.rdb_a.EnableItem(4,enable=False),self.rdb_a.EnableItem(5,enable=True)# ac3
        self.rdb_a.EnableItem(6,enable=True),self.rdb_a.EnableItem(7,enable=True)# mp3
        self.rdb_a.EnableItem(8,enable=True),self.rdb_a.EnableItem(9,enable=True)# manually
        self.rdb_a.EnableItem(10,enable=True),self.rdb_a.SetSelection(0)
        self.checkbox_audio_normalize = wx.CheckBox(self.notebook_1_pane_3, 
                      wx.ID_ANY, ("Normalize the audio amplitude of the video")
                                )
        self.button_analyzes = wx.Button(self.notebook_1_pane_3, wx.ID_ANY, 
                                ("Analyze")
                                )
        self.label_dbMax = wx.StaticText(self.notebook_1_pane_3, wx.ID_ANY, 
                                ("dB Maximum audio level: ")
                                )
        self.text_dbMax = wx.TextCtrl(self.notebook_1_pane_3, wx.ID_ANY, "", 
                                style=wx.TE_READONLY)
        self.label_dbMedium = wx.StaticText(self.notebook_1_pane_3, wx.ID_ANY, 
                                    ("dB Middle audio level:")
                                    )
        self.text_dbMedium = wx.TextCtrl(self.notebook_1_pane_3, wx.ID_ANY, "", 
                                style=wx.TE_READONLY)
        self.label_normalize = wx.StaticText(self.notebook_1_pane_3, wx.ID_ANY, 
                                    ("Normalize maximum amplitude in dB:  ")
                                    )
        self.spin_ctrl_audionormalize = FS.FloatSpin(self.notebook_1_pane_3, 
            wx.ID_ANY, min_val=-99.0, max_val=0.0, increment=1.0, value=-1.0, 
                        agwStyle=FS.FS_LEFT
                        )
        self.spin_ctrl_audionormalize.SetFormat("%f")
        self.spin_ctrl_audionormalize.SetDigits(1)
        #self.button_info = wx.Button(self.notebook_1_pane_3, wx.ID_ANY, ("?"), 
                            #style=wx.BU_BOTTOM)
        #self.bitmap_1 = wx.StaticBitmap(self.notebook_1_pane_3, wx.ID_ANY, 
            #wx.Bitmap(icon_eyes, wx.BITMAP_TYPE_ANY))
        
        self.notebook_1_pane_4 = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.radiobox_presets = wx.RadioBox(self.notebook_1_pane_4, wx.ID_ANY, (
        "presets h264 (optional)"), choices=[("ultrafast"), ("superfast"), 
        ("veryfast"), ("faster"), ("fast"), ("medium"), ("slow"), ("slower"), 
        ("veryslow"), ("placebo")], majorDimension=0, style=wx.RA_SPECIFY_ROWS
                                        )
        self.radiobox_profile = wx.RadioBox(self.notebook_1_pane_4, wx.ID_ANY, (
        "Profilo h264 (optional)"), choices=[("Default"), ("baseline"), 
        ("main"), ("high"), ("high10"), ("high444")], majorDimension=0, 
                                                    style=wx.RA_SPECIFY_ROWS
                                                    )
        self.radiobox_tune = wx.RadioBox(self.notebook_1_pane_4, wx.ID_ANY, (
        "Tune h264 (optional)"), choices=[("Default"), ("film"), ("animation"),
        ("grain"), ("stillimage"), ("psnr"), ("ssim"), ("fastecode"), 
                ("zerolatency")], majorDimension=0, style=wx.RA_SPECIFY_ROWS
                )
        # Set default layout of the widget in the window:
        self.button_save.Disable(), self.text_path_save.Disable()
        self.text_group_ext.Disable(),self.spin_ctrl_test.Disable()
        self.label_sec.Disable(), self.label_group.Disable()
        self.label_width_2.Disable(), self.spin_size_scaling.Disable()

        #----------------------Set Properties----------------------#
        self.SetTitle("Videomass - Video Standard Conversions Interface")
        
        if self.OS == 'darwin':
            self.SetSize((945, 505)) # OsX
        elif self.OS == 'linux2':
            #self.SetSize((890, 518)) # linux
            self.SetSize((915, 480)) # linux
        else:
            self.SetSize((970, 540))
        
        self.Centre()
        icon = wx.EmptyIcon()
        icon.CopyFromBitmap(wx.Bitmap(self.videomass_icon, wx.BITMAP_TYPE_ANY))
        self.SetIcon(icon)
        self.combobox_videoformat.SetToolTipString(
        u"Choose here a video format that will be used in the conversion.")
        self.combobox_videoformat.SetSelection(6)
        self.text_path_open.SetMinSize((120, 21))
        self.text_path_open.SetToolTipString("Imported path-name")
        self.text_path_save.SetMinSize((120, 21))
        self.text_path_save.SetToolTipString("Exported path-name")
        #self.button_ok.SetMinSize((250, 27))
        #self.button_ok.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))
        self.checkbox_batch.SetToolTipString("Apply the process to a files "
                                             "group with same extensions"
                                             )
        self.text_group_ext.SetMinSize((80, 21))
        self.text_group_ext.SetForegroundColour(wx.Colour(126, 63, 36))
        self.text_group_ext.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD,
                                                                        0, ""))
        self.text_group_ext.SetToolTipString("Write here a files group "
                               "extension to convert with batch mode. Do not "
                               "include punctuation marks '.' and blanks"
                                             )
        self.checkbox_num_pass.SetToolTipString("If you are using a double pass, "
                                "the conversion can improve the final video "
                                "quality but it can take twice as long. "
                                "Activate only in high compression in order to "
                                "improve their vision."
                                                 )
        self.checkbox_test.SetToolTipString("Run test conversion by setting "
                                            "the time in seconds"
                                            )
        self.spin_ctrl_bitrate.SetToolTipString("The bit rate determines the "
                                                "quality and the final video "
                                                "size. A larger value correspond "
                                                "to greater quality and size of "
                                                "the final video."
                                                 )
        self.spin_ctrl_bitrate.SetForegroundColour(wx.Colour(126, 63, 36))
        self.spin_ctrl_bitrate.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, 
                                                                wx.BOLD, 0, ""))
        self.spin_ctrl_test.SetMinSize((80, 21))
        self.spin_ctrl_test.SetForegroundColour(wx.Colour(126, 63, 36))
        self.spin_ctrl_test.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, 
                                                                wx.BOLD, 0, ""))
        self.spin_ctrl_test.SetToolTipString("Enter time in seconds")
        self.slider_CRF.SetMinSize((230, 46))
        self.slider_CRF.SetForegroundColour(wx.Colour(126, 63, 36))
        self.slider_CRF.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD,
                                                                        0, ""))
        self.slider_CRF.SetToolTipString("CRF (constant rate factor) Affects "
                                 "the size and quality of the final video. Is "
                                 "automatically activated in the h264 codec "
                                 "in single pass only. Double pass swich to "
                                 "bitrate. With lower values the quality is "
                                 "higher and the larger file-output size."
                                          )
        self.checkbox_videosize.SetToolTipString(u"Enables manual setting of "
                                                 "sizing (width x height)"
                                                 )
        self.spin_size_width.SetForegroundColour(wx.Colour(126, 63, 36))
        self.spin_size_width.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, 
                                                                wx.BOLD, 0, ""))
        self.spin_size_height.SetForegroundColour(wx.Colour(126, 63, 36))
        self.spin_size_height.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, 
                                                                wx.BOLD, 0, ""))
        sizestr = (u"The video size is the size of the final video in pixels. "
                "Example for youtube:: 640X480\nExample for Ipod/Iphone: "
                "320x180\nVideo per PSP: 320x240"
                )
        self.spin_size_height.SetToolTipString("Video height:\n%s" %(sizestr))
        self.spin_size_height.SetMinSize((70, 20))
        self.spin_size_width.SetToolTipString("Video width:\n%s" %(sizestr))##
        self.spin_size_width.SetMinSize((70, 20))
        self.checkbox_scale.SetToolTipString("Enable automatic scaling "
                            "proportionate videos. Scaling is any resizing "
                            "operation without changing the appearance of "
                            "the original movie")##
        self.spin_size_scaling.SetForegroundColour(wx.Colour(126, 63, 36))
        self.spin_size_scaling.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, 
                                                                wx.BOLD, 0, ""))
        self.spin_size_scaling.SetMinSize((70, 20))
        self.spin_size_scaling.SetToolTipString("Enter only the video width in "
                                       "pixels. The video height will be calculated"
                                       "automatically")##
        self.checkbox_deinterlace.SetToolTipString(u'Deinterlace the input video '
                    u'("w3fdif" stands for "Weston 3 Field Deinterlacing Filter. '
                    u'Based on the process described by Martin Weston for BBC R&D, '
                    u'and implemented based on the de-interlace algorithm written '
                    u'by Jim Easterbrook for BBC R&D, the Weston 3 field '
                    u'deinterlacing filter uses filter coefficients calculated '
                    u'by BBC R&D.")'
                                                   )
        self.radio_box_deinterlace.SetToolTipString('Set the interlacing filter '
                            'coefficients. Accepts one of the following values:\n'
                            'simple: Simple filter coefficient set.\n'
                            'complex: More-complex filter coefficient set. '
                            'Default value is complex.'
                                                    )
        self.checkbox_interlace.SetToolTipString('Simple interlacing filter from '
                    'progressive contents. This interleaves upper (or lower) '
                    'lines from odd frames with lower (or upper) lines from even '
                    'frames, halving the frame rate and preserving image height.'
                                                 )
        self.radio_box_interlace.SetToolTipString('It accepts the following '
            'optional parameters;\nscan: determines whether the interlaced frame '
            'is taken from the even (tff - default) or odd (bff) lines of the '
            'progressive frame.\nlowpas: Enable (default) or disable the vertical '
            'lowpass filter to avoid twitter interlacing and reduce moire '
            'patterns. Default is no setting.'
                                                  )
        self.combobox_Vaspect.SetSelection(0)
        self.combobox_Vaspect.SetToolTipString(u"Video aspect (Aspect Ratio) "
                        "is the video width and video height ratio. "
                        "Leave on 'Set default' to copy the original settings."
                                                )
        self.combobox_Vrate.SetSelection(0)
        self.combobox_Vrate.SetToolTipString(u"Video Rate: A any video consists "
                       "of images displayed as frames, repeated a given number "
                       "of times per second. In countries are 30 NTSC, PAL "
                       "countries (like Italy) are 25. Leave on 'Set default' "
                       "to copy the original settings."
                                              )
        
        self.checkbox_audio_normalize.SetToolTipString("Performs audio "
                            "normalization content in the video. Warning: "
                            "If set to 'Batch-Mode' this function is disable. "
                            "If 'Audio Codec Selecting' is set to 'Default' or "
                            "'Copy audio source' or 'Set manually (for experts)' "
                            "or 'Encoding video with no audio', this function is "
                            "disable."
                                                    )
        self.button_analyzes.SetToolTipString("Analysis of the peak maximum, "
                                "and average of the audio content in the video "
                                "in dB, for the normalization calculate. This "
                                "may take some time for the video."
                                               )
        self.spin_ctrl_audionormalize.SetMinSize((70, 20))
        self.spin_ctrl_audionormalize.SetToolTipString("Select with many db you "
                                     "want to increase or decrease the volume. "
                                     "The upper limit is 0.0, the minimum limit "
                                     "is -99.0. The default setting is to -1.0 "
                                     "and is good for most of the processes"
                                                       )
        #self.button_info.SetMinSize((40, 30))
        #self.button_info.SetBackgroundColour(wx.Colour(170, 255, 148))
        #self.button_info.SetForegroundColour(wx.Colour(69, 76, 255))
        #self.button_info.SetFont(wx.Font(12, wx.DECORATIVE, wx.NORMAL, wx.BOLD, 0, ""))
        #self.button_info.SetToolTipString("Guida locale per la gestione impostazioni audio")
        
        self.rdb_a.SetToolTipString("selection of audio codecs to be used in video")
        self.radiobox_presets.SetToolTipString("preset h264")
        self.radiobox_presets.SetSelection(5)
        self.radiobox_profile.SetToolTipString("profili h264")
        self.radiobox_profile.SetSelection(0)
        self.radiobox_tune.SetToolTipString("tune h264")
        self.radiobox_tune.SetSelection(0)
        self.notebook_1_pane_4.SetToolTipString("The parameters on this tab "
                        "are enabled only for the video-codec h264. Although "
                        "optional, is set to 'preset medium' as default "
                        "parameter.")
        self.sb.SetStatusText("Output format: mkv")
        self.sb.Refresh()
        
        #----------------------Build Layout----------------------#
        sizer_base = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_base = wx.FlexGridSizer(2, 1, 0, 0)
        sizer_pane4_base = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_pane4_base = wx.GridSizer(1, 3, 0, 0)
        sizer_pane3_base = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_pane3_base = wx.GridSizer(1, 2, 0, 0)
        sizer_pane3_audio_column2 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_in_column2 = wx.FlexGridSizer(3, 1, 0, 0)
        grid_sizer_audionormalize = wx.GridSizer(3, 1, 0, 0)
        grid_sizer_slider_normalize = wx.FlexGridSizer(1, 2, 0, 0)
        grid_sizer_text_normalize = wx.FlexGridSizer(2, 2, 0, 0)
        sizer_pane3_audio_column1 = wx.BoxSizer(wx.VERTICAL)
        sizer_pane2_base = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_pane2_base = wx.GridSizer(1, 3, 0, 0)
        grid_sizer_1 = wx.GridSizer(2, 1, 0, 0)
        self.sizer_videorate_staticbox.Lower()
        sizer_videorate = wx.StaticBoxSizer(self.sizer_videorate_staticbox, wx.VERTICAL)
        self.sizer_videoaspect_staticbox.Lower()
        sizer_videoaspect = wx.StaticBoxSizer(self.sizer_videoaspect_staticbox, wx.VERTICAL)
        self.sizer_1_staticbox.Lower()
        sizer_1 = wx.StaticBoxSizer(self.sizer_1_staticbox, wx.VERTICAL)
        grid_sizer_3 = wx.GridSizer(2, 2, 0, 0)
        self.sizer_videosize_staticbox.Lower()
        sizer_2 = wx.StaticBoxSizer(self.sizer_videosize_staticbox, wx.VERTICAL)
        grid_sizer_2 = wx.GridSizer(6, 1, 0, 0)
        grid_sizer_5 = wx.GridSizer(1, 3, 0, 0)
        grid_sizer_4 = wx.GridSizer(1, 2, 0, 0)
        grid_sizer_pane1_base = wx.GridSizer(1, 3, 0, 0)
        grid_sizer_pane1_right = wx.GridSizer(2, 1, 0, 0)
        self.sizer_crf_staticbox.Lower()
        sizer_crf = wx.StaticBoxSizer(self.sizer_crf_staticbox, wx.VERTICAL)
        self.sizer_bitrate_staticbox.Lower()
        sizer_bitrate = wx.StaticBoxSizer(self.sizer_bitrate_staticbox, wx.VERTICAL)
        self.sizer_automations_staticbox.Lower()
        sizer_automations = wx.StaticBoxSizer(self.sizer_automations_staticbox, wx.VERTICAL)
        grid_sizer_automations = wx.GridSizer(3, 1, 0, 0)
        grid_sizer_column_testing = wx.GridSizer(1, 3, 0, 0)
        grid_sizer_automations_column = wx.GridSizer(1, 3, 0, 0)
        grid_sizer_pane1_left = wx.GridSizer(2, 1, 0, 0)
        self.sizer_dir_staticbox.Lower()
        sizer_dir = wx.StaticBoxSizer(self.sizer_dir_staticbox, wx.VERTICAL)
        grid_sizer_dir = wx.GridSizer(2, 2, 0, 0)
        self.sizer_combobox_formatv_staticbox.Lower()
        sizer_combobox_formatv = wx.StaticBoxSizer(self.sizer_combobox_formatv_staticbox, wx.VERTICAL)
        sizer_combobox_formatv.Add(self.combobox_videoformat, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 20)
        grid_sizer_pane1_left.Add(sizer_combobox_formatv, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_dir.Add(self.button_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_dir.Add(self.text_path_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_dir.Add(self.button_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_dir.Add(self.text_path_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_dir.Add(grid_sizer_dir, 1, wx.EXPAND, 0)
        grid_sizer_pane1_left.Add(sizer_dir, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_pane1_base.Add(grid_sizer_pane1_left, 1, wx.EXPAND, 0)
        grid_sizer_automations_column.Add(self.checkbox_batch, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_automations_column.Add(self.label_group, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_automations_column.Add(self.text_group_ext, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_automations.Add(grid_sizer_automations_column, 1, wx.EXPAND, 0)
        grid_sizer_automations.Add(self.checkbox_num_pass, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_column_testing.Add(self.checkbox_test, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_column_testing.Add(self.label_sec, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_column_testing.Add(self.spin_ctrl_test, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_automations.Add(grid_sizer_column_testing, 1, wx.EXPAND, 0)
        sizer_automations.Add(grid_sizer_automations, 1, wx.EXPAND, 0)
        grid_sizer_pane1_base.Add(sizer_automations, 1, wx.ALL | wx.EXPAND, 15)
        sizer_bitrate.Add(self.spin_ctrl_bitrate, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 20)
        grid_sizer_pane1_right.Add(sizer_bitrate, 1, wx.ALL | wx.EXPAND, 15)
        sizer_crf.Add(self.slider_CRF, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 20)
        grid_sizer_pane1_right.Add(sizer_crf, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_pane1_base.Add(grid_sizer_pane1_right, 1, wx.EXPAND, 0)
        self.notebook_1_pane_1.SetSizer(grid_sizer_pane1_base)
        grid_sizer_2.Add(self.checkbox_videosize, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 10)
        grid_sizer_4.Add(self.label_width_1, 0, wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 20)
        grid_sizer_4.Add(self.label_height, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 20)
        grid_sizer_2.Add(grid_sizer_4, 1, wx.EXPAND, 0)
        grid_sizer_5.Add(self.spin_size_width, 0, wx.LEFT | wx.ALIGN_CENTER_HORIZONTAL, 20)
        grid_sizer_5.Add(self.label_x, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        grid_sizer_5.Add(self.spin_size_height, 0, wx.RIGHT | wx.ALIGN_CENTER_HORIZONTAL, 20)
        grid_sizer_2.Add(grid_sizer_5, 1, wx.EXPAND, 0)
        grid_sizer_2.Add(self.checkbox_scale, 0, wx.TOP | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 30)
        grid_sizer_2.Add(self.label_width_2, 0, wx.TOP | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 20)
        grid_sizer_2.Add(self.spin_size_scaling, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        sizer_2.Add(grid_sizer_2, 1, wx.EXPAND, 0)
        grid_sizer_pane2_base.Add(sizer_2, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_3.Add(self.checkbox_deinterlace, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_3.Add(self.radio_box_deinterlace, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        grid_sizer_3.Add(self.checkbox_interlace, 0, wx.ALL, 5)
        grid_sizer_3.Add(self.radio_box_interlace, 0, 0, 0)
        sizer_1.Add(grid_sizer_3, 1, wx.EXPAND, 0)
        grid_sizer_pane2_base.Add(sizer_1, 1, wx.ALL | wx.EXPAND, 15)
        sizer_videoaspect.Add(self.combobox_Vaspect, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 50)
        grid_sizer_1.Add(sizer_videoaspect, 1, wx.ALL | wx.EXPAND, 15)
        sizer_videorate.Add(self.combobox_Vrate, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 50)
        grid_sizer_1.Add(sizer_videorate, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_pane2_base.Add(grid_sizer_1, 1, wx.EXPAND, 0)
        sizer_pane2_base.Add(grid_sizer_pane2_base, 1, wx.EXPAND, 0)
        self.notebook_1_pane_2.SetSizer(sizer_pane2_base)
        sizer_pane3_audio_column1.Add(self.rdb_a, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_pane3_base.Add(sizer_pane3_audio_column1, 1, wx.EXPAND, 0)
        grid_sizer_in_column2.Add(self.checkbox_audio_normalize, 0, wx.TOP, 15)
        grid_sizer_audionormalize.Add(self.button_analyzes, 0, wx.TOP | wx.ALIGN_BOTTOM | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_text_normalize.Add(self.label_dbMax, 0, wx.TOP, 15)
        grid_sizer_text_normalize.Add(self.text_dbMax, 0, wx.TOP, 15)
        grid_sizer_text_normalize.Add(self.label_dbMedium, 0, wx.TOP, 15)
        grid_sizer_text_normalize.Add(self.text_dbMedium, 0, wx.TOP, 15)
        grid_sizer_audionormalize.Add(grid_sizer_text_normalize, 1, wx.EXPAND, 0)
        grid_sizer_slider_normalize.Add(self.label_normalize, 0, wx.TOP | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_slider_normalize.Add(self.spin_ctrl_audionormalize, 0, wx.TOP | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_audionormalize.Add(grid_sizer_slider_normalize, 1, wx.EXPAND, 0)
        grid_sizer_in_column2.Add(grid_sizer_audionormalize, 1, wx.EXPAND, 5)
        #grid_sizer_in_column2.Add(self.bitmap_1, 0, wx.ALL | wx.ALIGN_RIGHT, 5)
        sizer_pane3_audio_column2.Add(grid_sizer_in_column2, 1, wx.EXPAND, 0)
        grid_sizer_pane3_base.Add(sizer_pane3_audio_column2, 1, wx.EXPAND, 0)
        sizer_pane3_base.Add(grid_sizer_pane3_base, 1, wx.EXPAND, 0)
        self.notebook_1_pane_3.SetSizer(sizer_pane3_base)
        grid_sizer_pane4_base.Add(self.radiobox_presets, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_pane4_base.Add(self.radiobox_profile, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_pane4_base.Add(self.radiobox_tune, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_pane4_base.Add(grid_sizer_pane4_base, 1, wx.EXPAND, 0)
        self.notebook_1_pane_4.SetSizer(sizer_pane4_base)
        self.notebook_1.AddPage(self.notebook_1_pane_1, ("Management Conversion"))
        self.notebook_1.AddPage(self.notebook_1_pane_2, ("Video Preferences"))
        self.notebook_1.AddPage(self.notebook_1_pane_3, ("Audio Settings"))
        self.notebook_1.AddPage(self.notebook_1_pane_4, ("Codec H264 Options"))
        grid_sizer_base.Add(self.notebook_1, 1, wx.ALL | wx.EXPAND, 5)
        self.panel_base.SetSizer(grid_sizer_base)
        grid_sizer_base.AddGrowableRow(0)
        grid_sizer_base.AddGrowableRow(1)
        grid_sizer_base.AddGrowableCol(0)
        sizer_base.Add(self.panel_base, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_base)
        self.Layout()
        
        #----------------------Binding (EVT)----------------------#
        """
        Note: wx.EVT_TEXT_ENTER é diverso da wx.EVT_TEXT . Il primo é sensibile
        agli input di tastiera, il secondo é sensibile agli input di tastiera
        ma anche agli "append"
        """
        #self.Bind(wx.EVT_COMBOBOX, self.on_choice_list, self.combobox_videoformat)
        self.combobox_videoformat.Bind(wx.EVT_COMBOBOX, self.on_choice_list)
        
        self.Bind(wx.EVT_BUTTON, self.on_open, self.button_open)
        self.Bind(wx.EVT_TEXT, self.enter_path_open, self.text_path_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.button_save)
        self.Bind(wx.EVT_TEXT, self.enter_path_save, self.text_path_save)
        self.Bind(wx.EVT_CHECKBOX, self.push_batch_mode, self.checkbox_batch)
        self.Bind(wx.EVT_TEXT, self.enter_group_ext, self.text_group_ext)
        self.Bind(wx.EVT_CHECKBOX, self.push_pass, self.checkbox_num_pass)
        self.Bind(wx.EVT_CHECKBOX, self.push_test, self.checkbox_test)
        self.Bind(wx.EVT_SPINCTRL, self.enter_time_sec, self.spin_ctrl_test)
        self.Bind(wx.EVT_SPINCTRL, self.enter_bitrate, self.spin_ctrl_bitrate)
        self.Bind(wx.EVT_COMMAND_SCROLL, self.enter_CRF, self.slider_CRF)
        self.Bind(wx.EVT_CHECKBOX, self.push_enable_vsize, self.checkbox_videosize)
        self.Bind(wx.EVT_SPINCTRL, self.enter_size_width, self.spin_size_width)
        self.Bind(wx.EVT_SPINCTRL, self.enter_size_height, self.spin_size_height)
        self.Bind(wx.EVT_SPINCTRL, self.enter_size_scaling, self.spin_size_scaling)
        self.Bind(wx.EVT_CHECKBOX, self.push_enable_scale, self.checkbox_scale)
        self.Bind(wx.EVT_CHECKBOX, self.check_deinterlace, self.checkbox_deinterlace)
        self.Bind(wx.EVT_RADIOBOX, self.radio_deinterlace, self.radio_box_deinterlace)
        self.Bind(wx.EVT_CHECKBOX, self.check_interlace, self.checkbox_interlace)
        self.Bind(wx.EVT_RADIOBOX, self.radio_interlace, self.radio_box_interlace)
        self.Bind(wx.EVT_COMBOBOX, self.on_choice_vaspect, self.combobox_Vaspect)
        self.Bind(wx.EVT_COMBOBOX, self.on_choice_vrate, self.combobox_Vrate)
        self.Bind(wx.EVT_RADIOBOX, self.choice_audio, self.rdb_a)
        self.Bind(wx.EVT_CHECKBOX, self.push_audio_normalize, self.checkbox_audio_normalize)
        self.Bind(wx.EVT_BUTTON, self.on_audio_analyzes, self.button_analyzes)
        #self.Bind(wx.EVT_SPINCTRL, self.enter_normalize, self.spin_ctrl_audionormalize)
        #self.Bind(wx.EVT_BUTTON, self.on_info, self.button_info)##
        self.Bind(wx.EVT_RADIOBOX, self.choice_presets, self.radiobox_presets)
        self.Bind(wx.EVT_RADIOBOX, self.choice_profile, self.radiobox_profile)
        self.Bind(wx.EVT_RADIOBOX, self.choice_tune, self.radiobox_tune)
        self.Bind(wx.EVT_CLOSE, self.Quiet) # controlla la chiusura dalla x di chiusura
        
    #----------------------used methods----------------------#

        # call instances for initialize default layout:
        self.libx264()  
        self.audio_default()
        self.normalize_default()

    def enable_button(self):
        self.toolbar.EnableTool(wx.ID_OK, True)
        
    def disable_button(self):
        self.toolbar.EnableTool(wx.ID_OK, False)
        
    def default_videosettings(self):
        self.combobox_Vrate.SetSelection(0),self.combobox_Vaspect.SetSelection(0)
        self.checkbox_videosize.SetValue(False), self.checkbox_videosize.Enable()
        self.spin_size_height.Disable(), self.spin_size_width.Disable()
        self.spin_size_scaling.Disable(), self.spin_size_scaling.SetValue(0)
        self.spin_size_height.SetValue(0), self.spin_size_width.SetValue(0)
        self.combobox_Vaspect.Enable(), self.checkbox_scale.SetValue(False)
        self.checkbox_scale.Enable()
        self.label_width_1.Disable(), self.label_height.Disable()
        self.label_x.Disable(), self.label_width_2.Disable()
        self.radio_box_deinterlace.SetSelection(0), 
        self.checkbox_deinterlace.SetValue(False)
        self.radio_box_deinterlace.Disable()
        #self.radio_box_interlace.SetSelection(0), 
        self.checkbox_interlace.SetValue(False)
        #self.radio_box_interlace.Disable()
        diction_command["VideoAspect"] = ""
        diction_command["VideoRate"] = ""
        diction_command["VideoSize"] = ""
        diction_command["scale"] = ""
        self.radio_box_interlace.Hide() # TODO QUESTO E' IN SVILUPPO

    def libx264(self): 
        """
        abilito funzionalità per codec h264 (in doppia-pass si riabilita 
        il bitrate) enable h264 layout (double-pass enable bitrate)
        """
        self.disable_button()
        self.default_videosettings()
        #### notebook_pane
        self.notebook_1_pane_4.Enable()
        
        # enable or disable if double pass is on or off
        if self.checkbox_num_pass.GetValue() is False:
            # bitrate
            self.sizer_bitrate_staticbox.Disable() 
            self.spin_ctrl_bitrate.SetValue(1500)
            self.spin_ctrl_bitrate.Disable() # disabilito
            # CRF
            self.sizer_crf_staticbox.Enable(), self.slider_CRF.SetValue(22)
            self.slider_CRF.Enable() # riabilito
            
        elif self.checkbox_num_pass.GetValue() is True:
            # bitrate
            self.sizer_bitrate_staticbox.Enable()
            self.spin_ctrl_bitrate.SetValue(1500)
            self.spin_ctrl_bitrate.Enable() # riabilito
            # CRF
            self.sizer_crf_staticbox.Disable(), self.slider_CRF.SetValue(22)
            self.slider_CRF.Disable() # disabilito
            
    def no_libx264(self): 
        """
        abilito funzionalità per tutti gli altri codec che usano solo bitrate
        enable task for bitrate layout in other codec
        """
        self.disable_button()
        self.default_videosettings()
        
        #### notebook_pane
        self.notebook_1_pane_4.Disable() 

        #### bitrate
        self.sizer_bitrate_staticbox.Enable()
        self.spin_ctrl_bitrate.SetValue(1500)
        self.spin_ctrl_bitrate.Enable() # riabilito
        
        #### CRF
        self.sizer_crf_staticbox.Disable(), self.slider_CRF.SetValue(22)
        self.slider_CRF.Disable() # disabilito
        
    def audio_default(self):
        """
        Set default audio parameters. This fuction is called if push cancel 
        button in audio dialog and if audio codec is not supported by video
        codec or if change video profile in combobox
        """
        self.rdb_a.SetStringSelection("Default") # scheda param.audio
        diction_command["Audio"] = ""
        diction_command["AudioCodec"] = "Default"
        diction_command["AudioBitrate"] = ""
        diction_command["AudioChannel"] = ""
        diction_command["AudioRate"] = ""
        
    def normalize_default(self):
        """
        Set default normalization parameters of the audio panel. This fuction 
        is called if push cancel button in audio dialog and if audio codec is 
        not supported by video codec or if change video profile in combobox.
        Also, is called if checkbox batch-mode is True because not enable the
        normalization in this mode.
        """
        self.checkbox_audio_normalize.SetValue(False)
        self.checkbox_audio_normalize.Disable()
        self.button_analyzes.Disable(), self.spin_ctrl_audionormalize.Disable()
        self.button_analyzes.SetBackgroundColour(wx.NullColour)# norm
        self.spin_ctrl_audionormalize.SetValue(-1.0)
        self.text_dbMax.SetValue(""), self.text_dbMedium.SetValue("")
        self.text_dbMax.Disable(), self.text_dbMedium.Disable()
        self.label_dbMax.Disable(), self.label_dbMedium.Disable()
        self.label_normalize.Disable()
        diction_command["Normalize"] = ""
        
    def statusbar_msg(self, msg, color):
        """
        set the status-bar with messages and color types
        """
        if color == None:
            self.sb.SetBackgroundColour(wx.NullColour)
        else:
            self.sb.SetBackgroundColour(color)
        self.sb.SetStatusText(msg)
        self.sb.Refresh()
    
    #----------------------Event handler (callback)----------------------#
    #------------------------------------------------------------------#
    def on_choice_list(self, event):
        """
        L'evento scelta nella combobox dei formati video scatena
        il setting ai valori predefiniti. Questo determina lo stato 
        di default ogni volta che si cambia codec video. Inoltre
        vengono abilitate o disabilitate funzioni dipendentemente
        dal tipo di codec scelto.
        """
        #### main panel
        self.checkbox_batch.SetValue(False)
        self.checkbox_test.SetValue(False)
        self.checkbox_num_pass.SetValue(False)

        self.push_batch_mode(self) # call function of event (batch mode)
        self.push_test(self) # call function of event (test seconds)
        self.push_pass(self) # call function of event (number pass)
        
        #### Video settings panel
        self.default_videosettings()
        
        #### Audio settings panel
        self.audio_default() # reset audio radiobox and dict
        self.normalize_default() # reset normalization parametrs
        
        #### Codec options h264 panel
        self.radiobox_presets.SetStringSelection("medium") # scheda sett presets
        self.radiobox_profile.SetStringSelection("Default") # profile
        self.radiobox_tune.SetStringSelection("Default") # tune
        diction_command["Presets"] = ""
        diction_command["Profile"] = ""
        diction_command["Tune"] = ""
        
        selected = self.combobox_videoformat.GetValue()
        
        if diction_codec[selected] == "-vcodec libx264":
            diction_command["FormatChoice"] = "%s" % (selected)
            # avi,mkv,mp4,flv,etc.:
            diction_command['VideoFormat'] = "%s" % ( diction_format[selected])
            diction_command["VideoCodec"] = "-vcodec libx264"
            diction_command["Bitrate"] = ""
            diction_command["CRF"] = ""
            self.libx264()
        else:
            diction_command["FormatChoice"] = "%s" % (selected)
            # avi,mkv,mp4,flv,etc.:
            diction_command['VideoFormat'] = "%s" % (diction_format [selected])
            # -vcodec libx264 o altro:
            diction_command["VideoCodec"] = "%s" %(diction_codec [selected])
            diction_command["Bitrate"] = ""
            diction_command["CRF"] = ""
            self.no_libx264()
            
        self.setAudioRadiobox(self)
        self.statusbar_msg("Output format: %s" % (
                                    diction_command['VideoFormat']),None)
        
    #------------------------------------------------------------------#
    def on_open(self, event):
        """
        Open choice dialog input
        """
        dialfile = wx.FileDialog(self, "Import a video file", 
                                    ".", "", "*.*", wx.OPEN)
        dialdir = wx.DirDialog(self, "Run a directory batch process")
        
        if self.checkbox_batch.GetValue() is False:
            if dialfile.ShowModal() == wx.ID_OK:
                self.text_path_open.SetValue("")
                self.text_path_open.AppendText(dialfile.GetPath())
                dialfile.Destroy()
        else:
            if dialdir.ShowModal() == wx.ID_OK:
                self.text_path_open.SetValue("")
                self.text_path_open.AppendText(dialdir.GetPath())
                dialdir.Destroy()
                
    #------------------------------------------------------------------#
    def enter_path_open(self, event):
        """
        This is a text widget input. 
        """
        diction_command["InputDir"] = "%s" %(self.text_path_open.GetValue())
        
        if self.text_path_open.GetValue() == '':
            self.text_path_save.SetValue('')
            self.button_save.Disable(), self.text_path_save.Disable()
            self.toolbar.EnableTool(wx.ID_FILE1, False) # play (disable)
            self.toolbar.EnableTool(wx.ID_FILE2, False) # analyze (disable)
            self.file_save.Enable(False) # filesave in menubar (disable)
        else:
            self.button_save.Enable(), self.text_path_save.Enable()
            self.file_save.Enable(True) # filesave in menubar (enable)
            
            if self.checkbox_batch.GetValue() is False:
                self.toolbar.EnableTool(wx.ID_FILE1, True) # play (enable)
                self.toolbar.EnableTool(wx.ID_FILE2, True) # analyze (enable)

        self.normalize_default()
        self.audio_default()
        self.statusbar_msg("Input Files or Directory:  %s" % (
                                            diction_command["InputDir"]),None)
                
    #------------------------------------------------------------------#
    def on_save(self, event):
        """
        Open choice dialog output
        """
        ext = diction_command['VideoFormat']
        
        if self.checkbox_batch.GetValue() is False:
            dialsave = wx.FileDialog(self, "Where do you save your file ?", "",
                                     "", "%s files (*.%s)|*.%s" % (
                                            ext,ext,ext), wx.SAVE | 
                                        wx.OVERWRITE_PROMPT | 
                                        wx.FD_CHANGE_DIR
                                        )
            if dialsave.ShowModal() == wx.ID_OK:
                path = dialsave.GetPath()
                
                if path.endswith(ext) != True:
                    path = "%s.%s" % (path, ext)
                    
                self.text_path_save.SetValue("")
                self.text_path_save.AppendText(path)
                dialsave.Destroy()
                self.enable_button()
        else:
            dialdir = wx.DirDialog(self, "Where do you save your files ?")
            
            if dialdir.ShowModal() == wx.ID_OK:
                    self.text_path_save.SetValue("")
                    self.text_path_save.AppendText(dialdir.GetPath())
                    dialdir.Destroy()
                    self.enable_button()
                    
    #------------------------------------------------------------------#
    def enter_path_save(self, event):
        """
        This is a text widget output
        """
        diction_command["OutputDir"] = "%s" %(self.text_path_save.GetValue())
        if self.text_path_save.GetValue() == '':
            self.disable_button()
        else:
            self.enable_button()
            self.statusbar_msg("Output file:  %s" % (
                                        diction_command["OutputDir"]),None)
    #------------------------------------------------------------------#
    def push_batch_mode(self, event):
        """
        enable or disable functionality the std conv or batch conv
        """
        on_batch = "turn on"
        msg = ("Batch-Mode is Turn-On: Remember to type in "
               "'Group' field which extension has files format to be treated")
        
        if self.checkbox_batch.GetValue() is True:
            self.sizer_dir_staticbox.SetLabel('Import/Export Directory Movies')
            self.button_open.SetBackgroundColour(yellow)
            self.button_save.SetBackgroundColour(yellow)
            self.text_group_ext.Enable(), self.label_group.Enable() # estensione gruppo
            self.text_group_ext.SetValue(""), self.text_path_open.SetValue("")
            self.text_path_save.SetValue("")
            diction_command["Batch"] = "group files directory"
            array.append(on_batch)
            self.normalize_default()
            self.audio_default()
            self.statusbar_msg(msg,yellow)

        elif self.checkbox_batch.GetValue() is False:
            self.sizer_dir_staticbox.SetLabel('Import/Export Media Movie')
            self.button_open.SetBackgroundColour(wx.NullColour)
            self.button_save.SetBackgroundColour(wx.NullColour)
            self.text_group_ext.Disable(), self.label_group.Disable() # estensione gruppo
            self.text_group_ext.SetValue(""), self.text_path_open.SetValue("")
            self.text_path_save.SetValue("")
            self.normalize_default()
            self.audio_default()
            self.statusbar_msg("Batch-Mode is Turn-Off",None)
            
            if "turn on" in array:
                diction_command["Batch"] = "single file conversion"
                array.remove("turn on")

    #------------------------------------------------------------------#
    def enter_group_ext(self, event):
        """
        put file extension type as value of dict for batch mode
        """
        diction_command["ext_input"] = "%s" %(self.text_group_ext.GetValue())
        self.statusbar_msg("Batch-Mode is Turn-On | Files format extensions "
                           "group: %s" % (self.text_group_ext.GetValue()),None)
        
    #------------------------------------------------------------------#
    def push_pass(self, event):
        """
        enable or disable functionality for two pass encoding
        """
        on_doublepass = "double_pass"
        
        if self.checkbox_num_pass.GetValue() is True:
            diction_command["Passing"] = "double"
            array.append(on_doublepass)
            self.slider_CRF.SetValue(22)
            self.slider_CRF.Disable(),self.sizer_crf_staticbox.Disable()
            self.sizer_bitrate_staticbox.Enable(), self.spin_ctrl_bitrate.Enable()
            
        elif self.checkbox_num_pass.GetValue() is False:
            self.slider_CRF.Enable(),self.sizer_crf_staticbox.Enable()
            self.sizer_bitrate_staticbox.Disable(), self.spin_ctrl_bitrate.Disable()
            self.spin_ctrl_bitrate.SetValue(1500)
            
            if "double_pass" in array:
                diction_command["Passing"] = "single"
                array.remove("double_pass")
                
        self.statusbar_msg("%s pass ready" % (
                                    diction_command["Passing"]),None)

    #------------------------------------------------------------------#
    def push_test(self, event):
        """
        enable or disable preview of the result putting flag options of 
        ffmpeg to relative value dict or a empty value
        """
        if self.checkbox_test.GetValue() is True:
            self.spin_ctrl_test.Enable(), self.label_sec.Enable()
            diction_command["testing"] = "-t %s" % (self.spin_ctrl_test.GetValue())
            self.statusbar_msg("Run conversion for testing: %s seconds" % (
                self.spin_ctrl_test.GetValue()),None)
            
        elif self.checkbox_test.GetValue() is False:
            self.spin_ctrl_test.Disable(), self.spin_ctrl_test.SetValue(10)
            self.label_sec.Disable()
            diction_command["testing"] = ""
            self.statusbar_msg("Run conversion with no testing",None)
            
        
            
    #------------------------------------------------------------------#
    def enter_time_sec(self, event):
        """
        the string come remake when change time. This is disable
        if checkbox is false
        """
        diction_command["testing"] = "-t %s" % (self.spin_ctrl_test.GetValue())
        self.statusbar_msg("Run conversion for testing: %s seconds" % (
            self.spin_ctrl_test.GetValue()),None)
        
    #------------------------------------------------------------------#
    def enter_bitrate(self, event):
        """
        Reset CRF at empty (this depend if is h264 two-pass encoding
        or if not codec h264)
        """
        diction_command["CRF"] = ""
        self.slider_CRF.SetValue(22)
        
    #------------------------------------------------------------------#
    def enter_CRF(self, event):
        """
        Reset bitrate at empty (this depend if is h264 codec)
        """
        diction_command["Bitrate"] = ""
        self.spin_ctrl_bitrate.SetValue(1500)
        
    #------------------------------------------------------------------#
    def push_enable_vsize(self, event):
        """
        Enable or disable functionality for sizing video
        """
        ## TODO è meglio usare il filtro scale anzichè -s ???
        
        if self.checkbox_videosize.GetValue() is True:
            
            self.spin_size_height.Enable(), self.spin_size_width.Enable()
            self.spin_size_height.SetValue(0), self.spin_size_width.SetValue(0)
            self.label_width_1.Enable(), self.label_height.Enable()
            self.label_x.Enable(), self.checkbox_scale.Disable()
            
        elif self.checkbox_videosize.GetValue() is False:
            
            diction_command["scale"] = ""
            diction_command["VideoSize"] = ""
            self.spin_size_height.Disable(), self.spin_size_width.Disable()
            self.spin_size_height.SetValue(0), self.spin_size_width.SetValue(0)
            self.label_width_1.Disable(), self.label_height.Disable()
            self.label_x.Disable(), self.checkbox_scale.Enable()
            
    #------------------------------------------------------------------#
    def enter_size_width(self, event):
        """
        Set parameter with integear value 
        """
        diction_command["VideoSize"] = ""
        
    #------------------------------------------------------------------#
    def enter_size_height(self, event):
        """
        Set parameter with integear value 
        """
        diction_command["VideoSize"] = ""
        
    #------------------------------------------------------------------#
    def push_enable_scale(self, event):
        """
        Enable or disable functionality for scale. Scale is a parameter 
        that not work if 'VideoAspect' is enable. Also disable the 'VideoSize'
        checkbox and del value of dict for VideoSize
        """
        if self.checkbox_scale.GetValue() is True:
            
            diction_command["VideoAspect"] = ""
            self.combobox_Vaspect.Disable(), self.combobox_Vaspect.SetSelection(0)
            self.label_width_2.Enable(), self.spin_size_scaling.Enable()
            self.checkbox_videosize.Disable()

        elif self.checkbox_scale.GetValue() is False:
            diction_command["scale"] = ""
            diction_command["VideoSize"] = ""
            self.spin_size_scaling.Disable(), self.spin_size_scaling.SetValue(0)
            self.combobox_Vaspect.Enable(), self.label_width_2.Disable()
            self.checkbox_videosize.Enable()
            
    #------------------------------------------------------------------#
    def enter_size_scaling(self, event):
        """
        Set parameter with integear value 
        """
        diction_command["scale"] = "-vf scale=%s:-1" % (
                                            self.spin_size_scaling.GetValue())
        
    #------------------------------------------------------------------#
    def check_deinterlace(self, event):
        """
        Enable or disable functionality 
        """
        if self.checkbox_deinterlace.GetValue() is True:
            self.radio_box_deinterlace.Enable()
            self.checkbox_interlace.Disable()
            diction_command["deinterlace"] = "-vf w3fdif"
            
        elif self.checkbox_deinterlace.GetValue() is False:
            self.radio_box_deinterlace.Disable()
            self.checkbox_interlace.Enable()
            diction_command["deinterlace"] = ""
            
    #------------------------------------------------------------------#
    def radio_deinterlace(self, event):
        """
        Set parameter with string value 
        """
        diction_command["deinterlace"] = \
            "-vf w3fdif=%s" % self.radio_box_deinterlace.GetStringSelection()
    #------------------------------------------------------------------#

    def check_interlace(self, event):
        """
        Enable or disable functionality 
        """
        if self.checkbox_interlace.GetValue() is True:
            self.checkbox_deinterlace.Disable()
            diction_command["interlace"] = "-vf interlace"
            
        elif self.checkbox_interlace.GetValue() is False:
            self.checkbox_deinterlace.Enable()
            diction_command["interlace"] = ""
            
    #------------------------------------------------------------------#
    def radio_interlace(self, event):
        # TODO this is work in progress
        
        print "'radio_interlace' not implemented!"
        
    #------------------------------------------------------------------#
    def on_choice_vaspect(self, event):
        """
        Set parameter with choice and put in dict value 
        """
        if self.combobox_Vaspect.GetValue() == "Set default":
            diction_command["VideoAspect"] = ""
            
        else:
            diction_command["VideoAspect"] = self.combobox_Vaspect.GetValue()
            
    #------------------------------------------------------------------#
    def on_choice_vrate(self, event):
        """
        Set parameter with choice and put in dict value 
        """
        if self.combobox_Vrate.GetValue() == "Set default":
            diction_command["VideoRate"] = ""
            
        elif self.combobox_Vrate.GetValue() == "25 fps (50i) PAL":
            diction_command["VideoRate"] = "-r 25"
            
        elif self.combobox_Vrate.GetValue() == "29,97 fps (60i) NTSC":
            diction_command["VideoRate"] = "-r 29,97"
            
        elif self.combobox_Vrate.GetValue() == "30 fps (30p) Progessive":
            diction_command["VideoRate"] = "-r 30"
            
    #------------------------------------------------------------------#
    def setAudioRadiobox(self, event):
        """
        set audio radiobox selection with compatible audio codec
        """
        cmb_value = self.combobox_videoformat.GetValue()
        
        if diction_format [cmb_value] == "avi":
            self.rdb_a.EnableItem(0,enable=True)## dafault
            self.rdb_a.EnableItem(1,enable=True)## wav
            self.rdb_a.EnableItem(2,enable=False)# flac
            self.rdb_a.EnableItem(3,enable=False)# aac
            self.rdb_a.EnableItem(4,enable=False) # alac
            self.rdb_a.EnableItem(5,enable=True) ## ac3
            self.rdb_a.EnableItem(6,enable=False) # ogg
            self.rdb_a.EnableItem(7,enable=True) ## mp3
            self.rdb_a.EnableItem(8,enable=True)# copy
            self.rdb_a.EnableItem(9,enable=True)# manually
            self.rdb_a.EnableItem(10,enable=True) # no audio
            self.rdb_a.SetSelection(0)
        elif diction_format [cmb_value] == "flv" or \
                                    diction_format [cmb_value] == "mp4":
            self.rdb_a.EnableItem(0,enable=True)## dafault #
            self.rdb_a.EnableItem(1,enable=False)## wav
            self.rdb_a.EnableItem(2,enable=False)# flac
            self.rdb_a.EnableItem(3,enable=True)# aac #
            self.rdb_a.EnableItem(4,enable=False) # alac
            self.rdb_a.EnableItem(5,enable=True) ## ac3 #
            self.rdb_a.EnableItem(6,enable=False) # ogg
            self.rdb_a.EnableItem(7,enable=True) ## mp3 #
            self.rdb_a.EnableItem(8,enable=True)# copy
            self.rdb_a.EnableItem(9,enable=True)# manually
            self.rdb_a.EnableItem(10,enable=True) # no audio
            self.rdb_a.SetSelection(0)
        elif diction_format [cmb_value] == "m4v":
            self.rdb_a.EnableItem(0,enable=True)# dafault #
            self.rdb_a.EnableItem(1,enable=False)# wav
            self.rdb_a.EnableItem(2,enable=False)# flac
            self.rdb_a.EnableItem(3,enable=True)# aac #
            self.rdb_a.EnableItem(4,enable=True)# alac #
            self.rdb_a.EnableItem(5,enable=False)# ac3
            self.rdb_a.EnableItem(6,enable=False)# ogg
            self.rdb_a.EnableItem(7,enable=False)# mp3
            self.rdb_a.EnableItem(8,enable=True)# copy
            self.rdb_a.EnableItem(9,enable=True)# manually
            self.rdb_a.EnableItem(10,enable=True) # no audio
            self.rdb_a.SetSelection(0)
        elif diction_format [cmb_value] == "mkv":
            self.rdb_a.EnableItem(0,enable=True)# dafault #
            self.rdb_a.EnableItem(1,enable=True)# wav
            self.rdb_a.EnableItem(2,enable=True)# flac
            self.rdb_a.EnableItem(3,enable=True)# aac #
            self.rdb_a.EnableItem(4,enable=False)# alac #
            self.rdb_a.EnableItem(5,enable=True)# ac3
            self.rdb_a.EnableItem(6,enable=True)# ogg
            self.rdb_a.EnableItem(7,enable=True)# mp3
            self.rdb_a.EnableItem(8,enable=True)# copy
            self.rdb_a.EnableItem(9,enable=True)# manually
            self.rdb_a.EnableItem(10,enable=True) # no audio
            self.rdb_a.SetSelection(0)
        elif diction_format [cmb_value] == "webm":
            self.rdb_a.EnableItem(0,enable=True)# dafault #
            self.rdb_a.EnableItem(1,enable=False)# wav
            self.rdb_a.EnableItem(2,enable=False)# flac
            self.rdb_a.EnableItem(3,enable=False)# aac #
            self.rdb_a.EnableItem(4,enable=False)# alac #
            self.rdb_a.EnableItem(5,enable=False)# ac3
            self.rdb_a.EnableItem(6,enable=True)# ogg
            self.rdb_a.EnableItem(7,enable=False)# mp3
            self.rdb_a.EnableItem(8,enable=True)# copy
            self.rdb_a.EnableItem(9,enable=True)# manually
            self.rdb_a.EnableItem(10,enable=True) # no audio
            self.rdb_a.SetSelection(0)
        elif diction_format [cmb_value] == "ogg":
            self.rdb_a.EnableItem(0,enable=True)# dafault #
            self.rdb_a.EnableItem(1,enable=False)# wav
            self.rdb_a.EnableItem(2,enable=True)# flac
            self.rdb_a.EnableItem(3,enable=False)# aac #
            self.rdb_a.EnableItem(4,enable=False)# alac #
            self.rdb_a.EnableItem(5,enable=False)# ac3
            self.rdb_a.EnableItem(6,enable=True)# ogg
            self.rdb_a.EnableItem(7,enable=False)# mp3
            self.rdb_a.EnableItem(8,enable=True)# copy
            self.rdb_a.EnableItem(9,enable=True)# manually
            self.rdb_a.EnableItem(10,enable=True) # no audio
            self.rdb_a.SetSelection(0)
           
    #------------------------------------------------------------------#
    def choice_audio(self, event):
        """
        Qualche formato video supporta una limitata scelta di codec audio,
        quindi imposto le liste dei formati audio supportati in base al 
        formato video scelto
        """
        if self.checkbox_batch.GetValue() is False:
            self.checkbox_audio_normalize.Enable()
            
        audio = self.rdb_a.GetStringSelection()
        #diction_command["Audio"] = audio

        if audio == "Default":
            self.audio_default()
            self.normalize_default() # reset normalization parametrs
            
        #--------------------------------------------#
        elif audio == "Wav (Raw, No_MultiChannel)":
            diction_command["AudioCodec"] = "-acodec pcm_s16le"
            self.audio_("wav", "Audio WAV Codec Parameters")
            
        elif audio == "Flac (Lossless, No_MultiChannel)":
            diction_command["AudioCodec"] = "-acodec flac"
            self.audio_("flac", "Audio FLAC Codec Parameters")

        elif audio == "Aac (Lossy, MultiChannel)":
            diction_command["AudioCodec"] = "-acodec libfaac"
            self.audio_("aac", "Audio AAC Codec Parameters")
            
        elif audio == "Alac (Lossless, m4v, No_MultiChannel)":
            diction_command["AudioCodec"] = "-acodec alac"
            self.audio_("alac", "Audio ALAC Codec Parameters")
            
        elif audio == "Ac3 (Lossy, MultiChannel)":
            diction_command["AudioCodec"] = "-acodec ac3"
            self.audio_("ac3", "Audio AC3 Codec Parameters")
        
        elif audio == "Ogg (Lossy, No_MultiChannel)":
            diction_command["AudioCodec"] = "-acodec libvorbis"
            self.audio_("ogg", "Audio OGG Codec Parameters")
        
        elif audio == "Mp3 (Lossy, No_MultiChannel)":
            diction_command["AudioCodec"] = "-acodec libmp3lame"
            self.audio_("mp3", "Audio MP3 Codec Parameters")

        elif audio == "Copy audio source":
            self.audio_default()
            self.normalize_default() # reset normalization parametrs
            self.rdb_a.SetSelection(8)
            diction_command["Audio"] = audio
            diction_command["AudioCodec"] = "-acodec copy"
            wx.MessageBox("The copy of the audio from one video format "
                          "to another does not guarantee the success of the "
                          "conversion, as some video formats are incompatible "
                          "with some audio codecs or with particular "
                          "parameters, such as a high sample rate.",
                          "copy audio sourse advise",
                          wx.OK, self)

        elif audio == "Set manually (for experts)":
            dlg = wx.TextEntryDialog(self, 'Codec audio command:', 
            'Manually Parameters Audio Coced', style=wx.TE_MULTILINE | wx.OK | 
                        wx.CANCEL | wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER
                                     )
            diction_command["AudioRate"] = ""
            diction_command["AudioChannel"] = ""
            diction_command["AudioBitrate"] = ""
            
            if dlg.ShowModal() == wx.ID_OK:
                diction_command["AudioCodec"] = "%s" % dlg.GetValue()
                
            else:
                data = ''
                self.audio_default() # reset audio radiobox and dict
                self.normalize_default() # reset normalization parametrs

        elif audio == "Encoding video with no audio":
            self.audio_default()
            self.normalize_default() # reset normalization parametrs
            self.rdb_a.SetSelection(10)
            diction_command["AudioCodec"] = "-an"
            diction_command["Audio"] = audio
            
    #-------------------------------------------------------------------#
    def audio_(self, audio_type, title):
        """
        Run a dialogs for audio Choice parameters
        """
        audiodialog = audiodialogs.AudioSettings(self,audio_type,title)
        retcode = audiodialog.ShowModal()
        
        if retcode == wx.ID_OK:
            diction_command["AudioRate"] = "" # reset if in dict
            diction_command["AudioChannel"] = "" # reset if in dict
            diction_command["AudioBitrate"] = "" # reset if in dict
            data = audiodialog.GetValue()
            diction_command["AudioChannel"] = data[0]
            diction_command["AudioRate"] = data[1]
            diction_command["AudioBitrate"] = data[2]
        else:
            data = ''
            self.audio_default() # reset audio radiobox and dict
            self.normalize_default() # reset normalization parametrs
        
        audiodialog.Destroy()
        
    #------------------------------------------------------------------#
    def push_audio_normalize(self, event):  # check box
        """
        Enable or disable functionality for volume normalization of
        the video. Not enable if batch mode is enable
        """
        msg = ("Before clicking the Analyze button, set the normalize "
              "maximum amplitude in the spin-control, or accept default"
              " value (-1.0)")
        if self.checkbox_audio_normalize.GetValue() is True:
            self.statusbar_msg(msg, yellow)
            self.button_analyzes.Enable(), self.spin_ctrl_audionormalize.Enable()
            self.text_dbMax.Enable(), self.text_dbMedium.Enable()
            self.label_dbMax.Enable(), self.label_dbMedium.Enable()
            self.label_normalize.Enable()
            self.button_analyzes.SetBackgroundColour(wx.Colour(255, 115, 106))# color
            diction_command["Normalize"] = ""
            
            
            
        elif self.checkbox_audio_normalize.GetValue() is False:
            self.statusbar_msg("Disable audio normalization inside video", None)
            self.spin_ctrl_audionormalize.SetValue(-1.0), self.label_normalize.Disable()
            self.button_analyzes.Disable(), self.spin_ctrl_audionormalize.Disable()
            self.text_dbMax.SetValue(""), self.text_dbMedium.SetValue("")
            self.text_dbMax.Disable(), self.text_dbMedium.Disable()
            self.label_dbMax.Disable(), self.label_dbMedium.Disable()
            self.button_analyzes.SetBackgroundColour(wx.NullColour)# norm
            diction_command["Normalize"] = ""
            
            
    #------------------------------------------------------------------#
    def on_audio_analyzes(self, event):  # analyzes button
        """
        If push button, start control for values volume in db. This call
        function 'os_processing.proc_volumedetect'. Not enable if batch 
        mode is enable
        """
        self.statusbar_msg("",None)
        filepath = self.text_path_open.GetValue()
        alias_vol = proc_volumedetect(filepath, self.ffmpeg_link) # chiamo os_processing.proc_volumedetect
        self.text_dbMax.SetValue(""), self.text_dbMedium.SetValue("")
        
        if  alias_vol == None:
            return
        
        else:
            max_volume = alias_vol[0] # index int of raw_list
            mean_volume = alias_vol[1] # index int of raw_list
            maxvol_append = alias_vol[2] # composed string max vol
            medvol_append = alias_vol[3] # composed string med vol
            raw_list = alias_vol[4] # list
            self.text_dbMax.AppendText(maxvol_append)
            self.text_dbMedium.AppendText(medvol_append)
            normalize = self.spin_ctrl_audionormalize.GetValue()
            maxvol = raw_list[max_volume + 1] # next item of index
            result = float(maxvol) - float(normalize)
            result_str = str(result)
            diction_command["Normalize"] = "-af volume=%sdB" % (result_str[1:])
            
    #------------------------------------------------------------------#
    #def enter_normalize(self, event):  # slider_audionormalize
        #event.Skip()
        
    #------------------------------------------------------------------#
    def choice_presets(self, event):
        """
        Set only for h264
        """
        diction_command["Presets"] = "-preset:v %s" % (
                                    self.radiobox_presets.GetStringSelection()
                                                        )
    #------------------------------------------------------------------#
    def choice_profile(self, event):
        """
        Set only for h264
        """
        if self.radiobox_profile.GetStringSelection() == "Default":
            diction_command["Profile"] = ""
        else:
            diction_command["Profile"] = "-profile:v %s" % (
                                    self.radiobox_profile.GetStringSelection()
                                                             )
    #------------------------------------------------------------------#
    def choice_tune(self, event):
        """
        Set only for h264
        """
        if self.radiobox_tune.GetStringSelection() == "Default":
            diction_command["Tune"] = ""
        else:
            diction_command["Tune"] = "-tune:v %s" % (
                                        self.radiobox_tune.GetStringSelection()
                                                      )
    #------------------------------------------------------------------#
    def on_ok(self):
        """
        Here pass on std_conv(self) or proc_batch_thread(self) if the choice
        is standard or multiple files conversion. Also check user setting 
        parameters before start processes 
        """
        acodec_list = ["Default", "-acodec copy", "-an", ""]

        
        if diction_command["AudioCodec"] == "Default":
            diction_command["AudioCodec"] = ""
            
        if diction_command["Batch"] == "single file conversion":
            
            if diction_command["AudioCodec"] in acodec_list:
                diction_command["Normalize"] = ""
                
            elif self.checkbox_audio_normalize.GetValue() is True and \
                                self.text_dbMax.GetValue() == "" and \
                                self.text_dbMedium.GetValue() == "":
                            
                self.statusbar_msg("ERROR: have enabled the normalization but "
                                       "lack the data analyzed. Press the "
                                       "Analyze button or turn off the "
                                       "normalization !", red)
                return # exit from function
            
            self.std_conv()
            
        elif diction_command["Batch"] == "group files directory":
            
            diction_command["Normalize"] = ""
            self.batch_conv()
            
    #------------------------------------------------------------------#
    def std_conv(self):
        """
        Method for single file process with or no two-pass encoding
        """
        diction_command["ext_input"] = ""
        
        if self.ffmpeg_log == 'true':
            report = '-report'
        else:
            report = ''
            
        if self.spin_size_height.GetValue() != 0 and \
                                        self.spin_size_width.GetValue() != 0:
                                    
            diction_command["VideoSize"] = "-s %sx%s" % (
                                            self.spin_size_width.GetValue(), 
                                            self.spin_size_height.GetValue()
                                                         )
        if self.checkbox_scale.GetValue() is True:
            diction_command["VideoSize"] = diction_command["scale"]
                
        if self.combobox_Vaspect.GetValue() != "Set default":
            diction_command["VideoAspect"] = "-aspect %s" % (
                                            self.combobox_Vaspect.GetValue()
                                                             )
            
        #################################################### Use Codec h264
        if diction_command["VideoCodec"] == "-vcodec libx264":
            if "double_pass" in array:
                diction_command["Bitrate"] = "-vb %sk" % (
                                            self.spin_ctrl_bitrate.GetValue()
                                            )
                diction_command["Passing"] = "double"
                diction_command["CRF"] = ""
                diction_command["Presets"] = "-preset:v %s" % (
                                    self.radiobox_presets.GetStringSelection()
                                    )
                command = ("%s %s -i '%s' -loglevel %s -pass 1 -an %s "
                        "%s %s %s %s %s %s %s %s %s %s %s -f rawvideo -y "
                        "/dev/null && %s %s -i '%s' -loglevel "
                        "%s -pass 2 %s %s %s %s %s %s %s %s %s %s %s %s "
                        "%s %s %s %s %s -y '%s'" % (self.ffmpeg_link, report,
                diction_command["InputDir"], self.loglevel_type,
                diction_command["VideoCodec"],
                diction_command["Bitrate"], diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["Presets"],
                diction_command["Profile"], diction_command["Tune"],
                diction_command["VideoSize"], diction_command["VideoAspect"],
                diction_command["VideoRate"], self.threads,
                diction_command["testing"],
                self.ffmpeg_link, report,
                diction_command["InputDir"], self.loglevel_type,
                diction_command["VideoCodec"], diction_command["Bitrate"], 
                diction_command["interlace"], diction_command["deinterlace"],
                diction_command["Presets"], diction_command["Profile"],
                diction_command["Tune"], diction_command["VideoSize"],
                diction_command["VideoAspect"], diction_command["VideoRate"],
                diction_command["AudioCodec"], diction_command["AudioBitrate"],
                diction_command["AudioRate"], diction_command["AudioChannel"],
                diction_command["Normalize"], self.threads, 
                diction_command["testing"],diction_command["OutputDir"])
                            )
                valupdate = self.update_dict()
                ending = Formula(self, valupdate[0], valupdate[1])
                
                if ending.ShowModal() == wx.ID_OK:
                    dial = proc_std(self, 'Videomass - Run single process', 
                                    command, self.path_log, 
                                    'Videomass_video_sci.log', 
                                    self.command_log
                                    )
                #ending.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()
            else:
                diction_command["CRF"] = "-crf %s" % self.slider_CRF.GetValue()
                diction_command["Passing"] = "single"
                diction_command["Bitrate"] = ""
                diction_command["Presets"] = "-preset:v %s" % (
                                    self.radiobox_presets.GetStringSelection()
                                                               )
                command = ("%s %s -i '%s' -loglevel %s %s %s %s %s %s %s"
                        "%s %s %s %s %s %s %s %s %s %s %s -y '%s'\n" % (
                self.ffmpeg_link, report, diction_command["InputDir"], 
                self.loglevel_type, diction_command["VideoCodec"],
                diction_command["CRF"], diction_command["Presets"],
                diction_command["Profile"], diction_command["Tune"],
                diction_command["interlace"], diction_command["deinterlace"],
                diction_command["VideoSize"], diction_command["VideoAspect"],
                diction_command["VideoRate"], diction_command["AudioCodec"],
                diction_command["AudioBitrate"],diction_command["AudioRate"],
                diction_command["AudioChannel"], diction_command["Normalize"],
                self.threads, diction_command["testing"], 
                diction_command["OutputDir"])
                )
                valupdate = self.update_dict()
                ending = Formula(self, valupdate[0], valupdate[1])

                if ending.ShowModal() == wx.ID_OK:
                    
                    dial = proc_std(self, 'Videomass - Run single process', 
                                    command, self.path_log, 
                                    'Videomass_video_sci.log', 
                                    self.command_log
                                    )
                #ending.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()

        ####################################################### Not Use h264
        if diction_command["VideoCodec"] != "-vcodec libx264":
            
            diction_command["CRF"] = ""
            diction_command["Presets"] = ""
            diction_command["Profile"] = ""
            diction_command["Tune"] = ""
            diction_command["Bitrate"] = "-vb %sk" % (
                                            self.spin_ctrl_bitrate.GetValue()
                                            )
            if "double_pass" in array:
                diction_command["Passing"] = "double"
                command = ("%s %s -i '%s' -loglevel %s -pass 1 -an %s "
                        "%s %s %s %s %s %s %s %s -f rawvideo -y /dev/null && "
                        "%s %s -i '%s' -loglevel %s -pass 2 %s %s"
                        "%s %s %s %s %s %s %s %s %s %s %s %s -y '%s'" % (
                self.ffmpeg_link, report, diction_command["InputDir"], 
                self.loglevel_type, diction_command["VideoCodec"],
                diction_command["Bitrate"], diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["VideoSize"],
                diction_command["VideoAspect"], diction_command["VideoRate"],
                self.threads, diction_command["testing"],
                self.ffmpeg_link, report, diction_command["InputDir"], 
                self.loglevel_type, diction_command["VideoCodec"], 
                diction_command["Bitrate"], diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["VideoSize"],
                diction_command["VideoAspect"], diction_command["VideoRate"],
                diction_command["AudioCodec"], diction_command["AudioBitrate"],
                diction_command["AudioRate"], diction_command["AudioChannel"],
                diction_command["Normalize"], self.threads, 
                diction_command["testing"], diction_command["OutputDir"])
                           )
                valupdate = self.update_dict()
                ending = Formula(self, valupdate[0], valupdate[1])
                
                if ending.ShowModal() == wx.ID_OK:
                    dial = proc_std(self, 'Videomass - Run single process', 
                                    command, self.path_log, 
                                    'Videomass_video_sci.log', 
                                    self.command_log
                                    )
                #ending.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()
            else:
                diction_command["Passing"] = "single"
                command = ("%s %s -i '%s' -loglevel %s %s %s %s %s %s %s "
                        "%s %s %s %s %s %s %s %s -y '%s'\n" % (
                self.ffmpeg_link, report, diction_command["InputDir"], 
                self.loglevel_type, diction_command["VideoCodec"],
                diction_command["Bitrate"], diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["VideoSize"],
                diction_command["VideoAspect"], diction_command["VideoRate"],
                diction_command["AudioCodec"], diction_command["AudioBitrate"],
                diction_command["AudioRate"], diction_command["AudioChannel"],
                diction_command["Normalize"], self.threads,
                diction_command["testing"], diction_command["OutputDir"])
                           )
                valupdate = self.update_dict()
                ending = Formula(self, valupdate[0], valupdate[1])
                
                if ending.ShowModal() == wx.ID_OK:
                    dial = proc_std(self, 'Videomass - Run single process', 
                                    command, self.path_log, 
                                    'Videomass_video_sci.log', 
                                    self.command_log
                                    )
                #ending.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()

    #------------------------------------------------------------------#
    def batch_conv(self):
        """
        Function for batch process. In double pass mode split command in two
        part for place iteration cicles files ('for' istruction). For more 
        understanding see os_processing.py module at proc_batch_thread.
        """
        if self.text_group_ext.GetValue() == "":
            wx.MessageBox("No group extension specified "
                          "for the batch process.\nPlease, write "
                          "in the box 'Group' the extension of "
                          "the files to be processed.",
                          'Missing Extension Format', wx.OK | 
                          wx.ICON_ERROR, self)
            return

        if self.spin_size_height.GetValue() != 0 and \
                                        self.spin_size_width.GetValue() != 0:
            diction_command["VideoSize"] = "-s %sx%s" % (
                                              self.spin_size_width.GetValue(), 
                                              self.spin_size_height.GetValue()
                                                          )
        if self.checkbox_scale.GetValue() is True:
            diction_command["VideoSize"] = diction_command["scale"]
            
        if self.combobox_Vaspect.GetValue() != "Set default":
            diction_command["VideoAspect"] = "-aspect %s" %(
            self.combobox_Vaspect.GetValue()
            )
            
        ############# Batch-Mode Use h264 Codec #############
        if diction_command["VideoCodec"] == "-vcodec libx264":
            if "double_pass" in array: # Double-pass / Batch / Use h264
                diction_command["Bitrate"] = "-vb %sk" % (
                                             self.spin_ctrl_bitrate.GetValue()
                                                          )
                diction_command["Passing"] = "double"
                diction_command["CRF"] = ""
                diction_command["Presets"] = "-preset:v %s" % (
                                    self.radiobox_presets.GetStringSelection()
                                                               )
                command1 = ("-loglevel %s -pass 1 -an %s %s %s "
                            "%s %s %s %s %s %s %s %s %s -f rawvideo "
                            "-y /dev/null &&" % (
                self.loglevel_batch,
                diction_command["VideoCodec"], diction_command["Bitrate"],
                diction_command["Presets"], diction_command["Profile"],
                diction_command["Tune"], diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["VideoSize"],
                diction_command["VideoAspect"], diction_command["VideoRate"],
                self.threads, diction_command["testing"])
                             )
                command2 = ("-loglevel %s -pass 2 %s %s %s %s %s %s %s %s "
                            "%s %s %s %s %s %s %s %s -y" % (
                self.loglevel_batch,
                diction_command["VideoCodec"],diction_command["Bitrate"],
                diction_command["Presets"], diction_command["Profile"],
                diction_command["Tune"], diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["VideoSize"],
                diction_command["VideoAspect"], diction_command["VideoRate"],
                diction_command["AudioCodec"], diction_command["AudioBitrate"],
                diction_command["AudioRate"], diction_command["AudioChannel"], 
                self.threads,diction_command["testing"])
                            )
                cmdlog = ("%s -i '%s/*.%s' %s %s -i '%s/*.%s'  %s '%s/*.%s' " % (
                            self.ffmpeg_link, diction_command["InputDir"], 
                            diction_command["ext_input"], command1, self.ffmpeg_link, 
                            diction_command["InputDir"],
                            diction_command["ext_input"], command2, 
                            diction_command["OutputDir"],
                            diction_command['VideoFormat'])
                            )
                valupdate = self.update_dict()
                ending = Formula(self, valupdate[0], valupdate[1])
                
                if ending.ShowModal() == wx.ID_OK:
                    status = control_errors(diction_command["InputDir"],
                                            diction_command["ext_input"],cmdlog, 
                                            'Videomass_video_sci.log'
                                            )
                    if  status[0] == 'no_error':
                        proc_batch_thread(status[2], diction_command["InputDir"], 
                                            diction_command['VideoFormat'], 
                                            diction_command["OutputDir"], 
                                            None, command1, command2,
                                            'Videomass_video_sci.log',
                                            self.ffmpeg_link
                                            )
                        dial = ProgressDialog(self, 'Videomass Batch Process', 
                                    status[1], diction_command["ext_input"],
                                    diction_command['VideoFormat'],
                                    diction_command["OutputDir"], self.path_log, 
                                    self.command_log, 'Videomass_video_sci.log'
                                                )
                #ending.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()
                
            else: # Not double-pass / Batch-Mode / Use h264 Codec
                diction_command["CRF"] = "-crf %s" % (self.slider_CRF.GetValue())
                diction_command["Passing"] = "single"
                diction_command["Bitrate"] = ""
                diction_command["Presets"] = "-preset %s" % (
                self.radiobox_presets.GetStringSelection()
                )
                command = ("-loglevel %s %s %s %s %s %s %s %s %s %s %s "
                            "%s %s %s %s %s %s -y" % (
                self.loglevel_batch,
                diction_command["VideoCodec"], diction_command["CRF"],
                diction_command["Presets"], diction_command["Profile"],
                diction_command["Tune"], diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["VideoSize"],
                diction_command["VideoAspect"], diction_command["VideoRate"],
                diction_command["AudioCodec"], diction_command["AudioBitrate"],
                diction_command["AudioRate"], diction_command["AudioChannel"],
                self.threads,diction_command["testing"])
                            )
                cmdlog = ("%s -i '%s/*.%s' %s '%s/*.%s'" % (
                            self.ffmpeg_link, diction_command["InputDir"], 
                            diction_command["ext_input"], command, 
                            diction_command["OutputDir"],
                            diction_command['VideoFormat'])
                            )
                valupdate = self.update_dict()
                ending = Formula(self, valupdate[0], valupdate[1])
                
                if ending.ShowModal() == wx.ID_OK:
                    status = control_errors(diction_command["InputDir"],
                                            diction_command["ext_input"],cmdlog, 
                                            'Videomass_video_sci.log'
                                            )
                    if  status[0] == 'no_error':
                        proc_batch_thread(status[2], diction_command["InputDir"], 
                                            diction_command['VideoFormat'], 
                                            diction_command["OutputDir"], 
                                            command, None, None, 
                                            'Videomass_video_sci.log',
                                            self.ffmpeg_link
                                            )
                        dial = ProgressDialog(self, 'Videomass Batch Process', 
                                    status[1], diction_command["ext_input"],
                                    diction_command['VideoFormat'],
                                    diction_command["OutputDir"], self.path_log, 
                                    self.command_log, 'Videomass_video_sci.log'
                                                )
                #ending.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()

        ################################################### Not Use h264 Codec
        if diction_command["VideoCodec"] != "-vcodec libx264":
            
            diction_command["CRF"] = ""
            diction_command["Presets"] = ""
            diction_command["Profile"] = ""
            diction_command["Tune"] = ""
            diction_command["Bitrate"] = "-vb %sk" % self.spin_ctrl_bitrate.GetValue()

            if "double_pass" in array: # Double-pass / Batch / Not Use h264
                diction_command["Passing"] = "double"
                command1 = ("-loglevel %s -pass 1 -an %s %s %s %s %s %s "
                            "%s %s %s -f rawvideo -y /dev/null &&" % (
                self.loglevel_batch,
                diction_command["VideoCodec"], diction_command["Bitrate"],
                diction_command["interlace"], diction_command["deinterlace"],
                diction_command["VideoSize"], diction_command["VideoAspect"],
                diction_command["VideoRate"], self.threads,
                diction_command["testing"])
                            )
                command2 = ("-loglevel %s -pass 2 %s %s %s %s %s %s %s %s "
                            "%s %s %s %s %s -y" % (
                self.loglevel_batch,
                diction_command["VideoCodec"], diction_command["Bitrate"],
                diction_command["interlace"], diction_command["deinterlace"],
                diction_command["VideoSize"], diction_command["VideoAspect"],
                diction_command["VideoRate"], diction_command["AudioCodec"],
                diction_command["AudioBitrate"], diction_command["AudioRate"],
                diction_command["AudioChannel"], self.threads, 
                diction_command["testing"])
                            )
                cmdlog = ("%s -i '%s/*.%s' %s %s -i %s '%s/*.%s' %s '%s/*.%s' " % (
                            self.ffmpeg_link, diction_command["InputDir"], 
                            diction_command["ext_input"], command1, 
                            self.ffmpeg_link, diction_command["InputDir"],
                            diction_command["ext_input"], command2, 
                            diction_command["OutputDir"],
                            diction_command['VideoFormat'])
                            )
                valupdate = self.update_dict()
                ending = Formula(self, valupdate[0], valupdate[1])
                
                if ending.ShowModal() == wx.ID_OK:
                    status = control_errors(diction_command["InputDir"],
                                            diction_command["ext_input"],cmdlog, 
                                            'Videomass_video_sci.log'
                                            )
                    if  status[0] == 'no_error':
                        proc_batch_thread(status[2], diction_command["InputDir"], 
                                            diction_command['VideoFormat'], 
                                            diction_command["OutputDir"], 
                                            None, command1, command2,
                                            'Videomass_video_sci.log',
                                            self.ffmpeg_link
                                            )
                        dial = ProgressDialog(self, 'Videomass Batch Process', 
                                    status[1], diction_command["ext_input"],
                                    diction_command['VideoFormat'],
                                    diction_command["OutputDir"], self.path_log, 
                                    self.command_log, 'Videomass_video_sci.log'
                                                )
                #ending.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()
                    
            else: # Not double-pass / Batch-Mode / Not h264 Codec
                diction_command["Passing"] = "single"
                command = ("-loglevel %s %s %s %s %s %s %s %s %s %s %s %s "
                            "%s %s -y" % (self.loglevel_batch,
                diction_command["VideoCodec"],diction_command["Bitrate"], 
                diction_command["interlace"], diction_command["deinterlace"],
                diction_command["VideoSize"], diction_command["VideoAspect"], 
                diction_command["VideoRate"], diction_command["AudioCodec"], 
                diction_command["AudioBitrate"], diction_command["AudioRate"], 
                diction_command["AudioChannel"], self.threads, 
                diction_command["testing"])
                            )
                cmdlog = ("%s -i '%s/*.%s' %s '%s/*.%s'" % (
                            self.ffmpeg_link, diction_command["InputDir"], 
                            diction_command["ext_input"], 
                            command, diction_command["OutputDir"],
                            diction_command['VideoFormat'])
                            )
                valupdate = self.update_dict()
                ending = Formula(self, valupdate[0], valupdate[1])
                
                if ending.ShowModal() == wx.ID_OK:
                    status = control_errors(diction_command["InputDir"],
                                            diction_command["ext_input"],cmdlog, 
                                            'Videomass_video_sci.log'
                                            )
                    if  status[0] == 'no_error':
                        proc_batch_thread(status[2], diction_command["InputDir"], 
                                            diction_command['VideoFormat'], 
                                            diction_command["OutputDir"], 
                                            command, None, None, 
                                            'Videomass_video_sci.log',
                                            self.ffmpeg_link
                                            )
                        dial = ProgressDialog(self, 'Videomass Batch Process', 
                                    status[1], diction_command["ext_input"],
                                    diction_command['VideoFormat'],
                                    diction_command["OutputDir"], self.path_log, 
                                    self.command_log, 'Videomass_video_sci.log'
                                                )
                #ending.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()
                
    #------------------------------------------------------------------#
    def update_dict(self):
        """
        This method is required for update all diction_command
        dictionary values before send at epilogue
        """
        formula = (u"FORMULATIONS:\n\nConversion Mode:\nVideo Format:\
                \nVideo codec:\nVideo bit-rate:\nCRF:\nDouble/Single Pass:\
                \nDeinterlacing:\nInterlacing (progressive content)\
                \nVideo size:\nVideo aspect:\nVideo rate:\nPreset h264:\
                \nProfile h264:\nTune h264:\nAudio Format:\nAudio codec:\
                \nAudio channel:\nAudio rate:\nAudio bit-rate:\
                \nAudio Normalization\nInput dir/file:\nOutput dir/file:\
                \nExtension Files Group:\nVideo Testing (time second):")
        dictions = ("\n\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\
                \n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s" % (
                diction_command["Batch"], diction_command["FormatChoice"], 
                diction_command["VideoCodec"], diction_command["Bitrate"], 
                diction_command["CRF"], diction_command["Passing"],
                diction_command["deinterlace"], diction_command["interlace"],
                diction_command["VideoSize"], diction_command["VideoAspect"],
                diction_command["VideoRate"], diction_command["Presets"],
                diction_command["Profile"], diction_command["Tune"],
                diction_command["Audio"], diction_command["AudioCodec"],
                diction_command["AudioChannel"], diction_command["AudioRate"],
                diction_command["AudioBitrate"], diction_command["Normalize"],
                diction_command["InputDir"], diction_command["OutputDir"], 
                diction_command["ext_input"], diction_command["testing"])
                )
        return formula, dictions

#	---------------------------Build Menu Bar---------------------------#
    def menu_bar(self):
        """
        Makes and attaches the view menu

        """
        menuBar = wx.MenuBar()
        
        #----------------------- File
        fileButton = wx.Menu() 
                                    
        #fileButton.AppendSeparator()
        
        file_open = fileButton.Append(wx.ID_OPEN, "Open.. ", 
                        "Import files/directory")
        self.file_save = fileButton.Append(wx.ID_SAVE, "Save.. ", 
                        "Export files/directory")
        self.file_save.Enable(False)

        fileButton.AppendSeparator()
        
        exitItem = fileButton.Append(wx.ID_EXIT, 'Exit', 'Exit now...')
        
        menuBar.Append(fileButton,"File")
        
        #------------------- Strumenti
        toolsButton = wx.Menu()
        
        addprof = toolsButton.Append(wx.ID_ADD, "Save this profile on the "
                                                "Presets Manager", 
                           "Stores the profile as to current values into "
                           "'Your personal profiles' of the Presets_Manager")
        
        menuBar.Append(toolsButton,"Strumenti")
        
        #------------------ help buton
        helpButton = wx.Menu() 
        
        helpItem = helpButton.Append(wx.ID_HELP, "User Guide", "Program Guide")
        
        infoItem = helpButton.Append(wx.ID_ABOUT, "About Videomass", "Program Info")
        
        menuBar.Append(helpButton,"Help")
        
        # ...and set, finally .
        self.SetMenuBar(menuBar)
        #-----------------------Binding menu bar-------------------------#
        # menu file
        self.Bind(wx.EVT_MENU, self.File_Open, file_open)
        self.Bind(wx.EVT_MENU, self.File_Save, self.file_save)
        self.Bind(wx.EVT_MENU, self.Quiet, exitItem)
        # menu tools
        self.Bind(wx.EVT_MENU, self.Addprof, addprof)
        # menu help
        self.Bind(wx.EVT_MENU, self.Helpme, helpItem)
        self.Bind(wx.EVT_MENU, self.Info, infoItem)

    #-----------------Callback menu bar (event handler)------------------#
    #------------------------------------------------------------------#
    def File_Open(self, event):
        """
        Go in the on_open function for controls import file/dir
        """
        self.on_open(self)
    #------------------------------------------------------------------#
    def File_Save(self, event):
        """
        Go in the on_save function for controls export file/dir
        """
        self.on_save(self)
    #------------------------------------------------------------------#
    def Quiet(self, event):
        """
        Show parent and quiet current window.
        """
        self.parent.Show()
        self.Destroy()
    #------------------------------------------------------------------#
    def Addprof(self, event):
        """
        This store new personal profiles to a presets manager.
        The settings of saved profiles depend at the current setting of
        this frame (video_sci). 
        
        TODO have any problem with xml escapes in special character
        (like && for ffmpeg double pass), so there is some to get around it 
        (escamotage), but work .
        
        """
        if self.spin_size_height.GetValue() != 0 and \
                                        self.spin_size_width.GetValue() != 0:
            
            diction_command["VideoSize"] = "-s %sx%s" % (
                                            self.spin_size_width.GetValue(), 
                                            self.spin_size_height.GetValue()
                                            )
        if self.checkbox_scale.GetValue() is True:
            diction_command["VideoSize"] = diction_command["scale"]
            
        if self.combobox_Vaspect.GetValue() != "Set default":
            
            diction_command["VideoAspect"] = "-aspect %s" %(
                                            self.combobox_Vaspect.GetValue()
                                            )
        if diction_command["VideoCodec"] == "-vcodec libx264":
            if "double_pass" in array:

                diction_command["CRF"] = ""
                diction_command["Bitrate"] = "-vb %sk" % (
                self.spin_ctrl_bitrate.GetValue())
                diction_command["Presets"] = "-preset:v %s" % (
                self.radiobox_presets.GetStringSelection())
                
                command = ("-pass 1 -an %s %s %s %s %s %s %s %s %s %s %s "
                        "DOUBLE_PASS -pass 2 %s %s %s %s %s %s %s %s %s"
                        "%s %s %s %s %s %s %s" % (
                diction_command["VideoCodec"], diction_command["Bitrate"], 
                diction_command["CRF"], diction_command["Presets"],
                diction_command["Profile"], diction_command["Tune"],
                diction_command["interlace"], diction_command["deinterlace"],
                diction_command["VideoSize"], diction_command["VideoAspect"],
                diction_command["VideoRate"], diction_command["VideoCodec"], 
                diction_command["Bitrate"], diction_command["CRF"],
                diction_command["Presets"], diction_command["Profile"],
                diction_command["Tune"], diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["VideoSize"],
                diction_command["VideoAspect"], diction_command["VideoRate"],
                diction_command["AudioCodec"], diction_command["AudioBitrate"],
                diction_command["AudioRate"], diction_command["AudioChannel"],
                diction_command["Normalize"]))
            
            else: 
                diction_command["CRF"] = "-crf %s" % self.slider_CRF.GetValue()
                diction_command["Bitrate"] = ""
                
                diction_command["Presets"] = "-preset:v %s" % (
                self.radiobox_presets.GetStringSelection())
                
                command = ("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s "
                            "%s " % (diction_command["VideoCodec"], 
                diction_command["Bitrate"], diction_command["CRF"], 
                diction_command["Presets"], diction_command["Profile"], 
                diction_command["Tune"], diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["VideoSize"], 
                diction_command["VideoAspect"], diction_command["VideoRate"], 
                diction_command["AudioCodec"], diction_command["AudioBitrate"], 
                diction_command["AudioRate"], diction_command["AudioChannel"],
                diction_command["Normalize"]))
                
        if diction_command["VideoCodec"] != "-vcodec libx264":
            
            diction_command["Bitrate"] = "-vb %sk" % (
            self.spin_ctrl_bitrate.GetValue())

            if "double_pass" in array:

                command = ("-pass 1 -an %s %s %s %s %s %s %s DOUBLE_PASS "
                            "-pass 2 %s %s %s %s %s %s %s %s %s %s %s %s" % ( 
                diction_command["VideoCodec"], diction_command["Bitrate"],
                diction_command["interlace"], diction_command["deinterlace"],
                diction_command["VideoSize"], diction_command["VideoAspect"],
                diction_command["VideoRate"], diction_command["VideoCodec"], 
                diction_command["Bitrate"],  diction_command["interlace"], 
                diction_command["deinterlace"],diction_command["VideoSize"],
                diction_command["VideoAspect"], diction_command["VideoRate"],
                diction_command["AudioCodec"], diction_command["AudioBitrate"],
                diction_command["AudioRate"], diction_command["AudioChannel"],
                diction_command["Normalize"]))
                
            else:
                command = ("%s %s %s %s %s %s %s %s %s %s %s %s" % ( 
                diction_command["VideoCodec"], diction_command["Bitrate"],
                diction_command["interlace"], diction_command["deinterlace"],
                diction_command["VideoSize"], diction_command["VideoAspect"],
                diction_command["VideoRate"], diction_command["AudioCodec"], 
                diction_command["AudioBitrate"], diction_command["AudioRate"], 
                diction_command["AudioChannel"], diction_command["Normalize"]))

        command_split = string.split(command) # become list
        
        if "Default" in command_split: # Default is not ffmpeg command, than remove.
            command_split.remove("Default")
            
        command_join = string.join(command_split) # become string
        list = [command_join, diction_command["VideoFormat"]]

        filename = 'preset-v1-Personal'
        name_preset = 'Your personal profiles'
        full_pathname = os.path.expanduser('~/.videomass/preset-v1-Personal.vdms')
        
        prstdlg = presets_addnew.MemPresets(self, 'addprofile', full_pathname, 
                                 filename, list, 'Create a new '
                                 'profile on the selected preset: "%s"' % (
                                 name_preset))
        prstdlg.ShowModal()
        
    #------------------------------------------------------------------#
    def Helpme(self, event):
        
        webbrowser.open('%s/Indice_ipertestuale.html' % self.helping)
        
    #------------------------------------------------------------------#
    def Info(self, event):
        """
        Display the program informations and developpers
        """
        infoprg.info(self.videomass_icon)
        
    #------------------------Build the Tool Bar--------------------------#
    def tool_bar(self):
        """
        Makes and attaches the view tools bar
        """
        #--------- Properties
        self.toolbar = self.CreateToolBar(style=(wx.TB_HORZ_LAYOUT | wx.TB_TEXT))
        #self.toolbar.SetToolBitmapSize((32,32))
        self.toolbar.SetFont(wx.Font(8, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))
        
        #-------- Play button
        play = self.toolbar.AddLabelTool(wx.ID_FILE1, ' Play', 
                                        wx.Bitmap(self.icon_play)
                                        )
        self.toolbar.EnableTool(wx.ID_FILE1, False)
        self.toolbar.AddSeparator()
        
        #-------- Analyze button
        analyze = self.toolbar.AddLabelTool(wx.ID_FILE2, 'Info Media', 
                                    wx.Bitmap(self.icon_analyze))
        self.toolbar.EnableTool(wx.ID_FILE2, False)
        self.toolbar.AddSeparator()
        
        # ------- Presets button
        presets = self.toolbar.AddLabelTool(wx.ID_ANY, ' Presets Manager', 
                                    wx.Bitmap(self.icon_presets))
        self.toolbar.AddSeparator()
        
        # ------- Run process button
        run_coding = self.toolbar.AddLabelTool(wx.ID_OK, ' Start Encoding', 
                                    wx.Bitmap(self.icon_process))
        self.toolbar.EnableTool(wx.ID_OK, False)
        self.toolbar.AddSeparator()
        
        # ------- help button
        help_contest = self.toolbar.AddLabelTool(wx.ID_ANY, ' Contextual Help', 
                                    wx.Bitmap(self.icon_help))
        # finally, create it
        self.toolbar.Realize()
        
        #--------------------------Binding (evt)------------------------#
        self.Bind(wx.EVT_TOOL, self.Playpath, play)
        self.Bind(wx.EVT_TOOL, self.Analyze, analyze)
        self.Bind(wx.EVT_TOOL, self.Presets_tool, presets)
        self.Bind(wx.EVT_TOOL, self.Run_Coding, run_coding)
        self.Bind(wx.EVT_TOOL, self.Help_Contest, help_contest)
        
    #---------------------Callback (event handler)----------------------#
    #------------------------------------------------------------------#
    def Playpath(self, event):
        """
        Preview of media imported with open button.
        """
        filepath = self.text_path_open.GetValue()
        play_input(filepath, self.ffplay_link, self.loglevel_type)
        
    #------------------------------------------------------------------#
    def Analyze(self, event):
        """
        Show media information files imported with open button.
        This function call mediainfo.py that is window for metadata
        info multimedia files, witch call ffprobe.py for parsing.
        """
        filepath = self.text_path_open.GetValue()
        
        try:
            with open(filepath):
                frame = mediainfo.Mediainfo(self, filepath, self.helping, 
                                            self.ffprobe_link
                                            )
                frame.Show()
        except IOError:
            wx.MessageBox("File does not exist:  %s" % (filepath), "WARNING",
                        wx.ICON_EXCLAMATION, self)
        
    #------------------------------------------------------------------#
    def Presets_tool(self, event):
        """
        switch for show the presets manager.
        """
        self.Quiet(self)
        
    #------------------------------------------------------------------#
    def Run_Coding(self, event):
        """
        Pass at on_ok function for process
        """
        self.on_ok()
        
    #------------------------------------------------------------------#
    def Help_Contest(self, event):
        """
        Run the predefined browser on contestual help
        """
        webbrowser.open('%s/07-Video_Edit.html' % self.helping)
        
