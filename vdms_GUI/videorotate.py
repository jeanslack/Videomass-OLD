#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Sat Aug  3 09:58:50 2013
#
#########################################################
# Name: videorotate.py
# Porpose: rotate and direction orient each video
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (04) 20/07/2014
# Rev (05) 16/03/2015
#########################################################

import wx
import os
import webbrowser
from os_processing import proc_std
from epilogue import Formula

dirname = os.path.expanduser('~/') # /home/user/

# create need list, dict:
diction_command = {"ext_input":"", "InputDir":"" ,"OutputDir":"", 
                "Pathname":"", "Dirname":"", "Filename":"", 
                "orientation":"", "str_orientation":""
                }

class VideoRotate(wx.Dialog):
    """
    Change point orientation in the file movie imported
    TODO: make rotate button with images 
    """
    def __init__(self, parent, helping, ffmpeg_link, threads, ffmpeg_log, 
                command_log, path_log, loglevel_type):

        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        
        self.helping = helping
        self.ffmpeg_link = ffmpeg_link
        self.threads = threads
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.parent = parent
        self.parent.Hide()
        
        self.button_up = wx.Button(self, wx.ID_ANY, ("Flip over")) # capovolgi Sopra
        self.button_left = wx.Button(self, wx.ID_ANY, ("Rotate Left")) # ruota sx
        self.button_right = wx.Button(self, wx.ID_ANY, ("Rotate Right")) # ruota a destra
        self.button_down = wx.Button(self, wx.ID_ANY, ("Flip below")) # capovolgi sotto
        self.button_reset = wx.Button(self, wx.ID_ANY, ("RESET"))
        self.text_rotate = wx.TextCtrl(self, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                    )
        siz1_staticbox = wx.StaticBox(self, wx.ID_ANY, 
                                                    ("Orientation points")
                                                    )
        btn_open = wx.Button(self, wx.ID_OPEN, "")
        self.text_open = wx.TextCtrl(self, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                    )
        self.btn_save = wx.Button(self, wx.ID_SAVE, "")
        self.text_save = wx.TextCtrl(self, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                    )
        siz2_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Import/Export Media Movie"))
        btn_close = wx.Button(self, wx.ID_CANCEL, "Close")
        btn_help = wx.Button(self, wx.ID_HELP, "")
        self.button_ok = wx.Button(self, wx.ID_OK, "Start")
        
        diction_command["InputDir"] = ""
        diction_command["OutputDir"] = ""
        
        self.btn_save.Disable()


        #----------------------Properties------------------------------------#

        self.SetTitle("Rotate Movies")
        self.button_up.SetBackgroundColour(wx.Colour(122, 239, 255))
        self.button_up.SetToolTipString("Reverses visual movie from bottom to top")
        self.button_left.SetBackgroundColour(wx.Colour(122, 239, 255))
        self.button_left.SetToolTipString("Rotate view movie to left")
        self.button_right.SetBackgroundColour(wx.Colour(122, 239, 255))
        self.button_right.SetToolTipString("Rotate view movie to Right")
        self.button_down.SetBackgroundColour(wx.Colour(122, 239, 255))
        self.button_down.SetToolTipString("Reverses visual movie from top to bottom")
        self.button_reset.SetBackgroundColour(wx.Colour(108, 255, 95))
        self.button_reset.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))
        self.button_reset.SetToolTipString("Torna allo stato iniziale")
        self.text_rotate.SetMinSize((150, 21))
        self.text_rotate.SetToolTipString("Display show settings")

        self.text_open.SetMinSize((230, 21))
        self.text_open.SetToolTipString("Imported path-name (read only)")
        self.text_save.SetMinSize((230, 21))
        self.text_save.SetToolTipString("Exported path-name (read only)")
        self.button_ok.Disable()
        self.button_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
        self.button_ok.SetForegroundColour(wx.Colour(34, 31, 30))


        #----------------------Handle layout---------------------------------#

        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_4 = wx.GridSizer(1, 3, 0, 0)
        siz2_staticbox.Lower()
        sizer_5 = wx.StaticBoxSizer(siz2_staticbox, wx.VERTICAL)
        grid_sizer_3 = wx.FlexGridSizer(2, 2, 0, 0)
        siz1_staticbox.Lower()
        sizer_2 = wx.StaticBoxSizer(siz1_staticbox, wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_2 = wx.GridSizer(1, 2, 0, 0)
        grid_sizer_1 = wx.GridSizer(1, 2, 0, 0)
        sizer_3.Add(self.button_up, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
        grid_sizer_1.Add(self.button_left, 0, wx.ALL 
                                                | wx.ALIGN_CENTER_HORIZONTAL 
                                                | wx.ALIGN_CENTER_VERTICAL, 5
                                                )
        grid_sizer_1.Add(self.button_right, 0, wx.ALL 
                                            | wx.ALIGN_CENTER_HORIZONTAL 
                                            | wx.ALIGN_CENTER_VERTICAL, 5
                                            )
        sizer_3.Add(grid_sizer_1, 1, wx.EXPAND, 0)
        sizer_3.Add(self.button_down, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                                | wx.ALIGN_CENTER_VERTICAL,5
                                                )
        grid_sizer_2.Add(self.button_reset, 0, wx.ALL 
                                            | wx.ALIGN_CENTER_HORIZONTAL 
                                            | wx.ALIGN_CENTER_VERTICAL,5
                                            )
        grid_sizer_2.Add(self.text_rotate, 0, wx.ALL 
                                        | wx.ALIGN_CENTER_HORIZONTAL 
                                        | wx.ALIGN_CENTER_VERTICAL,5
                                        )
        sizer_3.Add(grid_sizer_2, 1, wx.EXPAND, 0)
        sizer_2.Add(sizer_3, 1, wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 1, wx.ALL | wx.EXPAND,15)
        grid_sizer_3.Add(btn_open, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                             | wx.ALIGN_CENTER_VERTICAL,5
                                             )
        grid_sizer_3.Add(self.text_open, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                                   | wx.ALIGN_CENTER_VERTICAL,5
                                                   )
        grid_sizer_3.Add(self.btn_save, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                                  | wx.ALIGN_CENTER_VERTICAL,5
                                                  )
        grid_sizer_3.Add(self.text_save, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                                | wx.ALIGN_CENTER_VERTICAL,5
                                                )
        sizer_5.Add(grid_sizer_3, 1, wx.EXPAND, 0)
        sizer_4.Add(sizer_5, 1, wx.ALL | wx.EXPAND,15)
        grid_sizer_4.Add(btn_close, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                              | wx.ALIGN_CENTER_VERTICAL,5
                                               )
        grid_sizer_4.Add(btn_help, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                             | wx.ALIGN_CENTER_VERTICAL,5
                                              )
        grid_sizer_4.Add(self.button_ok, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                                | wx.ALIGN_CENTER_VERTICAL,5
                                                )
        sizer_4.Add(grid_sizer_4, 1, wx.EXPAND, 0)
        sizer_1.Add(sizer_4, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()

        #----------------------Binding (EVT)---------------------------------#
        self.Bind(wx.EVT_BUTTON, self.on_up, self.button_up)
        self.Bind(wx.EVT_BUTTON, self.on_left, self.button_left)
        self.Bind(wx.EVT_BUTTON, self.on_right, self.button_right)
        self.Bind(wx.EVT_BUTTON, self.on_down, self.button_down)
        self.Bind(wx.EVT_BUTTON, self.on_reset, self.button_reset)
        self.Bind(wx.EVT_BUTTON, self.on_open, btn_open)
        self.Bind(wx.EVT_TEXT, self.enter_open, self.text_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.btn_save)
        self.Bind(wx.EVT_TEXT, self.enter_save, self.text_save)
        self.Bind(wx.EVT_BUTTON, self.on_close, btn_close)
        self.Bind(wx.EVT_BUTTON, self.on_help, btn_help)
        self.Bind(wx.EVT_BUTTON, self.on_ok, self.button_ok)
        

    #----------------------Event handler (callback)--------------------------#
    def on_up(self, event):
        diction_command["orientation"] = "-vf 'vflip, hflip'"
        diction_command["str_orientation"] = u"Reverses view movie 180° bottom/top"
        self.text_rotate.SetValue("180° from bottom to top")
        
    #------------------------------------------------------------------#
    def on_left(self, event):
        rotate = "-vf 'transpose=1,transpose=1,transpose=1'"
        diction_command["orientation"] = rotate
        diction_command["str_orientation"] = u"Rotate 90° view movie to left"
        self.text_rotate.SetValue("Rotate 90° Left")
        
    #------------------------------------------------------------------#
    def on_right(self, event):
        diction_command["orientation"] = "-vf 'transpose=1'"
        diction_command["str_orientation"] = u"Rotate 90° view movie to Right"
        self.text_rotate.SetValue("Rotate 90° Right")
        
    #------------------------------------------------------------------#
    def on_down(self, event):
        diction_command["orientation"] = "-vf 'hflip, vflip'"
        diction_command["str_orientation"] = u"Reverses view movie 180° top/bottom"
        self.text_rotate.SetValue("180° from top to bottom")
        
    #------------------------------------------------------------------#
    def on_reset(self, event):
        diction_command["orientation"] = ""
        diction_command["str_orientation"] = ""
        self.text_rotate.SetValue("")
        
    #------------------------------------------------------------------#
    def on_open(self, event):
        dialfile = wx.FileDialog(self, "Importing Movie", ".", "", "*.*", 
                                 wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
                                   )
        if dialfile.ShowModal() == wx.ID_OK:
            self.text_open.SetValue(""), self.btn_save.Enable() 
            self.text_save.SetValue("")
            self.text_open.AppendText(dialfile.GetPath())
            fileName, fileExtension = os.path.splitext(dialfile.GetPath())
            diction_command["ext_input"] = fileExtension.split(".")[-1]
            dialfile.Destroy()
            
    #------------------------------------------------------------------#
    def enter_open(self, event):
        diction_command["InputDir"] = self.text_open.GetValue()
        
    #------------------------------------------------------------------#
    def on_save(self, event):
        ext = diction_command["ext_input"]
        
        dialsave = wx.FileDialog(self, "Export Final Movie","", "", 
                                 "%s files (*.%s)|*.%s" % (ext,ext,ext), 
                                 wx.SAVE | wx.OVERWRITE_PROMPT | wx.FD_CHANGE_DIR
                                    )
        if dialsave.ShowModal() == wx.ID_OK:
            path = dialsave.GetPath()
            
            if path.endswith(ext) != True:
                path = "%s.%s" % (path, ext)
            self.text_save.SetValue("")
            self.text_save.AppendText(path)
            dialsave.Destroy()
            
    #------------------------------------------------------------------#
    def enter_save(self, event):
        diction_command["OutputDir"] = self.text_save.GetValue()
        
        if self.text_save.GetValue() == '':
            self.button_ok.Disable()
            self.button_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
            self.button_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        else:
            self.button_ok.Enable()
            self.button_ok.SetBackgroundColour(wx.Colour(180, 214, 255))
            self.button_ok.SetForegroundColour(wx.Colour(130, 51, 27))
            
    #------------------------------------------------------------------#
    def on_close(self, event):
        #self.Destroy()
        self.parent.Show()
        event.Skip()

    #------------------------------------------------------------------#
    def on_help(self, event):
        #wx.MessageBox("L'help contestuale é ancora in fase di sviluppo .")
        webbrowser.open('%s/15-Ruotare_video.html' % self.helping)
        
    #------------------------------------------------------------------#
    def on_ok(self, event):
        logname = 'Videomass_VideoRotate.log'
        
        if self.ffmpeg_log == 'true':
            report = '-report'
        else:
            report = ''
             
        if self.text_rotate.GetValue() == "": # text_rotate vuoto
            wx.MessageBox("You have not set any rotation point!", "Warning", 
                             wx.OK | wx.ICON_EXCLAMATION, self)
        else:
            command = ("%s %s -i '%s' -loglevel %s %s -metadata:s:v rotate=0 "
                       "-codec:a copy %s -y '%s'" % (
                       self.ffmpeg_link, report, self.text_open.GetValue(), 
                       self.loglevel_type, diction_command["orientation"], 
                       self.threads, self.text_save.GetValue())
                       )
            formula = (u"FORMULATIONS:\n\nConversion Mode:\
                       \nVideo:\nAudio:\nOrientation point:\
                       \nInput dir/file:\nOutput dir/file:"
                       )
            dictions = (u"\n\n%s\n%s\n%s\n%s\n%s\n%s" % ("Single file only", 
                         'Re-coding original format (%s)' % (
                         diction_command["ext_input"]),
                         'Source Audio Copy',
                         diction_command["str_orientation"],
                         diction_command["InputDir"],
                         diction_command["OutputDir"])
                        )
            ending = Formula(self,formula,dictions)
            
            if ending.ShowModal() == wx.ID_OK:
                dial = proc_std(self, 'Videomass - Run single process', command,
                                self.path_log, logname, self.command_log,
                                )
                
