#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Friday Aug 23 10:37:47 2013
#
#
#########################################################
# Name: presets_manager.py
# Porpose: top window presets manager and presets processing
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2013 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (08) 04/08/2014
# Rev (09) 01/18/2015
# Rev (10) 07/03/2015
# Rev (10) 07/05/2015
#########################################################

"""
This is the main frame window for storing and presets operation.
In the current version of the program, this become top window on the run.
"""

import wx
import string
import os
import webbrowser
from vdms_utils.ctrl_run import parser_xml, delete_profiles
from vdms_utils.os_interaction import copy_restore, copy_backup, copy_on
from os_processing import  proc_std, proc_batch_thread, ProgressDialog,\
                           control_errors, play_input
import audiograbber, audiograbseq, audioadd, videocut, videorotate,\
       videograbframe, videosilent, mediainfo, video_sci, settings,\
       normalize, infoprg, presets_addnew


# create need list and dictionaries:
array = []
batch = []
outformat = []
diction_command = {"testing":"","ext_input":""}
# this dictionarie is a list of the presets in ~/.videomass:
dict_presets = {
"Audio Conversions" : ("preset-v1-Audio", "Audio Conversions"), 
"Extract audio from video": ("preset-v1-VideoAudio", "Extract audio from video"),
"Convert to AVI" : ("preset-v1-AVI", "Convert to AVI"),
"Mobile Phones multimedia" : ("preset-v1-MobilePhones", "Mobile Phones multimedia"),
"iPod iTunes" : ("preset-v1-iPod-iTunes", "iPod iTunes"),
"Convert to VCD (mpg)" : ("preset-v1-VCD", "Convert to VCD (mpg)"),
"Convert DVD VOB" : ("preset-v1-VOB", "Convert DVD VOB"),
"Convert to quicktime (mov)" : ("preset-v1-quicktime", "Convert to quicktime (mov)"),
"Convert to DV" : ("preset-v1-DV", "Convert to DV"),
"Google Android" : ("preset-v1-GoogleAndroid", "Google Android"),
"Google webm" : ("preset-v1-GoogleWebm", "Google webm"),
"DVD Conversions" : ("preset-v1-DVD", "DVD Conversions"),
"MPEG-4 Conversions" : ("preset-v1-MPEG-4", "MPEG-4 Conversions"),
"PS3 Compatible" : ("preset-v1-PS3", "PS3 Compatible"),
"PSP Compatible" : ("preset-v1-PSP", "PSP Compatible"),
"Websites" : ("preset-v1-websites", "Websites"),
"Your personal profiles" : ("preset-v1-Personal", "Your personal profiles"),
                    }
# set widget colours in some case:
azure = '#d9ffff' # rgb form (wx.Colour(217,255,255))
orange = '#ff5f1a' # rgb form (wx.Colour(255,95,26))
yellow = '#faff35'
red = '#ff3a1f'

class MainFrame(wx.Frame):
    """
    This is the top window main frame for memorisation and presets operate. 
    In the current version of the program, this become top window on the run.
    """

    def __init__(self,videomass_icon,icon_play,icon_analyze,
                 icon_presets,icon_switchvideomass,icon_process,
                 icon_help,helping,OS,path_srcShare,path_confdir,PWD,
                 threads,ffmpeg_log,command_log,path_log,loglevel_type,
                 loglevel_batch,ffmpeg_link,ffprobe_link,ffplay_link,
                 ffmpeg_check,ffprobe_check,ffplay_check,full_list, 
                 writeline_exec):
        
        self.videomass_icon = videomass_icon
        self.icon_play = icon_play
        self.icon_analyze = icon_analyze
        self.icon_presets = icon_presets
        self.icon_switchvideomass = icon_switchvideomass
        self.icon_process = icon_process
        self.icon_help = icon_help
        self.helping = helping
        self.OS = OS
        self.path_srcShare = path_srcShare
        self.path_confdir = path_confdir
        self.PWD = PWD
        self.threads = threads
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.loglevel_batch = loglevel_batch
        self.ffmpeg_link = ffmpeg_link
        self.ffprobe_link = ffprobe_link
        self.ffplay_link = ffplay_link
        self.ffmpeg_check = ffmpeg_check
        self.ffprobe_check = ffprobe_check
        self.ffplay_check = ffplay_check
        self.full_list = full_list
        self.writeline_exec = writeline_exec
        
        wx.Frame.__init__(self, None, -1, style=wx.DEFAULT_FRAME_STYLE)
        
        self.panel_1 = wx.Panel(self, wx.ID_ANY)
        self.videomass_menu_bar()
        self.videomass_tool_bar()
        self.sb = self.CreateStatusBar(1)
        self.list_ctrl = wx.ListCtrl(self.panel_1, wx.ID_ANY, 
                                        style=wx.LC_REPORT | wx.SUNKEN_BORDER
                                        )
        nb1 = wx.Notebook(self.panel_1, wx.ID_ANY, style=0)
        
        nb1_p1 = wx.Panel(nb1, wx.ID_ANY)
        lab_prfl = wx.StaticText(nb1_p1, wx.ID_ANY, "Select a preset from "
                                                    "the drop down:")
        self.cmbx_prst = wx.ComboBox(nb1_p1,wx.ID_ANY, choices=[
                        (dict_presets["Audio Conversions"][1]),
                        (dict_presets["Extract audio from video"][1]),
                        (dict_presets["Convert to AVI"][1]),
                        (dict_presets["Mobile Phones multimedia"][1]),
                        (dict_presets["iPod iTunes"][1]),
                        (dict_presets["Convert to VCD (mpg)"][1]),
                        (dict_presets["Convert DVD VOB"][1]),
                        (dict_presets["Convert to quicktime (mov)"][1]),
                        (dict_presets["Convert to DV"][1]),
                        (dict_presets["Google Android"][1]),
                        (dict_presets["Google webm"][1]),
                        (dict_presets["DVD Conversions"][1]),
                        (dict_presets["MPEG-4 Conversions"][1]),
                        (dict_presets["PS3 Compatible"][1]),
                        (dict_presets["PSP Compatible"][1]),
                        (dict_presets["Websites"][1]),
                        (dict_presets["Your personal profiles"][1]),
                        ],
                        style=wx.CB_DROPDOWN | wx.CB_READONLY
                        )
        nb1_p2 = wx.Panel(nb1, wx.ID_ANY)
        
        if self.writeline_exec == 'true':
            self.txt_cmd = wx.TextCtrl(nb1_p2, wx.ID_ANY,"",
                                       style=wx.TE_MULTILINE | wx.TE_PROCESS_ENTER)
        else:
            self.txt_cmd = wx.TextCtrl(nb1_p2, wx.ID_ANY,"",
                                       style=wx.TE_MULTILINE | wx.TE_READONLY)
           
        nb1_p3 = wx.Panel(nb1, wx.ID_ANY)
        self.ckbox_batch = wx.CheckBox(nb1_p3, wx.ID_ANY, ("Batch-Mode"))
        self.lab_grp = wx.StaticText(nb1_p3, wx.ID_ANY, "Group : ")
        self.text_group_ext = wx.TextCtrl(nb1_p3, wx.ID_ANY, "", 
                                                  style=wx.TE_PROCESS_ENTER
                                           )
        static_line_1 = wx.StaticLine(nb1_p3, wx.ID_ANY, style=wx.LI_VERTICAL)
        self.ckbox_test = wx.CheckBox(nb1_p3, wx.ID_ANY, ("Testing"))
        self.lab_sec = wx.StaticText(nb1_p3, wx.ID_ANY, "Time Sec : ")
        self.spin_ctrl_test = wx.SpinCtrl(nb1_p3, wx.ID_ANY, "", min=10,
                             max=10000, style=wx.TE_PROCESS_ENTER
                                          )
        nb1_p4 = wx.Panel(nb1, wx.ID_ANY)
        self.btn_open = wx.Button(nb1_p4, wx.ID_OPEN, "")
        self.text_path_open = wx.TextCtrl(nb1_p4, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                                    )
        self.btn_save = wx.Button(nb1_p4, wx.ID_SAVE, "")
        self.text_path_save = wx.TextCtrl(nb1_p4, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                                    )
        # set layout of the widget in the window
        self.spin_ctrl_test.Disable()
        self.spin_ctrl_test.SetValue(10)
        self.lab_sec.Disable()
        
        #----------------------Set Properties----------------------#
        self.SetTitle("Videomass - Presets Manager")
        icon = wx.EmptyIcon()
        icon.CopyFromBitmap(wx.Bitmap(self.videomass_icon, wx.BITMAP_TYPE_ANY))
        self.SetIcon(icon)
        self.SetSize((710, 450))
        #self.Centre()
        self.CentreOnScreen()
        self.cmbx_prst.SetToolTipString("You can choice a preset. "
                                              "Each preset is defined by type")
        self.cmbx_prst.SetSelection(0)
        
        #self.list_ctrl.SetBackgroundColour(azure)
        self.list_ctrl.SetToolTipString("Select a profile to use")
        self.txt_cmd.SetMinSize((430, 60))
        self.txt_cmd.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))
        self.txt_cmd.SetToolTipString("Command output of selected profiles. If "
                                  "enable, you can also change it from here "
                                  "without changing the stored profiles."
                                  )
        self.ckbox_batch.SetToolTipString("Apply the process to a files "
                                             "group with same extensions"
                                            )
        self.text_group_ext.SetMinSize((50, 20))
        self.text_group_ext.SetToolTipString("Write here a files group "
                            "extension to convert with batch mode. Do not "
                            "include punctuation marks '.' and blanks"
                                            )
        self.ckbox_test.SetToolTipString("Run test conversion by setting "
                                            "the time in seconds"
                                            )
        self.spin_ctrl_test.SetMinSize((70, 21))
        self.spin_ctrl_test.SetToolTipString("Enter time in seconds")
        self.btn_open.SetToolTipString("Import Media (files/directory)")
        self.text_path_open.SetMinSize((320, 21))
        self.text_path_open.SetToolTipString("Imported path-name")
        self.btn_save.SetToolTipString("Export Media (files/directory)")
        self.text_path_save.SetMinSize((320, 21))
        self.text_path_save.SetToolTipString("Exported path-name")

        self.text_group_ext.Disable(), self.lab_grp.Disable()

        #----------------------Build Layout----------------------#
        #siz1 = wx.BoxSizer(wx.VERTICAL)
        siz1 = wx.FlexGridSizer(1, 1, 0, 0)
        grid_siz7 = wx.GridSizer(2, 1, 0, 0)
        grd_s1 = wx.FlexGridSizer(2, 1, 0, 0)
        grd_s2 = wx.FlexGridSizer(3, 1, 0, 0)
        grd_s4 = wx.GridSizer(1, 3, 0, 0)
        grid_siz5 = wx.FlexGridSizer(2, 2, 0, 0)
        grid_siz6 = wx.FlexGridSizer(1, 7, 0, 0)
        grd_s3 = wx.GridSizer(1, 1, 0, 0)
        grd_s1.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND, 15)
        grd_s3.Add(self.txt_cmd, 0, wx.ALL | wx.EXPAND 
                                            | wx.ALIGN_CENTER_HORIZONTAL 
                                            | wx.ALIGN_CENTER_VERTICAL, 15
                                            )
        grid_siz7.Add(lab_prfl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 0)
        grid_siz7.Add(self.cmbx_prst, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        nb1_p1.SetSizer(grid_siz7)
        nb1_p2.SetSizer(grd_s3)
        grid_siz6.Add(self.ckbox_batch, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_siz6.Add(self.lab_grp, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        grid_siz6.Add(self.text_group_ext, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_siz6.Add(static_line_1, 0, wx.ALL | wx.EXPAND, 15)
        grid_siz6.Add(self.ckbox_test, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_siz6.Add(self.lab_sec, 0, wx.ALIGN_CENTER_VERTICAL, 15)
        #grid_siz6.Add(self.text_secondi, 0, wx.ALIGN_CENTER_VERTICAL, 15)
        grid_siz6.Add(self.spin_ctrl_test, 0, wx.ALIGN_CENTER_VERTICAL, 15)
        nb1_p3.SetSizer(grid_siz6)
        grid_siz5.Add(self.btn_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_siz5.Add(self.text_path_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_siz5.Add(self.btn_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_siz5.Add(self.text_path_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        nb1_p4.SetSizer(grid_siz5)
        nb1.AddPage(nb1_p1, ("Selecting Presets"))
        nb1.AddPage(nb1_p2, ("Command Line Reading"))
        nb1.AddPage(nb1_p3, ("Options and Features"))
        nb1.AddPage(nb1_p4, ("Importing/Exporting"))
        grd_s2.Add(nb1, 1, wx.EXPAND, 0)
        grd_s2.Add(grd_s4, 1, wx.EXPAND, 0)
        grd_s1.Add(grd_s2, 1, wx.ALL | wx.EXPAND, 15)
        self.panel_1.SetSizer(grd_s1)
        siz1.Add(self.panel_1, 1, wx.EXPAND, 0)
        self.SetSizer(siz1)
        siz1.AddGrowableRow(0)
        siz1.AddGrowableCol(0)
        grd_s1.AddGrowableRow(0)
        grd_s1.AddGrowableCol(0)
        grd_s2.AddGrowableRow(0)
        grd_s2.AddGrowableCol(0)
        self.Layout()
        
        #----------------------Binder (EVT)----------------------#
        self.Bind(wx.EVT_COMBOBOX, self.on_choice_profiles, self.cmbx_prst)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_select, self.list_ctrl)
        self.Bind(wx.EVT_TEXT, self.enter_command, self.txt_cmd)
        self.Bind(wx.EVT_CHECKBOX, self.on_batch, self.ckbox_batch)
        self.Bind(wx.EVT_TEXT, self.enter_group_ext, self.text_group_ext)
        self.Bind(wx.EVT_CHECKBOX, self.on_test, self.ckbox_test)
        self.Bind(wx.EVT_SPINCTRL, self.enter_secondi, self.spin_ctrl_test)
        self.Bind(wx.EVT_BUTTON, self.on_open, self.btn_open)
        self.Bind(wx.EVT_TEXT, self.enter_open, self.text_path_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.btn_save)
        self.Bind(wx.EVT_TEXT, self.enter_save, self.text_path_save)
        self.Bind(wx.EVT_CLOSE, self.on_close) # controlla la chiusura
        
        #----------------------Create preset list----------------------#
        if array != []: # appena avvio, cancella i dati in memoria se esistono
            del array[0:5]
        
        self.disable_button() # create default layout
        self.set_listctrl() # passo l'istanza al parsing e creo la lista
        
    def enable_button(self):
        self.toolbar.EnableTool(wx.ID_OK, True)

    def disable_button(self):
        self.toolbar.EnableTool(wx.ID_OK, False)
        self.text_path_open.SetValue(""),self.text_path_save.SetValue("")
        self.btn_save.Disable(), self.text_path_save.Disable()
        
    def reset_list(self):
        """
        Clear all memory presets and pass to set_listctrl() for 
        re-charging new. 
        This is used when add/edit or delete profiles. The list is 
        reloaded automatically after closing the dialogs for view
        update .
        """
        self.list_ctrl.ClearAll()
        self.txt_cmd.SetValue("")
        self.set_listctrl()
        if array != []:
            del array[0:5]
            
    def set_listctrl(self):
        """
        Make a order list of preset (Name/Descript.) from reads xml (vdms)
        """
        try:
            self.statusbar_msg('Preset Name:  %s' % ( 
                           dict_presets[self.cmbx_prst.GetValue()][1]),None
            )
            av_presets = dict_presets[self.cmbx_prst.GetValue()][0]
            dati = parser_xml(av_presets) # function for parsing
            
            self.list_ctrl.InsertColumn(0, 'Profile Name', width=230)
            self.list_ctrl.InsertColumn(1, 'Description', width=350)
            self.list_ctrl.InsertColumn(2, 'Output Extension Type', width=230)
            self.list_ctrl.InsertColumn(3, 'File format import', width=350)
            
            index = 0
            for name in sorted(dati.keys()):
                rows = self.list_ctrl.InsertStringItem(index, name)
                self.list_ctrl.SetStringItem(rows, 0, name)
                param = dati[name]
                self.list_ctrl.SetStringItem(rows, 1, param["type"])
                self.list_ctrl.SetStringItem(rows, 2, param["estensione"])
                self.list_ctrl.SetStringItem(rows, 3, param["filesupport"])
        except:
            UnboundLocalError
            wx.MessageBox("For some circumstance, the selected preset is "
                          "corrupted. To restore the original copy at least go "
                          "to the File menu and choose 'restore the preset "
                          "source in use'. Note, before you make this choice, "
                          "saved your personal settings.", "ERROR !", 
                          wx.ICON_ERROR, self)
            return
        
    def statusbar_msg(self, msg, color):
        """
        set the status-bar with messages and color types
        """
        if color == None:
            self.sb.SetBackgroundColour(wx.NullColour)
        else:
            self.sb.SetBackgroundColour(color)
        self.sb.SetStatusText(msg)
        self.sb.Refresh()
        
    #----------------------Event handler (callback)----------------------#
    """
    event.Skip() are not used in callbacks, otherwise each event 
    occurs twice because the window is a frame in this case
    """
    #------------------------------------------------------------------#
    def on_choice_profiles(self, event): # combobox
        """
        Whenever change preset type in the combobox list, clear all
        in the list_ctrl and re-charge with new file xml, clear the 
        text command and delete elements of array[] if in list.
        """
        self.reset_list()
        
    #------------------------------------------------------------------#
    def on_select(self, event): # list_ctrl
        """
        When select a profile into the list_ctrl, set the parameters
        request.
        Is the main point where be filled the list_ctrl for view.
        The presets are the files with extension vdms. The profiles
        are content it on presets and are selected in list_ctrl
        
        """
        combvalue = dict_presets[self.cmbx_prst.GetValue()][0] # name xml
        dati = parser_xml(combvalue) # call module. All data go in dict
        if array != []:
            del array[0:5] # delete all: lista [0],[1],[2],[3],[4]
            
        slct = event.GetText() # event.GetText is a Name Profile
        self.txt_cmd.SetValue("")
        # param, pass a name profile and search all own elements in list.
        param = dati[event.GetText()] 
        # lista extract and construct command from param and description (type)
        # slct is the profile name
        lista =  [slct, param["type"],param["parametri"], param["filesupport"], 
            param["estensione"]]
        """
        # lista[0] is the format name, lista[1] is the description,
        # lista[2] is the final command, lista[3] is the filesupport,
        # lista[4] is the profile name. Append in array that is a GLOBAL LIST
        """
        array.append(lista[0]), array.append(lista[1]), 
        array.append(lista[2]), array.append(lista[3]),
        array.append(lista[4])
        self.txt_cmd.AppendText(lista[2]) # this is cmd show in text ctrl
        outformat.append(lista[4]) # this is extension show in filedialog save

        if 'batch' not in batch:
            File = self.text_path_save.GetValue().strip(".%s" % outformat)
            self.text_path_save.SetValue("%s.%s" % (File, array[4]))
            
        self.statusbar_msg('Profile Name: %s' % (lista[0]),None)

    #------------------------------------------------------------------#
    def enter_command(self, event): # text command view
        """
        If a profile is selected, append text into the text_ctrl. Also
        modified the text received signal event
        """
        self.txt_cmd.GetValue()
        
    #------------------------------------------------------------------#
    def on_batch(self, event): # checkbox
        """
        enable or disable functionality the std conv or batch conv
        """
        try:
            if array[3]:
                pass
        except:
                IndexError
                self.statusbar_msg("Before you check 'Batch-Mode' select an "
                                   "item from the control list", red)
                
                if self.ckbox_batch.GetValue() is True:
                    self.ckbox_batch.SetValue(False)
                else:
                    self.ckbox_batch.SetValue(True)
                return
            
        msg = ("Batch-Mode is Turn-On: Remember to type in "
               "'Group' field which extension has files format to be treated")
        
        if self.ckbox_batch.GetValue() is True:
            self.btn_open.SetBackgroundColour(yellow)
            self.btn_save.SetBackgroundColour(yellow)
            self.text_group_ext.Enable(), self.lab_grp.Enable()
            self.disable_button()
            batch.append('batch')
            self.statusbar_msg(msg, yellow)
            
        elif self.ckbox_batch.GetValue() is False:
            diction_command["ext_input"] = ""
            self.text_group_ext.Disable(), self.text_group_ext.SetValue("")
            self.lab_grp.Disable(), self.disable_button()
            self.btn_open.SetBackgroundColour(wx.NullColour)
            self.btn_save.SetBackgroundColour(wx.NullColour)
            self.text_path_open.SetValue(""),self.text_path_save.SetValue("")
            if 'batch' in batch:
                batch.remove('batch')
            self.statusbar_msg('Batch-Mode: OFF',None)
            
    #------------------------------------------------------------------#
    def enter_group_ext(self, event): # text
        """
        put file extension type as value of dict for batch mode
        """
        diction_command["ext_input"] = "%s" %(self.text_group_ext.GetValue())
        
        self.statusbar_msg('Batch-Mode ON, Group: %s' % (
                                       self.text_group_ext.GetValue()), None)
        
    #------------------------------------------------------------------#
    def on_test(self, event): # checkbox
        """
        enable or disable preview of the result putting flag options of 
        ffmpeg to relative value dict or a empty value
        """
        if self.ckbox_test.GetValue() is True:
            self.spin_ctrl_test.Enable(), self.lab_sec.Enable()
            diction_command["testing"] = "-t %s" % (
                                            self.spin_ctrl_test.GetValue()
                                                    )
            self.statusbar_msg('Testing: %s sec.' % (
                                            self.spin_ctrl_test.GetValue()),None
                                                        )
        elif self.ckbox_test.GetValue() is False:
            self.spin_ctrl_test.Disable(), self.spin_ctrl_test.SetValue(10)
            self.lab_sec.Disable()
            diction_command["testing"] = ""
            self.statusbar_msg('Testing: OFF', None)
            
    #------------------------------------------------------------------#
    def enter_secondi(self, event): # spinctrl
        """
        the string come remake when change time. This is disable
        if checkbox is false
        """
        diction_command["testing"] = "-t %s" % (self.spin_ctrl_test.GetValue())
        self.statusbar_msg('Testing: %s sec.' % (
                                            self.spin_ctrl_test.GetValue()),None
                                                     )
        
    #------------------------------------------------------------------#
    def on_open(self, event): # choice import file/dir dialog
        """
        If batch is true, set to batch mode, if batch is...
        """
        try:
            if array[3]:
                pass
        except:
                IndexError
                self.statusbar_msg("Before importing files or directories "
                                   "select an item from the control list",red)
                return
            
        if array[3] in string.whitespace:
            wildcard = "*.*"
            
        else:
            new_li = []
            li = string.split(array[3])

            for a in li:
                new_li.append("*.%s;" % (a))

            part1 = string.join(new_li) # la lista diventa una stringa.
            
            # rimuove tutti gli spazi bianchi anche multipli in tutta la 
            # lunghezza della stringa:
            part2 = "".join(part1.split()) 
            sum_all = "Source (%s)|%s|" % (part1, part2), "All files (*.*)|*.*"
            lista = list(sum_all)
            wildcard = string.join(lista)

        dialfile = wx.FileDialog(self, "File Import", "", "", 
                        wildcard, wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        
        dialdir = wx.DirDialog(self, "Select the directory for the batch process")
        
        if self.ckbox_batch.GetValue() is False:
            if dialfile.ShowModal() == wx.ID_OK:
                self.text_path_open.SetValue("")
                self.text_path_open.AppendText(dialfile.GetPath())
                self.btn_save.Enable(), self.text_path_save.Enable()
                dialfile.Destroy()
        else:
            if dialdir.ShowModal() == wx.ID_OK:
                    self.text_path_open.SetValue("")
                    self.text_path_open.AppendText(dialdir.GetPath())
                    self.btn_save.Enable(), self.text_path_save.Enable()
                    dialdir.Destroy()
                    
    #------------------------------------------------------------------#
    def enter_open(self, event): # text
        """
        This control enable or disable the info-analyze and
        play button's in tools bar.
        """
        if self.text_path_open.GetValue() == '':
            
            self.toolbar.EnableTool(wx.ID_FILE3, False) # play (disable)
            self.toolbar.EnableTool(wx.ID_FILE4, False) # analyze (disable)
            self.file_save.Enable(False) # filesave in menubar (disable)
            
        else:
            self.file_save.Enable(True)
            
            if self.ckbox_batch.GetValue() is False:
                self.toolbar.EnableTool(wx.ID_FILE3, True) # play (enable)
                self.toolbar.EnableTool(wx.ID_FILE4, True) # analyze (enable)
                
        self.statusbar_msg("Input Files or Directory:  %s" % (
                                         self.text_path_open.GetValue()),None)
                
    #------------------------------------------------------------------#
    def on_save(self, event): # choice export file/dir dialog
        """
        Also here set dialog in accord if batch-mode or standard mode 
        for export.
        NOTA: array[4] é l'estensione senza il punto
        """
        if self.ckbox_batch.GetValue() is False:
            try:
                dialsave = wx.FileDialog(
                           self, "Filename and location for save", "", "",
                           "%s files (*.%s)|*.%s" % (
                            array[4],array[4],array[4]), wx.SAVE |
                            wx.OVERWRITE_PROMPT | wx.FD_CHANGE_DIR)
                           
                if dialsave.ShowModal() == wx.ID_OK:
                    path = dialsave.GetPath()
                    
                    if path.endswith(array[4]) != True:
                        path = "%s.%s" % (path, array[4])
                    self.text_path_save.SetValue("")
                    self.text_path_save.AppendText(path)
                    self.enable_button()
                    dialsave.Destroy()
            except:
                IndexError
                self.statusbar_msg("Before exporting files or directories "
                                   "select an item from the control list",red)
        else:
            dialdir = wx.DirDialog(self, "Where do you want to save the new files?")
            if dialdir.ShowModal() == wx.ID_OK:
                    self.text_path_save.SetValue("")
                    self.text_path_save.AppendText(dialdir.GetPath())
                    self.enable_button()
                    dialdir.Destroy()
                    
    #------------------------------------------------------------------#
    def enter_save(self, event): # text
        """
        This is a text widget output path
        """
        path = self.text_path_save.GetValue()
        self.statusbar_msg("Output file:  %s" % (path),None)
        
    #------------------------------------------------------------------#
    def on_close(self, event):
        """
        destroy the videomass.
        """
        self.Destroy()
        
    #------------------------------------------------------------------#
    def on_ok(self, event):
        """
        Before to process, here is evaluated the condictions related
        to the video file. For audio file preset are not need .
        
        """
        # comcheck, cut string in spaces and become list:
        comcheck = self.txt_cmd.GetValue().split()
        
        logname = 'Videomass_PresetsManager.log'
        
        ######## ------------ VALIDAZIONI: --------------
        # define ext type:
        if array == []:
            wx.MessageBox("Please, first select an item from the control list", 
                         "Profile Selection Warning", wx.OK | 
                         wx.ICON_EXCLAMATION, self
                         )
            self.statusbar_msg("Before start encoding, select "
                               "an item from the control list",red)
            return
        
        if array[3] == " ":
            supported = "All files"
        else: 
            supported = array[3] # é una stringa

        if 'batch' in batch:
            if self.text_group_ext.GetValue() == "":
                wx.MessageBox("No group extension "
                     "specified for the batch process.\nNote that the "
                     "selected profile only support these extensions "
                     "importing:\n\n%s\n\nPlease, see in the 'Options and "
                     "Features' tab and in the\n'File Format Import' of the " 
                     "control list" % (supported),
                     'Missing Extension Format', 
                     wx.OK |  wx.ICON_ERROR, self
                     )
                return
        
            if supported != "All files":
                if self.text_group_ext.GetValue() not in supported:
                    wx.MessageBox("The extension group in the "
                          "'Options and Features' tab is not "
                          "correctly. Note that the selected profile only "
                          "support these extensions importing:\n\n%s\n\n"
                          "You wrote the wrong extension: %s\n\n"
                          "Please, re-type the extension group. See in the " 
                          "'File Format Import' of the control list." % (array[3], 
                          self.text_group_ext.GetValue()), 
                          "Wrong Extension Format", wx.OK | 
                          wx.ICON_EXCLAMATION, self
                          )
                    return
                    
            ######## ------------FINE VALIDAZIONI: --------------
            if 'DOUBLE_PASS' in comcheck:
                
                split = self.txt_cmd.GetValue().split('DOUBLE_PASS')
                pass1 = split[0].strip()
                pass2 = split[1].strip()
                
                command1 = ("-loglevel %s %s %s %s -f rawvideo -y "
                            "/dev/null &&" % (
                            self.loglevel_batch, pass1, self.threads, 
                            diction_command["testing"])
                            )
                command2 = ("-loglevel %s %s %s %s -y" % (self.loglevel_batch, 
                            pass2, self.threads, diction_command["testing"])
                            )
                # this is for log if enable:
                command = (
                    "%s -i '%s/*.%s' -loglevel %s %s %s -i '%s/*.%s' "
                    "-loglevel %s %s -y '%s/*.%s'" % (self.ffmpeg_link,
                    self.text_path_open.GetValue(),diction_command["ext_input"],
                    self.loglevel_batch, command1, self.ffmpeg_link, 
                    self.text_path_open.GetValue(),diction_command["ext_input"],
                    self.loglevel_batch, command2, 
                    self.text_path_save.GetValue(), array[4])
                            )
                status = control_errors(self.text_path_open.GetValue(),
                                            diction_command["ext_input"],
                                            command, logname
                                        )
                if  status[0] == 'no_error':
                
                    proc_batch_thread(status[2], self.text_path_open.GetValue(),
                                    array[4], self.text_path_save.GetValue(),
                                    None, command1, command2, logname,
                                    self.ffmpeg_link
                                       )
                    dial = ProgressDialog(self, 'Videomass Batch Process', 
                                            status[1],  
                                            diction_command["ext_input"],
                                            array[4],
                                            self.text_path_save.GetValue(),
                                            self.path_log, self.command_log,
                                            logname
                                            )
            else:
                command = ("-loglevel %s %s %s %s -y" % (
                            self.loglevel_batch, self.txt_cmd.GetValue(),
                            self.threads, diction_command["testing"])
                            )
                # this is for log:
                log_command = ("%s -i '%s/*.%s' %s '%s/*.%s'" % (
                            self.ffmpeg_link, self.text_path_open.GetValue(),
                            diction_command["ext_input"], command, 
                            self.text_path_save.GetValue(), array[4])
                               )
                status = control_errors(self.text_path_open.GetValue(),
                                            diction_command["ext_input"],
                                            log_command, logname
                                        )
                if  status[0] == 'no_error':

                    proc_batch_thread(status[2], self.text_path_open.GetValue(), 
                    array[4], self.text_path_save.GetValue(),command, None, None,
                                                logname, self.ffmpeg_link
                                        )
                    dial = ProgressDialog(self, 'Videomass Batch Process',
                                            status[1],  
                                            diction_command["ext_input"],
                                            array[4],
                                            self.text_path_save.GetValue(),
                                            self.path_log, self.command_log,
                                            logname
                                            )
        elif 'batch' not in batch:
            
            if 'DOUBLE_PASS' in comcheck:
                
                split = self.txt_cmd.GetValue().split('DOUBLE_PASS')
                pass1 = split[0].strip()
                pass2 = split[1].strip()
                
                command = ("%s -i '%s' -loglevel %s %s %s %s -f rawvideo "
                        "-y /dev/null && %s -i '%s' -loglevel %s %s "
                        "%s %s -y '%s'" % (
                            self.ffmpeg_link, self.text_path_open.GetValue(), 
                            self.loglevel_type, pass1, self.threads, 
                            diction_command["testing"], self.ffmpeg_link, 
                            self.text_path_open.GetValue(), self.loglevel_type,
                            pass2, self.threads, diction_command["testing"], 
                            self.text_path_save.GetValue())
                           )
                dial = proc_std(self, 'Videomass - Run single process', command, 
                                self.path_log, logname, self.command_log
                                )
            else:
                command = ("%s -i '%s' -loglevel %s %s %s %s -y '%s'" % (
                            self.ffmpeg_link, self.text_path_open.GetValue(),
                            self.loglevel_type, self.txt_cmd.GetValue(),
                            self.threads, diction_command["testing"],
                            self.text_path_save.GetValue())
                            )
                dial = proc_std(self, 'Videomass - Run single process', command, 
                                self.path_log, logname, self.command_log
                                )
                
    #---------------------- MENU BAR ----------------------#
    def videomass_menu_bar(self):
        """
        Make a menu bar 
        """
        menuBar = wx.MenuBar()
        
        ####----------------------- file
        fileButton = wx.Menu()
        
        file_open = fileButton.Append(wx.ID_OPEN, "Open.. ", 
                        "Import files/directory")
        self.file_save = fileButton.Append(wx.ID_SAVE, "Save.. ", 
                        "Export files/directory")
        self.file_save.Enable(False)
        
        #fileButton.AppendSeparator()
        #save_all = fileButton.Append(wx.ID_REVERT_TO_SAVED, "Salva tutti i  "
                                            #"presets in una cartella scelta ",
                                            #"Back-Up")
        fileButton.AppendSeparator()
        saveme = fileButton.Append(wx.ID_REVERT_TO_SAVED, "Save the current "
                                             "preset in a separated file",
                 "If you want make a back-Up of the presets, you choose this."
                                    )
        restore = fileButton.Append(wx.ID_REPLACE, "Import a previously "
                                                "saved preset", 
                 "If you want restore your saved presets, you choose this."
                                                )
        fileButton.AppendSeparator()
        default = fileButton.Append(wx.ID_ANY, "Reset the preset source "
                                                "in use",
                         "This back to default and Revert one only preset"
                                                )
        
        default_all = fileButton.Append(wx.ID_UNDO, "Reset all presets ",
                         "Revert to Default all original presets"
                                                )
        
        fileButton.AppendSeparator()
        refresh = fileButton.Append(wx.ID_REFRESH, "Reload presets list", "All Refresh")
        fileButton.AppendSeparator()
        exitItem = fileButton.Append(wx.ID_EXIT, "Exit", "Close the program")
        menuBar.Append(fileButton,"File")
        
        ####------------------- toolsButton
        toolsButton = wx.Menu()
        
        addprof = toolsButton.Append(wx.ID_ADD, "Create new profiles on the "
                                                "selected preset", 
                                        "Create new customized profiles")
        toolsButton.AppendSeparator()
        
        delprof = toolsButton.Append(wx.ID_REMOVE, "Delete the selected profile",
                                          "This Remove from the current list") #
                                                
        editprof = toolsButton.Append(wx.ID_EDIT, "Modify the selected profile",
                                      "Customize existing profiles")
        
        menuBar.Append(toolsButton,"Tools")
        
        #------------------ Plugins
        Plugins = wx.Menu()
        
        normalize = Plugins.Append(wx.ID_ANY, "Normalyze audio files",
                                        "Amplitude changes")
        
        Plugins.AppendSeparator()
        
        audiograbber = Plugins.Append(wx.ID_ANY, "Grabbing audio from video ",
                                            "Extract audio track from video")
        Plugins.AppendSeparator()
        
        videograbframe = Plugins.Append(wx.ID_ANY, "Extract images from video", 
                                            "Save in jpg format")
        Plugins.AppendSeparator()
        
        videosilent = Plugins.Append(wx.ID_ANY, "Remove audio from video ", 
                                            "Make silent a video")
        
        addsound = Plugins.Append(wx.ID_ANY, "Add audio track to a video", 
                                            "Add audio stream to a video")
        
        Plugins.AppendSeparator()
        
        audiograbseq = Plugins.Append(wx.ID_ANY, "Cut audio clip from a video", 
                                "Extract audio from a video sequence")
                            
        cutvideosequence = Plugins.Append(wx.ID_ANY, "Cut video clip from a video",
                                    "Extract a video sequence")
        Plugins.AppendSeparator()
        
        videorotate = Plugins.Append(wx.ID_ANY, "Change point of view", 
                                    "Change the orientation point in a video")
        
        menuBar.Append(Plugins,"Editing_Tools")
        
        ####------------------ setup button
        setupButton = wx.Menu()
        setupItem = setupButton.Append(wx.ID_PREFERENCES, "Setup", 
                                       "General Settings") 
        menuBar.Append(setupButton,"Preferences")
        
        ####------------------ help buton
        helpButton = wx.Menu()
        helpItem = helpButton.Append( wx.ID_HELP, "User Guide", "Program Guide")
        infoItem = helpButton.Append(wx.ID_ABOUT, "About Videomass", "About the program")
        menuBar.Append(helpButton,"Help")
        
        self.SetMenuBar(menuBar)
        
        #-----------------------Binding menu bar-------------------------#
        #----FILE----
        self.Bind(wx.EVT_MENU, self.File_Open, file_open)
        self.Bind(wx.EVT_MENU, self.File_Save, self.file_save)
        self.Bind(wx.EVT_MENU, self.Saveme, saveme)
        #self.Bind(wx.EVT_MENU, self.Save_all, save_all) # not implemented
        self.Bind(wx.EVT_MENU, self.Restore, restore)
        self.Bind(wx.EVT_MENU, self.Default, default)
        self.Bind(wx.EVT_MENU, self.Default_all, default_all)
        self.Bind(wx.EVT_MENU, self.Refresh, refresh)
        self.Bind(wx.EVT_MENU, self.Quiet, exitItem)
        #----STRUMENTI----
        self.Bind(wx.EVT_MENU, self.Addprof, addprof)
        self.Bind(wx.EVT_MENU, self.Editprof, editprof)
        self.Bind(wx.EVT_MENU, self.Delprof, delprof)
        #----EDITING----
        self.Bind(wx.EVT_MENU, self.Normalizer, normalize)
        self.Bind(wx.EVT_MENU, self.Audiograbber, audiograbber)
        self.Bind(wx.EVT_MENU, self.Videosilent, videosilent)
        self.Bind(wx.EVT_MENU, self.Addsound, addsound)
        self.Bind(wx.EVT_MENU, self.Audiograbseq, audiograbseq)
        self.Bind(wx.EVT_MENU, self.Cutvideosequence, cutvideosequence)
        self.Bind(wx.EVT_MENU, self.Videorotate, videorotate)
        self.Bind(wx.EVT_MENU, self.Videograbframe, videograbframe)
        #----PREFERENZE----
        self.Bind(wx.EVT_MENU, self.Setup, setupItem)
        #----HELP----
        self.Bind(wx.EVT_MENU, self.Helpme, helpItem)
        self.Bind(wx.EVT_MENU, self.Info, infoItem)

    #----------------------Event handler (callback)----------------------#
    #------------------------------------------------------------------#
    def File_Open(self, event):
        """
        Go in the on_open function for controls import file/dir
        """
        self.on_open(self)
        
    #------------------------------------------------------------------#
    def File_Save(self, event):
        """
        Go in the on_save function for controls export file/dir
        """
        self.on_save(self)
        
    #------------------------------------------------------------------#
    def Saveme(self, event):
        """
        save a single file copy of preset. The saved presets must be have
        same name where is saved to restore it correctly
        """
        combvalue = dict_presets[self.cmbx_prst.GetValue()][0]
        filedir = '%s%s.vdms' % (self.path_confdir, combvalue)
        filename = combvalue
        
        dialsave = wx.DirDialog(self, "Select a directory to save it")
        if dialsave.ShowModal() == wx.ID_OK:
            dirname = dialsave.GetPath()
            copy_backup(filedir, '%s/%s.vdms' % (dirname, filename))
            dialsave.Destroy()
            wx.MessageBox(u"The preset is saved", "Info", wx.OK, self)
            
    #------------------------------------------------------------------#
    #def Save_all(self, event):
        #"""
        #save all presets on a separate directory. The saved presets must be have
        #same name where is saved to restore it correctly
        
        #"""
        #combvalue = dict_presets[self.cmbx_prst.GetValue()][0]
        #filedir = '%s%s.vdms' % (self.path_confdir, combvalue)
        #filename = combvalue
        

        #dialsave = wx.DirDialog(self, "Scegli una cartella dove salvare "
                                    #"il file"
                                    #)                            
        #if dialsave.ShowModal() == wx.ID_OK:
            #dirname = dialsave.GetPath()
            #copy_backup(filedir, '%s/%s.vdms' % (dirname, filename))
            #dialsave.Destroy()
            #wx.MessageBox(u"Il preset è stato salvato correttamente.", 
                            #"Info", wx.OK, self)
            
    #------------------------------------------------------------------#
    def Restore(self, event):
        """
        restore a single preset file in the path presets of the program
        """
        combvalue = dict_presets[self.cmbx_prst.GetValue()][0]
        filedir = '%s%s.vdms' % (self.path_confdir, combvalue)
        filename = combvalue
        
        if self.OS == 'linux2':
            
            dialfile = wx.FileDialog(self, "Preset Restore:  "
                            "%s.vdms" % (filename), "", "", 
                            "Source (*.vdms)|*.vdms|", wx.OPEN
                            )
        else:
            
            dialfile = wx.FileDialog(self, "Preset Restore:  %s.vdms" % (
                                     filename), ".vdms", ".vdms", ".vdms",
                                     wx.OPEN
                                     )
        if dialfile.ShowModal() == wx.ID_OK:
            dirname = dialfile.GetPath()
            tail = os.path.basename(dirname) # take a filename for valuated
            dialfile.Destroy()
        
            if wx.MessageBox(u'The preset "%s" will be imported and will '
                    u'overwrite the one in use ! Proceed ?' % (tail), 
                    'Please confirm', wx.ICON_QUESTION | wx.YES_NO, 
                                self) == wx.NO:
                return
            
            if tail != '%s.vdms' % filename:
                wx.MessageBox("The name of the preset imported does not "
                              "match the one in use!!", "ERROR !", 
                                wx.ICON_ERROR, self
                                )
                return
            
            copy_restore('%s' % (dirname), filedir)
            self.reset_list() # re-charging functions
            
    #------------------------------------------------------------------#
    def Default(self, event):
        """
        copy the single original preset file into the configuration
        folder. This replace new personal changes make at profile.
        """ 
        #copy_restore('%s/share/av_presets.xml' % (self.PWD), '%s' % (self.dirconf))
        if wx.MessageBox("The current preset will be overwritten! proceed?", \
                         "Please confirm", wx.ICON_QUESTION | 
                         wx.YES_NO, self) == wx.NO:
            return
        
        filename = dict_presets[self.cmbx_prst.GetValue()][0]
        copy_restore('%s/%s.vdms' % (self.path_srcShare, filename), 
                                    '%s%s.vdms' % (self.path_confdir, filename)
                      )
        self.reset_list() # re-charging functions
        
    #------------------------------------------------------------------#
    def Default_all(self, event):
        """
        restore all preset files in the path presets of the program
        """
        if wx.MessageBox(u"WARNING: you are going to restore all default "
                         "presets from videomass! Proceed?", "Please confirm", 
                    wx.ICON_QUESTION | wx.YES_NO, self) == wx.NO:
            return

        copy_on('vdms', self.path_srcShare, self.path_confdir)

    #------------------------------------------------------------------#
    def Refresh(self, event):
        """ 
        Pass to reset_list function for re-charging list
        """
        self.reset_list()
        
    #------------------------------------------------------------------#
    def Quiet(self, event):
        """
        destroy the videomass.
        """
        self.Destroy()
        
    #------------------------------------------------------------------#
    def Addprof(self, event):
        """
        Store new profiles in the same presets selected in the
        combobox. The list is reloaded automatically after
        pressed ok button in the dialog for update view.
        """
        filename = dict_presets[self.cmbx_prst.GetValue()][0]
        name_preset = dict_presets[self.cmbx_prst.GetValue()][1]
        full_pathname = '%s%s.vdms' % (self.path_confdir, filename)

        prstdialog = presets_addnew.MemPresets(self, 'newprofile', 
                          full_pathname, filename, None, 'Create a new '
                          'profile on the selected preset: "%s"' % (
                              name_preset))
        ret = prstdialog.ShowModal()
        
        if ret == wx.ID_OK:
            self.reset_list() # re-charging list_ctrl with newer
            
    #------------------------------------------------------------------#
    def Editprof(self, event):
        """
        A choice in the list (profile) can be edit in all own part.
        The list is reloaded automatically after pressed ok button 
        in the dialog.
        """
        if array == []:
            self.statusbar_msg("Before to edit a profile select "
                               "an item from the control list",red)
            return
            
        else:
            
            filename = dict_presets[self.cmbx_prst.GetValue()][0]
            name_preset = dict_presets[self.cmbx_prst.GetValue()][1]
            full_pathname = '%s%s.vdms' % (self.path_confdir, filename)
            
            prstdialog = presets_addnew.MemPresets(self, 'edit', full_pathname, 
                   filename, array, 'change the profile on the selected preset: ' 
                       '"%s"' % (name_preset))
            
            ret = prstdialog.ShowModal()
            
            if ret == wx.ID_OK:
                self.reset_list() # re-charging list_ctrl with newer
                
    #------------------------------------------------------------------#
    def Delprof(self, event):
        """
        Delete a choice in list_ctrl. This delete only single
        profile of the preset used
        """
        if array == []:
            self.statusbar_msg("Before deleting a profile, select "
                               "an item from the control list",red)
        else:
            filename = dict_presets[self.cmbx_prst.GetValue()][0]
            # call module-function and pass list as argument
            delete_profiles(array, filename)
            self.reset_list()
            
    #------------------------------------------------------------------#
    def Normalizer(self, event):
        
        frame = normalize.Normalize(self, self.helping, self.ffmpeg_link, 
                                    self.threads, self.ffmpeg_log, 
                                    self.command_log, self.path_log, 
                                    self.loglevel_type
                                     )
        frame.Show()
        
    #------------------------------------------------------------------#
    def Audiograbber(self, event):
        
        frame = audiograbber.AudioGrabber(self, self.helping, self.ffmpeg_link, 
                                          self.ffprobe_link, self.threads, 
                                          self.ffmpeg_log, self.command_log, 
                                          self.path_log, self.loglevel_type, 
                                          self.loglevel_batch
                                           )
        frame.Show()
        
    #------------------------------------------------------------------#
    def Videosilent(self, event):
        
        frame = videosilent.VideoSilent(self, self.helping, self.ffmpeg_link, 
                                        self.threads, self.ffmpeg_log, 
                                        self.command_log, self.path_log, 
                                        self.loglevel_type
                                        )
        frame.Show()
        
    #------------------------------------------------------------------#
    def Addsound(self, event):
        
        frame = audioadd.AudioAdd(self, self.helping, self.ffmpeg_link, 
                                  self.threads, self.ffmpeg_log, 
                                  self.command_log, self.path_log, 
                                  self.loglevel_type,
                                   )
        frame.Show()
        
    #------------------------------------------------------------------#
    def Audiograbseq(self, event):

        frame = audiograbseq.AudiograbSeq(self, self.helping, self.ffmpeg_link, 
                                    self.ffprobe_link, self.ffmpeg_log, 
                                    self.command_log, self.path_log, 
                                    self.loglevel_type
                                          )
        frame.Show()
        
    #------------------------------------------------------------------#
    def Cutvideosequence(self, event):
        
        frame = videocut.VideoCut(self, self.helping, self.ffmpeg_link, 
                                  self.threads, self.ffmpeg_log, 
                                  self.command_log, self.path_log, 
                                  self.loglevel_type
                                  )
        frame.Show()
        
    #------------------------------------------------------------------#
        
    def Videorotate(self, event):
        
        frame = videorotate.VideoRotate(self, self.helping, self.ffmpeg_link, 
                                        self.threads, self.ffmpeg_log, 
                                        self.command_log, self.path_log, 
                                        self.loglevel_type
                                         )
        frame.Show()
        
    #------------------------------------------------------------------#
    def Videograbframe(self, event):
        
        frame = videograbframe.VideoFrame(self, self.helping, self.ffmpeg_link, 
                                          self.threads, self.ffmpeg_log, 
                                          self.command_log, self.path_log, 
                                          self.loglevel_type
                                           )
        frame.Show()
        
    #------------------------------------------------------------------#
    def Setup(self, event):
        """
        Call the module setup for setting preferences
        """
        #self.parent.Setup(self)
        setup_dlg = settings.setting(self, self.threads, self.ffmpeg_log, 
                                     self.command_log, self.path_log, 
                                     self.ffmpeg_link, self.ffmpeg_check,
                                     self.ffprobe_link, self.ffprobe_check, 
                                     self.ffplay_link, self.ffplay_check, 
                                     self.full_list, self.writeline_exec
                                     )
        setup_dlg.ShowModal()
        
    #------------------------------------------------------------------#
    def Helpme(self, event):
        
        #wx.MessageBox("La guida al programma deve venire sviluppata a breve.")
        #self.on_manual(self)
        #self.parent.on_help(self)
        webbrowser.open('%s/Indice_ipertestuale.html' % (self.helping))
        
    #------------------------------------------------------------------#
    def Info(self, event):
        """
        Display the program informations and developpers
        
        """
        infoprg.info(self.videomass_icon)
        
    #------------------------BUILD THE TOOL BAR--------------------------#
    def videomass_tool_bar(self):
        """
        Makes and attaches the view tools bar
        """
        #--------- Properties
        self.toolbar = self.CreateToolBar(style=(wx.TB_HORZ_LAYOUT | wx.TB_TEXT))
        #self.toolbar.SetToolBitmapSize((32,32))
        self.toolbar.SetFont(wx.Font(8, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))
        
        #-------- Play button
        play = self.toolbar.AddLabelTool(wx.ID_FILE3, ' Play Media',
                                         wx.Bitmap(self.icon_play),
                                                )
        self.toolbar.EnableTool(wx.ID_FILE3, False)
        
        self.toolbar.AddSeparator()
        
        #-------- Info button
        analyze = self.toolbar.AddLabelTool(wx.ID_FILE4, 'Info Media', 
                                    wx.Bitmap(self.icon_analyze)
                                                )
        self.toolbar.EnableTool(wx.ID_FILE4, False)
        self.toolbar.AddSeparator()
        
        #-------- Switch at videomass
        switch = self.toolbar.AddLabelTool(wx.ID_ANY, ' VSCI', 
                                    wx.Bitmap(self.icon_switchvideomass)
                                                )
        self.toolbar.AddSeparator()
        
        # ------- Run process button
        run_coding = self.toolbar.AddLabelTool(wx.ID_OK, ' Start Encoding', 
                                    wx.Bitmap(self.icon_process)
                                                )
        self.toolbar.EnableTool(wx.ID_OK, False)
        self.toolbar.AddSeparator()
        
        # ------- help button
        help_contest = self.toolbar.AddLabelTool(wx.ID_ANY, ' Contextual Help', 
                                    wx.Bitmap(self.icon_help)
                                                )
        # finally, create it
        self.toolbar.Realize()

        #--------------------------Binding (evt)------------------------#
        self.Bind(wx.EVT_TOOL, self.Playpath, play)
        self.Bind(wx.EVT_TOOL, self.Analyze, analyze)
        self.Bind(wx.EVT_TOOL, self.switch_video_sci, switch)
        self.Bind(wx.EVT_TOOL, self.Run_Coding, run_coding)
        self.Bind(wx.EVT_TOOL, self.Help_Contest, help_contest)

    #---------------------Callback (event handler)----------------------#
    #------------------------------------------------------------------#
    def Playpath(self, event):
        """
        Preview of media imported with open button.
        """
        filepath = self.text_path_open.GetValue()
        play_input(filepath, self.ffplay_link, self.loglevel_type)

    #------------------------------------------------------------------#
    def Analyze(self, event):
        """
        Show media information files imported with open button.
        This function call mediainfo.py that is window for metadata
        info multimedia files, witch call ffprobe.py for parsing.
        """
        filepath = self.text_path_open.GetValue()
        
        try:
            with open(filepath):
                dialog = mediainfo.Mediainfo(self, filepath, self.helping, 
                                            self.ffprobe_link)
                dialog.Show()

        except IOError:
            wx.MessageBox("File does not exist:  %s" % (filepath), "WARNING",
                        wx.ICON_EXCLAMATION, self)
        
    #------------------------------------------------------------------#
    def switch_video_sci(self, event):
        """
        Show Video Edit
        """
        frame = video_sci.Video_Sci(self, self.helping, self.ffmpeg_link, 
                                      self.threads, self.command_log, 
                                      self.path_log, self.loglevel_type, 
                                      self.icon_play, self.icon_analyze, 
                                      self.icon_process, self.icon_help, 
                                      self.icon_presets, self.videomass_icon, 
                                      self.OS, self.ffprobe_link, 
                                      self.ffplay_link, self.ffmpeg_log, 
                                      self.full_list, self.path_srcShare, 
                                      self.loglevel_batch
                                      )
        frame.Show()
        #event.Skip()
        
    #------------------------------------------------------------------#
    def Run_Coding(self, event):
        """
        Pass at on_ok function for process
        """
        self.on_ok(self)
        
    #------------------------------------------------------------------#
    def Help_Contest(self, event):
        """
        Run the predefined browser on contestual help
        """
        webbrowser.open('%s/04-Gestione_presets.html' % (self.helping))
        
