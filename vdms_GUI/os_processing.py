#!/usr/bin/python
# -*- coding: UTF-8 -*- 
#
#
#########################################################
# Name: os_processing.py (for wxpython >= 2.8)
# Porpose: module for processing commands
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (06) 24/08/2014
# Rev (07) 12/01/2015
# Rev (08) 20/04/2015
#########################################################

"""
This module rapresent the main unit of videomas program for all processes 
that involve the subprocess module, classes for separate threads and all 
dialogs/frames for show information on progress process.
"""
import wx
import subprocess
import shlex
import os
import glob
import string
from threading import Thread
import time
import re
try:
    from wx.lib.pubsub import Publisher
    API = 'old api'
except ImportError:
    # pubsub’s old API not work on wxPython >= 2.9
    from wx.lib.pubsub import pub
    API = 'new api'

from vdms_utils.os_interaction import copy_restore, rename_file

##########################
"""
Set global variable for batch thread process:
"""
CHANGE_STATUS = None
STATUS_ERROR = None

"""
current user directory:
"""
DIRNAME = os.path.expanduser('~') # /home/user


########################################################################
# CLASS AND FUNCTIONS FOR PROCESS:
########################################################################

class FFProbe(object):
    """
    FFProbe wraps the ffprobe command and pulls the data into an object form:
    
    metadata = FFProbe(filename, ffprobe_link)
    
    You can use the methods of this class for getting metadata in separated 
    list of each stream or format, example:
    
    print metadata.video_stream()
    print metadata.audio_stream()
    print metadata.subtitle_stream()
    print metadata.data_format()
    
    First you should do a check for errors that might generate ffprobe 
    or lack of it.
    Example for a control errors interface before instantiating the 
    other methods:
    
    if metadata.ERROR(): # control for errors
        print "Some Error:  %s" % (metadata.error)
        return

    This class was partially inspired to: 
    https://github.com/simonh10/ffprobe/blob/master/ffprobe/ffprobe.py
    thank at:
    simonh10
    """
    def __init__(self, mediafile, ffprobe_link):
        """
        The ffprobe command has stdout and stderr (unlike ffmpeg and ffplay)
        which allows me to initialize attribute for errors
        """
        self.error = False
        self.mediastreams = []
        self.mediaformat = []
        self.video = []
        self.audio = []
        self._format = []
        self.subtitle = []
        datalines = []

        try:
            cmnd = ['ffprobe', '-show_format', '-show_streams', '-v', 
                    'error', '-pretty', mediafile]

            p = subprocess.Popen(cmnd, stdout=subprocess.PIPE, 
                                 stderr=subprocess.PIPE)
            output, error =  p.communicate()

            if error:
                self.error = error

        except OSError:
            self.error = 'ffprobe command not found in the path system'
            return

        raw_list = string.split(output, '\n') # create list with strings element

        for s in raw_list:
            if re.match('\[STREAM\]',s):
                datalines=[]

            elif re.match('\[\/STREAM\]',s):
                self.mediastreams.append(datalines)
                datalines=[]
            else:
                datalines.append(s)

        for f in raw_list:
            if re.match('\[FORMAT\]',f):
                datalines=[]

            elif re.match('\[\/FORMAT\]',f):
                self.mediaformat.append(datalines)
                datalines=[]
            else:
                datalines.append(f)

    def video_stream(self):
        """
        Return a metadata list for video stream. If there is not
        data video return a empty list
        """
        for datastream in self.mediastreams:
            if 'codec_type=video'in datastream:
                self.video.append(datastream)
        return self.video

    def audio_stream(self):
        """
        Return a metadata list for audio stream. If there is not
        data audio return a empty list
        """
        for datastream in self.mediastreams:
            if 'codec_type=audio'in datastream:
                self.audio.append(datastream)
        return self.audio

    def subtitle_stream(self):
        """
        Return a metadata list for subtitle stream. If there is not
        data subtitle return a empty list
        """
        for datastream in self.mediastreams:
            if 'codec_type=subtitle'in datastream:
                self.subtitle.append(datastream)
        return self.subtitle

    def data_format(self):
        """
        Return a metadata list for data format. If there is not
        data format return a empty list
        """
        for dataformat in self.mediaformat:
                self._format.append(dataformat)
        return self._format

    def get_audio_codec_name(self):
        """
        Return a list of possible audio stream codec name into a
        video, movie. If no audio stream in video return None.
        """
        audio_list = self.audio_stream()
        stream_list = []
        
        if audio_list == []:
            #stream_list.append('no audio stream')
            print 'No AUDIO stream metadata found'
            return
        else:    
            n = len(audio_list)
            for a in range(n):
                (key, value) = audio_list[a][0].strip().split('=')
                for b in audio_list[a]:
                    (key, value) = b.strip().split('=')
                    if "codec_name" in key:
                        stream_list.append(value)
        return stream_list

    def ERROR(self):
        """
        check if there are errors on stderr of ffprobe command or if there 
        is a IOError exception. For output errors you can use this method 
        as control interface before using all other methods of this class.
        """
        if self.error:
            return self.error

########################################################################
def play_input(filepath, ffplay_link, loglevel_type):
    """
    Run process for play imported media with a called at ffplay witch 
    need x-window-terminal-emulator for show the multimedia files.
    NOTE: the loglevel is set on quiet and not use global set loglevel_type
    because exit with too errors on this version of ffmpeg 2.1
    """
    #self.Disable()
    try:
        with open(filepath): # ctrl if exist file
            
            try:
                subprocess.check_call("'%s' -i '%s' -loglevel %s" % (
                    ffplay_link, filepath, loglevel_type) , shell = True)
                                        
            except subprocess.CalledProcessError:

                wx.MessageBox("CalledProcessError: exit with status error !\n"
                                "ffplay exist ?", "ERROR", wx.ICON_ERROR)
                return
                
    except IOError:
        wx.MessageBox("File does not exist:  %s" % (filepath), "WARNING",
                    wx.ICON_EXCLAMATION)
        return
    
########################################################################

#def play_input(filepath, ffplay_link, loglevel_type):
    #"""
    #Run process for play imported media with a called at ffplay witch 
    #need x-window-terminal-emulator for show the multimedia files.
    #NOTE: the loglevel is set on quiet and not use global set loglevel_type
    #because exit with too errors on this version of ffmpeg 2.1
    #"""
    #try:
        #with open(filepath): # ctrl if exist file
            
            ##command = [ffplay_link, '-loglevel', loglevel_type, filepath]
            #command = [ffplay_link, '-i' , filepath, '-loglevel', 'error']
            #p = subprocess.Popen(command, stderr=subprocess.PIPE)
            #error =  p.communicate()
            
            #if error:
                #wx.MessageBox("[ffplay] Error:  %s" % (error), "ERROR",
                            #wx.ICON_EXCLAMATION)

                #return
                
    #except IOError:
        #wx.MessageBox("File inesistente:  %s" % (filepath), "WARNING",
                        #wx.ICON_ERROR)

        #return
        
########################################################################
def proc_volumedetect(filepath, ffmpeg_link):
    """
    This function is only for batch_mode normalization and be 
    used by normalize.py for get volume levels 
    """
    loadDlg = PopupDialog(None, ("Videomass - Loading..."), 
                                ("\nWait....\nAudio peak analysis .\n")
                                )
    cmnd = [ffmpeg_link, '-i', filepath, '-af', 'volumedetect', '-f', 
            'null', '-']
    status = 0
    try:
        with open(filepath):
            p = subprocess.Popen(cmnd, stdout=subprocess.PIPE, 
            stderr=subprocess.PIPE)
            output, error =  p.communicate()
            raw_list = string.split(error) # splitta tutti gli spazi
            lista = string.split(error, '\n') # splitta solo gli a capo
            loadDlg.Destroy()
            
            if 'mean_volume:' in raw_list:
                mean_volume = raw_list.index("mean_volume:") # indice integear
                max_volume = raw_list.index("max_volume:") # indice integear
                maxvol_append = "%s %s" % (raw_list[max_volume + 1], "dB")
                medvol_append = "%s %s" % (raw_list[mean_volume + 1], "dB" )

            elif "%s: Invalid data found when processing input" % (
                                                        filepath) in lista:
                wx.MessageBox("The files validation has not been succesful. "
                              "The file is not supported.", "ERROR !", 
                              wx.ICON_ERROR
                              )
                status = None
                return
            else:
                wx.MessageBox("Error", "ERROR !",  wx.ICON_ERROR)
                status = None
                return
    except IOError:
        loadDlg.Destroy()
        wx.MessageBox("File does not exist:  %s" % (filepath), "WARNING",
                        wx.ICON_EXCLAMATION)
        status = None
        return
    
    return (max_volume, mean_volume, maxvol_append, 
            medvol_append, raw_list, status
            )

########################################################################

class proc_normalize_batch_thread(Thread):
    """
    Pass a separate process for conversions files group (batch mode) 
    and send the status progress of loop at the ProgressDialog class
    that update the progress bar. The ProgressDialog bar measure the 
    number of files list amount in the filelist.append. This process 
    is only for audio/files normalization.  
    """
    def __init__(self, filelist, inputdir, n_threads, extoutput, outputdir, 
                                limited, normalize, logname, ffmpeg_link):
        """
        Init Worker Thread Class.
        """
        Thread.__init__(self)
        
        self.filelist = filelist  # list of files (elements)
        self.dirname = '%s' % inputdir # input path
        self.n_threads = n_threads # comand set 
        self.outputdir = outputdir # output path
        self.extoutput = extoutput # output format (extension)
        self.limited = limited # soglia di files da non processare
        self.normalize = normalize # db volume target
        self.logname = logname # is for log filename
        self.ffmpeg_link = ffmpeg_link # bin executable path-name 
        
        self.start()    # start the thread (va in self.run())
        
        ##########################
    def run(self):
        """
        Run Worker Thread. This is the code executing in the new thread
        for global variables declaration should must make before create 
        global varible, otherwise occured: 
        SyntaxWarning: name 'STATUS_ERROR' is assigned to before global 
        declaration
        """
        global STATUS_ERROR
        
        for video in self.filelist:
            
            if CHANGE_STATUS == 1: # loop terminate. User change idea
                    break
                    
            if STATUS_ERROR == 1: # loop terminate. Process ffmpeg error
                    break
            
            if os.path.isfile(os.path.join(self.dirname,video)):
                
                cmnd = [self.ffmpeg_link, '-i', video, '-af', 'volumedetect', \
                    '-f', 'null', '-']
                p = subprocess.Popen(cmnd, stderr = subprocess.PIPE)
                error_a =  p.communicate()[1]
                raw_list = string.split(error_a) # splitta tutti gli spazi
                #lista = string.split(error_a, '\n') # splitta solo gli a capo
                
                if 'mean_volume:' in raw_list:
                    max_volume = raw_list.index("max_volume:")
                    maxvol = raw_list[max_volume + 1]
                    
                    if float(maxvol) < self.limited: # if vol max is inferior at..
                        # non processa se è inferirore a limited
                        result = float(maxvol) - float(self.normalize)
                        result_str = str(result)
                        
                        if float(maxvol) >= float(self.normalize):
                            # non processa se è superiore o uguale a norm.
                            pass
                        
                        else:
                            typecommand = "%s -i '%s' -loglevel error -af \
                                    volume=%sdB %s '%s/%s.%s'" % (
                                    self.ffmpeg_link, video, result_str[1:], 
                                    self.n_threads, self.outputdir, 
                                    video, self.extoutput
                                                )
                            args = shlex.split(typecommand)
                            p = subprocess.Popen(args, stderr=subprocess.PIPE)
                            error =  p.communicate()[1]

                    #-------------------------------------------------------
                    #try:
                        #subprocess.check_call("%s -i '%s' -af \
                                        #volume=%sdB %s '%s/%s.%s'" % (
                                        #ffmpeg_link, video, result_str[1:], 
                                        #self.command, self.outputdir, 
                                        #video, self.extoutput) , shell = True)
                                        
                    #except subprocess.CalledProcessError:
                        
                        #File = open("%s/.videomass/videomass_error.log" % 
                                                                #DIRNAME,"w")
                        #File.write("E' stata sollevata un'eccezione")
                        #File.close()
                        #STATUS_ERROR = 1
                    #------------------------------------------------------
                            if error != '':
                                # scrivo un log del comando su ~/.videomass
                                File = open("%s/.videomass/%s" % (DIRNAME, 
                                                            self.logname), "a")
                                File.write("%s" % error)
                                File.close()
                                STATUS_ERROR = 1
            if API == 'old api':
                wx.CallAfter(Publisher().sendMessage, "update", "")
            else:
                #wx.CallAfter(pub.subscribe(sendMessage, "update"))
                wx.CallAfter(pub.sendMessage, "update", msg="")

########################################################################

class proc_std(wx.Frame):
    """
    Conversion parameters for single file process (not batch). The
    parameters are build and passed at this std_conv(self) where required
    a process. Also, run the new dialog with display text and a separate 
    thread for view ffmpeg progress process.
    Is not bound a some specific programs and 'command' can used with all.
    """
    def __init__(self, parent, title, command, pathlog, logname, makelog):
        
        wx.Frame.__init__(self, parent, -1, title=title, style=wx.CAPTION)
        self.parent = parent # this window is a child of a window parent
        self.command = command # command for process
        self.path_log = pathlog # path for save a log for the users
        self.logname = logname # is filename type
        self.makelog = makelog # if = true make a copy into self.path_log
        self.status_run = 0 # 0 = end process, 1 = process interrupted 
        
        self.panel_base = wx.Panel(self, wx.ID_ANY)
        self.OutText = wx.TextCtrl(self.panel_base, wx.ID_ANY, size=(700,400),
                        style = wx.TE_MULTILINE | wx.TE_READONLY | wx.TE_RICH2
                                    )
        self.sizer_with_label_staticbox = wx.StaticBox(self.panel_base, 
                                wx.ID_ANY, "Progress Output ffmpeg command"
                                                        )
        self.button_stop = wx.Button(self.panel_base, wx.ID_STOP, "")
        self.button_close = wx.Button(self.panel_base, wx.ID_CLOSE, "")

        # __set_properties
        self.OutText.SetBackgroundColour((217, 255, 255))
        self.OutText.SetToolTipString("Output of the FFmpeg command line")
        self.button_stop.SetMinSize((200, 27))
        self.button_stop.SetToolTipString("Stops the current process")
        self.button_close.SetMinSize((200, 27))

        # __do_layout
        sizer_base = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_flex = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_buttons = wx.GridSizer(1, 2, 0, 0)
        self.sizer_with_label_staticbox.Lower()
        sizer_with_label = wx.StaticBoxSizer(self.sizer_with_label_staticbox, 
                                                                wx.VERTICAL)
        sizer_with_label.Add(self.OutText, 0, wx.ALL | wx.EXPAND, 5)
        grid_sizer_flex.Add(sizer_with_label, 1, wx.ALL | wx.EXPAND, 5)
        grid_sizer_buttons.Add(self.button_stop, 0, wx.ALL, 10)
        grid_sizer_buttons.Add(self.button_close, 0, wx.ALL | wx.ALIGN_RIGHT, 10)
        grid_sizer_flex.Add(grid_sizer_buttons, 1, wx.EXPAND, 0)
        self.panel_base.SetSizer(grid_sizer_flex)
        grid_sizer_flex.AddGrowableRow(0)
        grid_sizer_flex.AddGrowableRow(1)
        grid_sizer_flex.AddGrowableCol(0)
        sizer_base.Add(self.panel_base, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_base)
        sizer_base.Fit(self)
        self.Layout()
        self.CentreOnScreen()
        self.Show()
        self.OutText.Clear()
        
        # bind
        self.Bind(wx.EVT_BUTTON, self.on_stop, self.button_stop)
        self.Bind(wx.EVT_BUTTON, self.on_close, self.button_close)
        
        # scrivo un log del comando su ~/.videomass
        current_date =  time.strftime("%c") # date/time
        is_list = string.split(command)
        format_string = string.join(is_list)
        log = open("%s/.videomass/%s" % (DIRNAME, logname),"w")
        log.write('[PYTHON] CURRENT DATE/TIME:\n%s' % current_date)
        log.write('\n\n[VIDEOMASS] COMMAND LINE:\n%s' % format_string)
        log.write('\n\n[FFMPEG] OUTPUT PROCESS:\n')
        log.close()
        
        try:
            # WARNING: se non è shell=True, con gli operatori di controllo && fallisce
            self.std_process = subprocess.Popen(self.command, shell=True, 
                                    stderr=subprocess.PIPE)
            # I redirect stderr in subprocess.PIPE and start thread 
            # out_thread which read stderr of this process and add to TextCtrl:
            out_thread = OutTextThread(self.std_process.stderr, self.AddText, self.logname)
            out_thread.start()
            
            self.button_stop.Enable(True)
            self.button_close.Enable(False)
            self.parent.Hide()
            
        except OSError:
            
            self.Hide()
            wx.MessageBox("Subprocess: exit with OSError !\n"
                          "ffmpeg exist ?", "ERROR", wx.ICON_ERROR)
            self.Destroy()
        # publisher update the ending status:
        if API == 'old api':
            Publisher().subscribe(self.updateDisplay, "update")
        else:
            pub.subscribe(self.updateDisplay, "update")

    def updateDisplay(self, msg):
        """
        Received the end thread of the process when the while loop
        in the thread has finish or if interrupted process
        """
        
        #File = open("/home/gianluca/Video/preset.log","r")
        #li = File.readlines()
        #File.close()
        
        if self.status_run == 1:
            self.OutText.SetDefaultStyle(wx.TextAttr(wx.RED))
            self.OutText.AppendText('\n !! Interrupted Process !!\n')
        else:
            self.OutText.SetDefaultStyle(wx.TextAttr(wx.BLUE))
            self.OutText.AppendText('\n ...Process successfully executed\n')

        self.button_stop.Enable(False)
        self.button_close.Enable(True)

        # if user want file log
        if self.makelog == 'true': 
            copy_restore("%s/.videomass/%s" % (DIRNAME, self.logname),
                            "%s/%s" % (self.path_log, self.logname))

    def on_stop(self, event):
        """
        The user change idea and was stop process
        """
        self.std_process.terminate()
        self.status_run = 1
        event.Skip()

    def on_close(self, event):
        """
        close dialog and show main frame
        """
        self.parent.Show()
        self.Destroy()
        event.Skip()

    def AddText(self, text):
        """
        Update the output and send at the thread
        """
        wx.CallAfter(self.OutText.AppendText, text)

#-----------------INIT THREAD FOR SINGLE PROCESS------------------------#

class OutTextThread(Thread):
    """
    This class is a thread for single process
    """
    def __init__(self, std_out, cb, prgname):
        super(OutTextThread, self).__init__()
        self.std_out = std_out # output of a process
        self.cb = cb # append text into ctrltext
        self.prgname = prgname # is log filename

    def run(self):
        # make a file log for control errors
        File = open("%s/.videomass/%s" % (DIRNAME, self.prgname), "a")

        text = None
        while text != '':
            #text = self.std_out.readline() # readline is not realtime
            text = self.std_out.read(1)
            self.cb(text)
            File.write("%s" % text)

        File.close()
        self.cb('\n End thread.\n')
        # send the end of process (exit loop):
        if API == 'old api':
            wx.CallAfter(Publisher().sendMessage, "update", "Thread finished!")
        else:
            wx.CallAfter(pub.sendMessage, "update", msg="")

########################################################################
class proc_batch_thread(Thread):
    """
    Pass a separate process for conversions files group (batch mode) 
    and send the status progress of loop at the ProgressDialog class
    that update the progress bar.
    """
    def __init__(self, filelist, inputdir, extoutput, outputdir, command, 
                            pass1, pass2, logname, ffmpeg_link):
        """
        Init Worker Thread Class.
        """
        Thread.__init__(self)
        
        self.filelist = filelist # list of files (elements)
        self.dirname = '%s' % inputdir # input path
        self.command = command # comand set on no-double-pass
        self.pass1 = pass1 # comand set on first-pass
        self.pass2 = pass2 # comand set on second-pass
        self.outputdir = outputdir # output path
        self.extoutput = extoutput # format (extension)
        self.logname = logname # is for log filename
        self.ffmpeg_link = ffmpeg_link # bin executable path-name

        self.start()    # start the thread (va in self.run())
        ##########################

    def run(self):
        """
        Run Worker Thread. This is the code executing in the new thread
        
        for global variables declaration should must make before create global 
        varible, otherwise occured: 
        SyntaxWarning: name 'STATUS_ERROR' is assigned to before global declaration
        """
        global STATUS_ERROR
        
        for video in self.filelist:
            if CHANGE_STATUS == 1: # loop terminate. User change idea
                    break
            if STATUS_ERROR == 1: # loop terminate. Process error
                    break
            if self.pass1 == None and self.pass2 == None:
                # single pass ffmpeg
                typass = "%s -i '%s' %s '%s/%s.%s'" % (self.ffmpeg_link, 
                                                        video, self.command, 
                                                        self.outputdir, video, 
                                                        self.extoutput
                                                        )
            else:
                # double pass ffmpeg
                typass = "%s -i '%s' %s %s -i '%s' %s '%s/%s.%s'" % (
                                           self.ffmpeg_link, video, self.pass1, 
                                           self.ffmpeg_link, video, self.pass2, 
                                           self.outputdir, video, 
                                           self.extoutput)
            if os.path.isfile(os.path.join(self.dirname,video)):
                
                # WARNING: se non è shell=True, la doppia passata && non vale
                # args = shlex.split(typass)
                p = subprocess.Popen(typass, shell=True, stderr=subprocess.PIPE)
                error =  p.communicate()[1]
                
                if error != '':
                    # scrivo un log del comando su ~/.videomass
                    File = open("%s/.videomass/%s" % (DIRNAME, self.logname), "a")
                    File.write("%s" % error)
                    File.close()
                    STATUS_ERROR = 1
                    
                if API == 'old api':
                    wx.CallAfter(Publisher().sendMessage, "update", "")
                else:
                    #wx.CallAfter(pub.subscribe(sendMessage, "update"))
                    wx.CallAfter(pub.sendMessage, "update", msg="")

########################################################################
def control_errors(inputdir, extinput, log_command, logname):
    """
    Before proceded with proc_batch_thread, there is a bit control of
    the exist path, filename and extensions inputs.
    """
    # scrivo un log del comando su ~/.videomass
    current_date =  time.strftime("%c") # date/time
    is_list = string.split(log_command) # become list
    format_string = string.join(is_list) # format the string
    log = open("%s/.videomass/%s" % (DIRNAME, logname),"w")
    log.write("""[PYTHON] CURRENT DATE/TIME:
%s\n\n
[VIDEOMASS] COMMAND LINE BATCH:
%s\n\n
[VIDEOMASS] INFO FOR USERS: 
batch Process not include output verbose of ffmpeg, loglevel is set to 'error'.
\n\n
[FFMPEG] ERRORS:\n""" % (current_date, format_string))
    
    log.close()
    filelist = []
    try:
        os.chdir(inputdir)
    except OSError:
        wx.MessageBox("File or directory does not exist: '%s' !" % (inputdir), 
                        "No exist file/dir !", wx.ICON_ERROR
                        )
        return ('error', None, None)
    
    for video in glob.glob("*.%s" % extinput):
            filelist.append(video) # fill the filelist = []
            
    lenghmax = len(filelist)
    
    if lenghmax == 0: # Not found file with extension in extinput
        
        wx.MessageBox("The folder do not contains files with this extension: "
                        "'%s' ! " % (extinput), "Incorrect file extension !", 
                        wx.ICON_ERROR
                        )
        return ('error', None, None)
    return ('no_error', lenghmax, filelist)

########################################################################
class ProgressDialog(wx.Dialog):
    """
    Defines a gauge and a timer which updates the gauge. This dialog
    come use only by all batch process.
    """
    def __init__(self, parent, title, lenghmax, extinput, extoutput, outputdir,
                    pathlog, makelog, logname):
        
        wx.Dialog.__init__(self, parent, -1, title=title)
        # Settings Variables:
        self.parent = parent # used for disable/enable main frame
        self.extinput = extinput # estensione di gruppo
        self.extoutput = extoutput # formato
        self.outputdir = outputdir
        self.path_log = pathlog # path for save a log for the users
        self.logname = logname # is for log filename
        self.makelog = makelog # if = true make a copy into self.path_log
        self.maxer = lenghmax # number of elements content list (integear)
        self.count = 0 # setting iniziale del contatore

        self.barProg = wx.Gauge(self, wx.ID_ANY, range = self.maxer)
        self.labelState = wx.StaticText(self, wx.ID_ANY | wx.ALIGN_CENTER, (
                                      'Process Number 1 of %s' % self.maxer))
        self.button = wx.Button(self, wx.ID_STOP, "")

        #-----PROPERTIES
        self.SetTitle(title)
        self.barProg.SetMinSize((240, 30))

        #-----LAYOUT
        sizer_base = wx.BoxSizer(wx.VERTICAL)
        grid_sizer = wx.FlexGridSizer(3, 1, 0, 0)
        grid_sizer.Add(self.barProg, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | 
                wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer.Add(self.labelState, 0, wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer.Add(self.button, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        sizer_base.Add(grid_sizer, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_base)
        sizer_base.Fit(self)
        self.CentreOnScreen()
        # Display the Dialog
        self.Show()
        # disable main frame
        self.parent.Disable()

        #----BINDING
        self.Bind(wx.EVT_BUTTON, self.on_cancel, self.button)

        # create a pubsub listener/receiver
        if API == 'old api':
            Publisher().subscribe(self.updateProgress, "update")
        else:
            pub.subscribe(self.updateProgress, "update")

    #------------------UPDATE GAUGE---------------#	
    def updateProgress(self, msg):
        """
        self.count increase for number of loop. 
        """
        self.count += 1 # per ogni ciclo for aumenta di uno
        
        if self.count >= self.maxer:
            # when end process count = maxer
            self.Close()
            self.post_proc()
            self.parent.Enable()
            self.Destroy() 
        elif CHANGE_STATUS == 1:
            # the user change idea and stop the process
            self.log_for_user()
            self.reset_status()
            self.parent.Enable()
            self.Destroy()
        elif STATUS_ERROR == 1:
            # if ffmpeg exit with status stderr in the 
            # proc_batch_thread, stop process 
            self.Close()
            #File = open('%s/.videomass/videomass_error.log' % (DIRNAME),'r')
            #err = File.readlines()
            #File.close()
            #wx.MessageBox(u"ffmpeg è uscito con status error:\n%s" % (
                    #string.join(err)), "ERROR", wx.ICON_ERROR)
            wx.MessageBox("FFmpeg came out with error status !\n"
                "If you have not set any log to save, view the log file: "
                "%s/.videomass/%s"
                % (DIRNAME, self.logname), "ERROR", wx.ICON_ERROR)
            self.log_for_user()
            self.reset_error()
            self.parent.Enable()
            self.Destroy()
        else:
            self.barProg.SetValue(self.count)
            self.labelState.SetLabel('Process Number %s of %s' % (
                                      self.count +1, self.maxer))
    def post_proc(self):
        """
        if all previus process not send error, execute ultimate work 
        """
        wx.MessageBox("The batch process is completed successfully !", 
                      "Sucess !")
        self.log_for_user()
        os.chdir(self.outputdir)
        rename_file(".%s.%s"%(self.extinput, self.extoutput),
                              ".%s" % (self.extoutput))
    def reset_error(self):
        """
        Reset global variable of the ffmpeg status error at original 
        value because prevent the next program block
        """
        global STATUS_ERROR
        STATUS_ERROR = None
        
    def reset_status(self):
        """
        Reset global variable where the user have push the button stop 
        at original value because prevent the next program block
        """
        global CHANGE_STATUS
        CHANGE_STATUS = None

    #---------------------EVENT -----------------#
    def on_cancel(self, event):
        """
        The user change idea and it want stop the process
        """
        global CHANGE_STATUS
        CHANGE_STATUS = 1
        event.Skip()
        
    def log_for_user(self):
        """
        if user want file log in any directory
        """
        if self.makelog == 'true': 
            copy_restore("%s/.videomass/%s" % (DIRNAME, self.logname),
                            "%s/%s" % (self.path_log, self.logname))
        
########################################################################


class PopupDialog(wx.Dialog):
    """ 
    A popup dialog for temporary user messages 
    
    http://wxpython-users.1045709.n5.nabble.com/Wait-dialog-td4943451.html


    Create a popup telling the user about the load (needed for large files)
    # loadDlg = PopupDialog(None, ("Videomass - Loading..."), 
                                ("\nAttendi....\nSto eseguendo un processo .\n")
                                )
    # 
    # loadDlg
        # ...
    Destroy the Load Popup Dialog
    # loadDlg.Destroy()
    TODO: with button abort for process terminate .
    
    other method for popup:
        # BusyInfo = wx.ProgressDialog('Videomass - stand. conv.', 
                                    ' ...Attendi, sto processando'
                                    )
        # BusyInfo.Destroy()
    """
    def __init__(self, parent, title, msg):
        # Create a dialog
        wx.Dialog.__init__(self, parent, -1, title, size=(350, 150), 
                            style=wx.CAPTION)
        # Add sizers
        box = wx.BoxSizer(wx.VERTICAL)
        box2 = wx.BoxSizer(wx.HORIZONTAL)
        # Add an Info graphic
        bitmap = wx.EmptyBitmap(32, 32)
        bitmap = wx.ArtProvider_GetBitmap(wx.ART_INFORMATION, 
                                        wx.ART_MESSAGE_BOX, (32, 32)
                                        )
        graphic = wx.StaticBitmap(self, -1, bitmap)
        box2.Add(graphic, 0, wx.EXPAND | wx.ALIGN_CENTER | wx.ALL, 10)
        # Add the message
        message = wx.StaticText(self, -1, msg)
        box2.Add(message, 0, wx.EXPAND | wx.ALIGN_CENTER 
                                    | wx.ALIGN_CENTER_VERTICAL | wx.ALL, 10
                                    )
        box.Add(box2, 0, wx.EXPAND)
        # Handle layout
        self.SetAutoLayout(True)
        self.SetSizer(box)
        self.Fit()
        self.Layout()
        self.CentreOnScreen()
        # Display the Dialog
        self.Show()
        # Make sure the screen gets fully drawn before continuing.
        wx.Yield()
