#!/usr/bin/env python
# -*- coding: UTF-8 -*-

#########################################################
# Name: infoprog.py
# Porpose: about videomass dialog
# resource: <http://zetcode.com/wxpython/dialogs/>
# Copyright: (c) 2015-2017 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (00) 10/Nov/2017
#########################################################

import wx
from vdms_utils.msg_info import current_release, descriptions_release

cr = current_release()
Name = cr[1]
Version = cr[2]
Release = cr[3]
Copyright = cr[4]
Website = cr[5]
Author = cr[6]
Mail = cr[7]
Comment = cr[8]

dr = descriptions_release()
Short_Dscrp = dr[0]
Long_Dscrp = dr[1]
Short_Lic = dr[2]
Long_Lic = dr[3]

def info(videomass_icon):
        """
        It's a predefined template to create a dialogue on 
        the program information
        
        """
        info = wx.AboutDialogInfo()

        info.SetIcon(wx.Icon(videomass_icon, wx.BITMAP_TYPE_PNG))
        
        info.SetName(Name)
        
        info.SetVersion(Version)
        
        info.SetDescription(u'%s\n%s' %(Short_Dscrp,Long_Dscrp))
        
        info.SetCopyright("%s %s" %(Copyright, Author))
        
        info.SetWebSite(Website)
        
        info.SetLicence(Long_Lic)
        
        info.AddDeveloper(u"\n\n%s \n"
                        u"%s\n"
                        u"%s\n\n"
                        u"%s\n" %(Author,Mail,Website,Comment))
                        
        #info.AddDocWriter(u"La documentazione ufficiale é parziale e ancora\n"
                        #u"in fase di sviluppo, si prega di contattare l'autore\n"
                        #u"per ulteriori delucidazioni.")
                        
        #info.AddArtist(u'Gianluca Pernigotto powered by wx.Python')
        
        #info.AddTranslator(u"Al momento, il supporto alle traduzioni manca del\n"
                        #u"tutto, l'unica lingua presente nel programma é\n"
                        #u"quella italiana a cura di: Gianluca Pernigotto\n\n"

                        #u"Se siete interessati a tradurre il programma\n"
                        #u"in altre lingue, contattare l'autore.")
                    
        wx.AboutBox(info)
        #event.Skip()
