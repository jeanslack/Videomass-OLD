#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Thursday Aug 22 21:37:01 2013
#
#########################################################
# Name: audiodialogs.py
# Porpose: module for choice and settings audio
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev: 30/04/2015
#########################################################

"""
this class is used by video_sci.py, audioadd.py, audiograbber.py, 
audiograbseq.py
"""
import wx

class AudioSettings(wx.Dialog):
    """
    Provides a dialog for audio settings of bit-rate, sample-rate 
    and audio-channels.
    """
    def __init__(self, parent, audio_type, title):
        """
        The audio_type argument is a type string of the audio format.
        """
        wx.Dialog.__init__(self, parent, -1, title=title, 
                           style=wx.DEFAULT_DIALOG_STYLE
                           )
        datastr = TypeAudioParameters(audio_type)# instance for audio param
        # calling attribute:
        self.sample_rate = datastr.sample_rate
        self.channels = datastr.channels
        self.bitrate = datastr.bitrate
        samplerate_list = []
        channel_list = []
        bitrate_list = []

        if self.bitrate == None:
            self.bitrate = {0:('no bitrate available',"")}

        for a in self.sample_rate.values():
            samplerate_list.append(a[0])
        for a in self.channels.values():
            channel_list.append(a[0])
        for a in self.bitrate.values():
            bitrate_list.append(a[0])

        self.rdb_bitrate = wx.RadioBox(self, wx.ID_ANY, (
            "Audio Bit-Rate"), choices=bitrate_list, majorDimension=0, 
             style=wx.RA_SPECIFY_ROWS)

        self.rdb_channels = wx.RadioBox(self, wx.ID_ANY, (
            "Audio Channel"), choices=channel_list, majorDimension=0, 
             style=wx.RA_SPECIFY_ROWS)

        self.rdb_sample_r = wx.RadioBox(self, wx.ID_ANY, (
            "Audio Rate (sample rate)"), choices=samplerate_list, 
             majorDimension=0, style=wx.RA_SPECIFY_ROWS) 
        
        if self.rdb_bitrate.GetStringSelection() == 'no bitrate available':
            self.rdb_bitrate.Disable()

        self.button_2 = wx.Button(self, wx.ID_CANCEL, "")
        self.panel_1 = wx.Panel(self, wx.ID_ANY)
        self.button_1 = wx.Button(self, wx.ID_OK, "")
        
        """----------------------Properties----------------------"""
        self.rdb_bitrate.SetSelection(0)
        self.rdb_bitrate.SetToolTipString(datastr.bitrate_tooltip)
        self.rdb_channels.SetSelection(0)
        self.rdb_channels.SetToolTipString(datastr.channel_tooltip)
        self.rdb_sample_r.SetSelection(0)
        self.rdb_sample_r.SetToolTipString(datastr.sample_rate_tooltip)

        """----------------------Build layout----------------------"""
        grid_sizer_1 = wx.FlexGridSizer(2, 3, 0, 0)
        grid_sizer_1.Add(self.rdb_bitrate, 0, wx.ALL, 15)
        grid_sizer_1.Add(self.rdb_channels, 0, wx.ALL, 15)
        grid_sizer_1.Add(self.rdb_sample_r, 0, wx.ALL, 15)
        grid_sizer_1.Add(self.button_2, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | 
                                    wx.ALIGN_CENTER_VERTICAL | wx.SHAPED, 15
                                    )
        grid_sizer_1.Add(self.panel_1, 1, wx.EXPAND, 0)
        
        grid_sizer_1.Add(self.button_1, 0, wx.ALL | wx.EXPAND | 
                    wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 15
                    )
        self.SetSizer(grid_sizer_1)
        grid_sizer_1.Fit(self)
        self.Layout()
        
        """--------------------Binders (EVT)----------------------"""
        self.Bind(wx.EVT_BUTTON, self.on_cancel, self.button_2)
        self.Bind(wx.EVT_BUTTON, self.on_apply, self.button_1)


    def on_cancel(self, event):
        """
        if you enable self.Destroy(), it delete from memory all data event and
        no return correctly. It has the right behavior if not used here, because 
        it is called in the main frame. 
        
        Event.Skip(), work correctly here.
        
        """
        #self.Destroy()
        event.Skip()
        
        
    def on_apply(self, event):
        """
        if you enable self.Destroy(), it delete from memory all data event and
        no return correctly. It has the right behavior if not used here, because 
        it is called in the main frame. 
        
        Event.Skip(), work correctly here. Sometimes needs to disable it for
        needs to maintain the view of the window (for exemple).
        
        """
        self.GetValue()
        #self.Destroy()
        event.Skip()
        
        
    def GetValue(self):
        """
        This method return values via the interface GetValue()
        """
        
        for k,v in self.channels.items():
            if self.rdb_channels.GetStringSelection() in v[0]:
                channel = v[1]
                
        for k,v in self.sample_rate.items():
            if self.rdb_sample_r.GetStringSelection() in v[0]:
                samplerate = v[1]
                
        for k,v in self.bitrate.items():
            if self.rdb_bitrate.GetStringSelection() in v[0]:
                bitrate = v[1]

        return channel, samplerate, bitrate
    

class TypeAudioParameters(object):
    """
    The class provides an adequate representation of the different 
    audio parameters that need to be encoded or decoded by FFmpeg. 
    These parameters relate to some aspects of quality and technical: 
    audio bitrates, sample rate, and audio channels.
    Also include messages tooltip of channels, sample rate and bit rate.
    """
    channel_tooltip = (u"""\
The audio channels are represented by monophonic, 
stereophonic and quadraphonic techniques reproduction.
For some codecs can only assign an audio stream
monaural or stereo, for others even polyphonic.
If you are insecure you set to "Default", will be copied
source values.\
""")
    sample_rate_tooltip = (u"""\
The audio Rate (or sample-rate) is the sound sampling 
frequency and is measured in hertz. The higher the frequency, 
plus the audio signal will be true to the sound source, but 
the file will increase its size. For normal playback with 
audio CD set a sample rate of 44100kHz. If you are insecure 
you set to "Default", will be copied source values.\
""")
    bitrate_tooltip = (u"""\
The audio bitrate affects on file compression
and on the quality of listening. The higher
the value and more higher quality.\
""")
    def __init__(self, audio_format):
        """
        Accept a type string object representing the name of 
        the audio format. For now there is support for these 
        audio formats: wav, flac, alac, aac, ac3, ogg, mp3.
        Each attribute is instantiable with this class and 
        returns the data string object or the object dictionary 
        for others attribute.
        """
        self.sample_rate = None
        self.channels = None
        self.bitrate = None
        
        if audio_format in ('wav','flac','alac'):
            self.wav_param()
        elif audio_format == 'aac':
            self.aac_param()
        elif audio_format == 'ac3':
            self.ac3_param()
        elif audio_format == 'ogg':
            self.ogg_param()
        elif audio_format == 'mp3':
            self.mp3_param()
            
    def wav_param(self):
        """
        """
        self.sample_rate = {
            0:("Default", ''), 
            1:("44100  (Audio-CD, Mp3, compressed movies","-ar 44100"), 
            2:("48000  (Hi quality, not suitable for audio-CD","-ar 48000"), 
            3:("88200  (Audio productions, engineering)","-ar 88200"), 
            4:("96000  (Hi definition DVD)","-ar 96000")
            }
        
        self.channels = {0:("Default",""), 1:("Mono","-ac 1"), 
                         2:("Stereo","-ac 2")
                         }
    def aac_param(self):
        """
        """
        self.sample_rate = {
            0:("Default", ''), 
            1:("44100  (Audio-CD, Mp3, compressed movies","-ar 44100"), 
            2:("48000  (Hi quality, not suitable for audio-CD","-ar 48000"), 
            3:("88200  (Audio productions, engineering)","-ar 88200"), 
            4:("96000  (Hi definition DVD)","-ar 96000")
            }
        self.channels = {0:("Default",""), 
                         1:("Mono","-ac 1"), 
                         2:("Stereo","-ac 2"), 
                         3:("MultiChannel 5.1", "-ac 6")
                         }
        self.bitrate = {
            0:("Default",""), 
            1:("128   >  VBR 128 kbit/s (low quality)", "-ab 128k"), 
            2:("160   >  VBR 160 kbit/s (medium/low quality)", "-ab 160k"), 
            3:("192   >  VBR 192 kbit/s (medium qulity)", "-ab 192k"), 
            4:("260   >  VBR 260 kbit/s (good quality)", "-ab 260k"), 
            5:("320   >  CBR 320 kbit/s (very good quality)", "-ab 320k")}
        
    def ac3_param(self):
        """
        """
        self.sample_rate = {
            0:("Default", ''), 
            1:("44100  (Audio-CD, Mp3, compressed movies","-ar 44100"), 
            2:("48000  (Hi quality, not suitable for audio-CD","-ar 48000"), 
            3:("88200  (Audio productions, engineering)","-ar 88200"), 
            4:("96000  (Hi definition DVD)","-ar 96000")
            }
        self.channels = {0:("Default",""), 
                         1:("Mono","-ac 1"), 
                         2:("Stereo","-ac 2"), 
                         3:("MultiChannel 5.1", "-ac 6")
                         }
        self.bitrate = {0:("Default",""), 
                        1:("192  >  (low quality)", "-ab 192k"), 
                        2:("224", "-ab 224k"), 
                        3:("256", "-ab 256k"), 
                        4:("320", "-ab 320k"), 
                        5:("384", "-ab 384k"), 
                        6:("448", "-ab 448k"), 
                        7:("512", "-ab 512k"), 
                        8:("576", "-ab 576k"), 
                        9:("640  >  (very good quality)", "-ab 640k")
                        }
    def ogg_param(self):
        """
        """
        self.sample_rate = {
            0:("Default", ""), 
            1:("44100  (Audio-CD, Mp3, compressed movies","-ar 44100"), 
            2:("48000  (Hi quality, not suitable for audio-CD","-ar 48000"), 
            3:("88200  (Audio productions, engineering)","-ar 88200"), 
            4:("96000  (Hi definition DVD)","-ar 96000")
            }
        
        self.channels = {0:("Default",""), 1:("Mono","-ac 1"), 
                         2:("Stereo","-ac 2")
                         }
        self.bitrate = {0:("Default", ""), 
                        1:("1   >  VBR 64 kbit/s", "-aq 1"), 
                        2:("2    >  VBR 92 kbit/s", "-aq 2"), 
                        3:("3   >  VBR 128 kbit/s", "-aq 3"), 
                        4:("4    >  VBR 160 kbit/s", "-aq 4"), 
                        5:("5   >  VBR 175 kbit/s", "-aq 5"), 
                        6:("6    >  VBR 192 kbit/s", "-aq 6"), 
                        7:("7   >  VBR 220 kbit/s", "-aq 7"), 
                        8:("8    >  VBR 260 kbit/s", "-aq 8"), 
                        9:("9   >  VBR 320 kbit/s", "-aq 9"), 
                        10:("10   >  VBR 500 kbit/s", "-aq 10")
                        }
    def mp3_param(self):
        """
        """
        self.sample_rate = {
            0:("Default", ""), 
            1:("44100  (Audio-CD, Mp3, compressed movies","-ar 44100"), 
            2:("48000  (Hi quality, not suitable for audio-CD","-ar 48000"), 
            3:("88200  (Audio productions, engineering)","-ar 88200"), 
            4:("96000  (Hi definition DVD)","-ar 96000")
            }
        
        self.channels = {0:("Default",""), 1:("Mono","-ac 1"), 
                         2:("Stereo","-ac 2")
                         }
        self.bitrate = {
            0:("Default", ""), 
            1:("128   >  VBR 128 kbit/s (low quality)", "-ab 128k"), 
            2:("160   >  VBR 160 kbit/s (medium/low quality)", "-ab 160k"), 
            3:("192   >  VBR 192 kbit/s (medium qulity)", "-ab 192k"), 
            4:("260   >  VBR 260 kbit/s (good quality)", "-ab 260k"), 
            5:("320   >  CBR 320 kbit/s (very good quality)", "-ab 320k")
                        }
