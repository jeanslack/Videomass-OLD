#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Friday Aug 23 10:37:47 2013
#
#########################################################
# Name: epilogue.py
# Porpose: show final param before start process
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (05) 12/03/2015
#########################################################

import wx

class Formula(wx.Dialog):
    """
    Class for show epilogue dialog before run process (if all ok 
    (validations?)). It not return usable values.
    """
    def __init__(self, parent, formula, diction):
        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        
        panel = wx.Panel(self, wx.ID_ANY, style=wx.RAISED_BORDER | wx.TAB_TRAVERSAL)
        label1 = wx.StaticText(panel, wx.ID_ANY, formula)
        label2 = wx.StaticText(panel, wx.ID_ANY, diction)
        self.button_1 = wx.Button(panel, wx.ID_CANCEL, "")
        self.button_2 = wx.Button(panel, wx.ID_OK, "")
        
        #----------------------Properties----------------------#
        self.SetTitle("Epilogue")
        label2.SetForegroundColour(wx.Colour(255, 106, 249))
        panel.SetBackgroundColour(wx.Colour(212, 255, 249))
        
        #---------------------- Layout ----------------------#
        s1 = wx.BoxSizer(wx.VERTICAL)
        gr_s1 = wx.FlexGridSizer(2, 2, 0, 0)
        gr_s1.Add(label1, 0, wx.ALL, 15)
        gr_s1.Add(label2, 0, wx.ALL, 15)
        gr_s1.Add(self.button_1, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        gr_s1.Add(self.button_2, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        panel.SetSizer(gr_s1)
        s1.Add(panel, 1, wx.ALL | wx.EXPAND, 15)
        self.SetSizer(s1)
        s1.Fit(self)
        self.Layout()
        
        #----------------------Binders (EVT)--------------------#
        self.Bind(wx.EVT_BUTTON, self.on_cancel, self.button_1)
        self.Bind(wx.EVT_BUTTON, self.on_ok, self.button_2)
        
        #----------------------Event handler (callback)----------------------#
    def on_cancel(self, event):  
        #self.Destroy()
        event.Skip()
        
    def on_ok(self, event):  
        #self.Destroy()
        event.Skip() 
