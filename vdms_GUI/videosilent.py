#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Mon Aug  5 08:51:18 2013
#
#########################################################
# Name: videosilent.py
# Porpose: delete audio track on a video
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (04) 20/07/2014
# Rev (05) 16/03/2015
#########################################################


import wx
import os
import webbrowser
from os_processing import proc_std
from epilogue import Formula

dirname = os.path.expanduser('~/') # /home/user/

# create need list, dict:
diction_command = {"ext_input":"", "InputDir":"" ,"OutputDir":"", 
                "Pathname":"", "Dirname":"", "Filename":"",
                   }

class VideoSilent(wx.Dialog):
    """
    Remove all audio stream from a movie
    """
    def __init__(self, parent, helping, ffmpeg_link, threads, ffmpeg_log, 
                command_log, path_log, loglevel_type):
        
        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        
        self.helping = helping
        self.ffmpeg_link = ffmpeg_link
        self.threads = threads
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.parent = parent
        self.parent.Hide()
        
        btn_open = wx.Button(self, wx.ID_OPEN, "")
        self.text_open = wx.TextCtrl(self, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                    )
        self.button_save = wx.Button(self, wx.ID_SAVE, "")
        self.text_save = wx.TextCtrl(self, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                    )
        siz1_staticbox = wx.StaticBox(self, wx.ID_ANY, ("Import/Export Media Movie"))
        btn_close = wx.Button(self, wx.ID_CANCEL, "Close")
        btn_help = wx.Button(self, wx.ID_HELP, "")
        self.button_ok = wx.Button(self, wx.ID_OK, "Start")
        
        diction_command["InputDir"] = ""
        diction_command["OutputDir"] = ""
        self.button_save.Disable()

        #----------------------Properties-----------------------------------#

        self.SetTitle("Audio Removal from video stream")
        self.text_open.SetMinSize((230, 21))
        self.text_open.SetToolTipString("Imported path-name (read only)")
        self.text_save.SetMinSize((230, 21))
        self.text_save.SetToolTipString("Exported path-name (read only)")
        self.button_ok.SetBackgroundColour(wx.Colour(180, 214, 255))
        self.button_ok.SetForegroundColour(wx.Colour(130, 51, 27))

        #----------------------Handle Layout--------------------------------#
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_2 = wx.GridSizer(1, 3, 0, 0)
        siz1_staticbox.Lower()
        sizer_2 = wx.StaticBoxSizer(siz1_staticbox, wx.VERTICAL)
        grid_sizer_1 = wx.FlexGridSizer(2, 2, 0, 0)
        grid_sizer_1.Add(btn_open, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                            | wx.ALIGN_CENTER_VERTICAL, 15
                                            )
        grid_sizer_1.Add(self.text_open, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                                | wx.ALIGN_CENTER_VERTICAL, 
                                                15
                                                )
        grid_sizer_1.Add(self.button_save, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                                    | wx.ALIGN_CENTER_VERTICAL, 
                                                    15
                                                    )
        grid_sizer_1.Add(self.text_save, 0, wx.ALIGN_CENTER_HORIZONTAL 
                                        | wx.ALIGN_CENTER_VERTICAL, 15
                                        )
        sizer_2.Add(grid_sizer_1, 1, wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 1, wx.ALL | wx.EXPAND, 15)
        
        grid_sizer_2.Add(btn_close, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                            | wx.ALIGN_CENTER_VERTICAL, 15
                                            )
        grid_sizer_2.Add(btn_help, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                            | wx.ALIGN_CENTER_VERTICAL, 15
                                            )
        grid_sizer_2.Add(self.button_ok, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL 
                                                | wx.ALIGN_CENTER_VERTICAL, 
                                                15
                                                )
        sizer_1.Add(grid_sizer_2, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()

        #----------------------Binding (EVT)--------------------------------#
        self.Bind(wx.EVT_BUTTON, self.on_open, btn_open)
        self.Bind(wx.EVT_TEXT, self.enter_open, self.text_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.button_save)
        self.Bind(wx.EVT_TEXT, self.enter_save, self.text_save)
        self.Bind(wx.EVT_BUTTON, self.on_close, btn_close)
        self.Bind(wx.EVT_BUTTON, self.on_help, btn_help)
        self.Bind(wx.EVT_BUTTON, self.on_ok, self.button_ok)

    #----------------------Event handler (callback)-------------------------#
    def on_open(self, event):
        dialfile = wx.FileDialog(self, "Importing Movie", ".", "", "*.*", 
                                 wx.FD_OPEN | wx.FD_CHANGE_DIR | 
                                 wx.FD_FILE_MUST_EXIST
                                 )
        if dialfile.ShowModal() == wx.ID_OK:
            self.text_open.SetValue(""), self.button_save.Enable() 
            self.text_save.SetValue("")
            self.text_open.AppendText(dialfile.GetPath())
            fileName, fileExtension = os.path.splitext(dialfile.GetPath())
            diction_command["ext_input"] = fileExtension.split(".")[-1]
            dialfile.Destroy()
            
    #------------------------------------------------------------------#
    def enter_open(self, event):
        diction_command["InputDir"] = self.text_open.GetValue()
        
    #------------------------------------------------------------------#
    def on_save(self, event):
        
        ext = diction_command["ext_input"]
        
        dialsave = wx.FileDialog(self, "Export Final Silenced Movie", "", "", 
                                 "%s files (*.%s)|*.%s" % (ext,ext,ext), 
                                 wx.SAVE | wx.OVERWRITE_PROMPT | wx.FD_CHANGE_DIR
                                    )
        if dialsave.ShowModal() == wx.ID_OK:
            path = dialsave.GetPath()
            
            if path.endswith(ext) != True:
                path = "%s.%s" % (path, ext)
            self.text_save.SetValue("")
            self.text_save.AppendText(path)
            dialsave.Destroy()
            
    #------------------------------------------------------------------#
    def enter_save(self, event):
        diction_command["OutputDir"] = self.text_save.GetValue()
        
    #------------------------------------------------------------------#
    def on_close(self, event):
        #self.Destroy()
        self.parent.Show()
        event.Skip()

    #------------------------------------------------------------------#
    def on_help(self, event):
        #wx.MessageBox("Spiacente, ma l'help contestuale é ancora in fase di "
                    #"sviluppato ...Abbiate pazienza !")
        webbrowser.open('%s/11-Cancellare_audio.html' % self.helping)
        
    #------------------------------------------------------------------#
    def on_ok(self, event):
        
        logname = 'Videomass_VideoSilent.log'

        if self.ffmpeg_log == 'true':
            report = '-report'
        else:
            report = ''

        if self.text_open.GetValue() == "": # inputdir vuoto
            wx.MessageBoxg("No files selected for conversion !", "Warning", 
                             wx.OK | wx.ICON_EXCLAMATION, self
                             )
        elif self.text_save.GetValue() == "": # outputdir vuoto
            wx.MessageBox("No name/path to save !", "Warning", wx.OK | 
                          wx.ICON_EXCLAMATION, self
                          )
        else:
            command = ("%s %s -i  '%s' -loglevel %s -an -vcodec "
                    "copy %s -y '%s'" % (self.ffmpeg_link, report, 
                            self.text_open.GetValue(), self.loglevel_type, 
                            self.threads, self.text_save.GetValue())
                        )
            formula = (u"FORMULATIONS:\n\nConversion Mode:\
                        \nVideo Format:\nInput dir/file:\nOutput dir/file:"
                        )
            dictions = ("\n\n%s\nvideo copy (%s)\n%s\n%s" % (
                        "Single file only", diction_command["ext_input"], 
                        diction_command["InputDir"],diction_command["OutputDir"])
                        )
            ending = Formula(self, formula, dictions)
            
            if ending.ShowModal() == wx.ID_OK:
                dial = proc_std(self, 'Videomass - Run single process', command,
                                self.path_log, logname, self.command_log,
                                )
                
