#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Friday Aug 23 10:37:47 2013
#
#########################################################
# Name: audiograbber.py
# Porpose: dialog for extract audio format on any video
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (05)  24/08/2014
# Rev (06)  14/01/2015
# Rev (07)  12/03/2015
# Rev (08)  30/04/2015 (improved management audio with ffprobe)
#########################################################

"""
Extract audio from video. Also, extract in batch mode and can store preset
for quick use .
"""

import wx 
import os
import string
import webbrowser
from os_processing import proc_std, proc_batch_thread, ProgressDialog,\
                          control_errors, FFProbe
from epilogue import Formula
import audiodialogs

dirname = os.path.expanduser('~/') # /home/user/

# create need list, dict:
array = []
diction_command = {"Batch":"single file conversion", "ext_input":"", 
                "testing":"", "InputDir":"" ,"OutputDir":"","Audioext":"ogg", 
                "Audio":"", "AudioCodec":"-acodec libvorbis", 
                "AudioChannel":"-ac 2", "AudioRate":"-ar 44100", 
                "AudioBitrate":"-aq 3"
                   }

class AudioGrabber(wx.Dialog):
    """
    This class show dialog grabbing audio from video and is called from menu 
    bar tools on videomass .
    """
    def __init__(self, parent, helping, ffmpeg_link, ffprobe_link, 
                 threads, ffmpeg_log, command_log, path_log, loglevel_type, 
                 loglevel_batch
                 ):
        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        
        self.helping = helping
        self.ffmpeg_link = ffmpeg_link
        self.ffprobe_link = ffmpeg_link
        self.threads = threads
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.loglevel_batch = loglevel_batch
        self.parent = parent
        self.parent.Hide()
        
        self.notebook_1 = wx.Notebook(self, wx.ID_ANY, style=0)
        self.notebook_1_pane_1 = wx.Panel(self.notebook_1, wx.ID_ANY)
        # * self.btn_preset = wx.Button(self.notebook_1_pane_1, wx.ID_ADD, "")
        self.static_line_1 = wx.StaticLine(self.notebook_1_pane_1, wx.ID_ANY)
        # * self.sizer_1_staticbox = wx.StaticBox(self.notebook_1_pane_1, wx.ID_ANY,("Memorizza come preset"))
        self.checkbox_batch = wx.CheckBox(self.notebook_1_pane_1, wx.ID_ANY, ("Batch-Mode"))
        self.checkbox_batch.SetValue(False) # setto in modo spento
        self.checkbox_test = wx.CheckBox(self.notebook_1_pane_1, wx.ID_ANY, ("Testing"))
        self.checkbox_test.SetValue(False) # setto in modo spento
        self.label_secondi = wx.StaticText(self.notebook_1_pane_1, wx.ID_ANY, ("Time Sec.:"))
        self.spin_ctrl_test = wx.SpinCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
                            "", min=10, max=10000, style=wx.TE_PROCESS_ENTER
                            )
        self.sizer_6_staticbox = wx.StaticBox(self.notebook_1_pane_1, 
                                                        wx.ID_ANY, ("Options")
                                                        )
        self.btn_open = wx.Button(self.notebook_1_pane_1, wx.ID_OPEN, "")
        self.text_open = wx.TextCtrl(self.notebook_1_pane_1, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                    )
        self.btn_save = wx.Button(self.notebook_1_pane_1, wx.ID_SAVE, "")
        self.text_save = wx.TextCtrl(self.notebook_1_pane_1, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY)
        self.sizer_4_staticbox = wx.StaticBox(self.notebook_1_pane_1, 
                                    wx.ID_ANY, ("Import/Export Media")
                                    )
        self.notebook_1_pane_2 = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.radiobox_audio = wx.RadioBox(self.notebook_1_pane_2, wx.ID_ANY, 
        ("Convert the audio stream to extract:    "), choices=[
                                  ("Default"),
                                  ("Wav (Raw, No_MultiChannel)"), 
                                  ("Flac (Lossless, No_MultiChannel)"),
                                  ("Aac (Lossy, MultiChannel)"), 
                                  ("Alac (Lossless, m4v, No_MultiChannel)"),
                                  ("Ac3 (Lossy, MultiChannel)"), 
                                  ("Ogg (Lossy, No_MultiChannel)"), 
                                  ("Mp3 (Lossy, No_MultiChannel)"), 
                                  ("Copy audio source")], majorDimension=0,
                                  style=wx.RA_SPECIFY_ROWS
                                  )
        btn_close = wx.Button(self, wx.ID_CANCEL, "Close")
        btn_help = wx.Button(self, wx.ID_HELP, "")
        self.btn_ok = wx.Button(self, wx.ID_OK, "Start")
        
        self.spin_ctrl_test.Disable()
        self.spin_ctrl_test.SetValue(10)
        self.label_secondi.Disable()
        self.btn_save.Disable(), self.text_save.Disable()
        self.spin_ctrl_test.Disable(), self.label_secondi.Disable()
        diction_command["InputDir"] = ""
        diction_command["OutputDir"] = ""
        diction_command["testing"] = ""
        
        if 'batch' in array:
            diction_command ["Batch"] = "single file conversion"
            array.remove('batch')

        #----------------------Set Properties----------------------#
        self.SetTitle("Audio Streams Grabbing Tool")
        #self.btn_preset.SetMinSize((160, 27))
        #self.btn_preset.SetBackgroundColour(wx.Colour(253, 255, 180))
        #self.btn_preset.SetToolTipString("Memorizza come preset da usare con #"arraymass")
        self.checkbox_batch.SetToolTipString("Apply the process to a files "
                                             "group with same extensions"
                                             )
        self.checkbox_test.SetToolTipString("Run test conversion by setting "
                                            "the time in seconds"
                                            )
        self.spin_ctrl_test.SetMinSize((80, 21))
        self.spin_ctrl_test.SetToolTipString("Enter time in seconds")
        self.btn_open.SetToolTipString("Importing file or directory")
        self.text_open.SetMinSize((230, 21))
        self.text_open.SetToolTipString("Imported path-name (read only, not editable)")
        self.btn_save.SetToolTipString("Exporting files")
        self.text_save.SetMinSize((230, 21))
        self.text_save.SetToolTipString("Exported path-name (read only, not editable)")
        self.radiobox_audio.SetSelection(0)
        self.notebook_1.SetMinSize((420, 420))
        self.btn_ok.Disable()
        self.btn_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
        self.btn_ok.SetForegroundColour(wx.Colour(34, 31, 30))

        #----------------------Layout----------------------#
        grid_sizer_5 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_3 = wx.GridSizer(1, 3, 0, 0)
        sizer_5 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_2 = wx.GridSizer(2, 1, 0, 0)
        self.sizer_4_staticbox.Lower()
        sizer_4 = wx.StaticBoxSizer(self.sizer_4_staticbox, wx.VERTICAL)
        grid_sizer_1 = wx.FlexGridSizer(2, 2, 0, 0)
        grid_sizer_4 = wx.GridSizer(2, 1, 0, 0)
        self.sizer_6_staticbox.Lower()
        sizer_6 = wx.StaticBoxSizer(self.sizer_6_staticbox, wx.VERTICAL)
        grid_sizer_6 = wx.GridSizer(1, 4, 0, 0)
        ############ if uncomment this, enable button of the presets (*)
        # * self.sizer_1_staticbox.Lower()
        # * sizer_1 = wx.StaticBoxSizer(self.sizer_1_staticbox, wx.VERTICAL)
        # * sizer_1.Add(self.btn_preset, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        # * grid_sizer_4.Add(sizer_1, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_4.Add(self.static_line_1, 0, wx.EXPAND, 0)
        grid_sizer_6.Add(self.checkbox_batch, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_6.Add(self.checkbox_test, 0, wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_6.Add(self.label_secondi, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_6.Add(self.spin_ctrl_test, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_6.Add(grid_sizer_6, 1, wx.EXPAND, 0)
        grid_sizer_4.Add(sizer_6, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_2.Add(grid_sizer_4, 1, wx.EXPAND, 0)
        grid_sizer_1.Add(self.btn_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_1.Add(self.text_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_1.Add(self.btn_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_1.Add(self.text_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_4.Add(grid_sizer_1, 1, wx.EXPAND, 0)
        grid_sizer_2.Add(sizer_4, 1, wx.ALL | wx.EXPAND, 15)
        self.notebook_1_pane_1.SetSizer(grid_sizer_2)
        sizer_5.Add(self.radiobox_audio, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 30)
        self.notebook_1_pane_2.SetSizer(sizer_5)
        self.notebook_1.AddPage(self.notebook_1_pane_1, ("Files Management"))
        self.notebook_1.AddPage(self.notebook_1_pane_2, ("Audio Codec Selection"))
        grid_sizer_5.Add(self.notebook_1, 1, wx.ALL | wx.EXPAND, 5)
        grid_sizer_3.Add(btn_close, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer_3.Add(btn_help, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer_3.Add(self.btn_ok, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer_5.Add(grid_sizer_3, 1, wx.ALL | wx.EXPAND, 15)
        self.SetSizer(grid_sizer_5)
        grid_sizer_5.Fit(self)
        self.Layout()
        
        self.radiobox_audio.Disable()

        #----------------------Binding (EVT)----------------------#

        # * self.Bind(wx.EVT_BUTTON, self.on_preset, self.btn_preset)
        self.Bind(wx.EVT_CHECKBOX, self.on_batch, self.checkbox_batch)
        self.Bind(wx.EVT_CHECKBOX, self.on_test, self.checkbox_test)
        self.Bind(wx.EVT_SPINCTRL, self.enter_secondi, self.spin_ctrl_test)
        self.Bind(wx.EVT_BUTTON, self.on_open, self.btn_open)
        self.Bind(wx.EVT_TEXT, self.enter_open, self.text_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.btn_save)
        self.Bind(wx.EVT_TEXT, self.enter_save, self.text_save)
        self.Bind(wx.EVT_RADIOBOX, self.choice_audio, self.radiobox_audio)
        self.Bind(wx.EVT_BUTTON, self.on_close, btn_close)
        self.Bind(wx.EVT_BUTTON, self.on_help, btn_help)
        self.Bind(wx.EVT_BUTTON, self.on_ok, self.btn_ok)
    
    def enable_button(self):
        self.btn_ok.Enable()
        self.btn_ok.SetBackgroundColour(wx.Colour(180, 214, 255))
        self.btn_ok.SetForegroundColour(wx.Colour(130, 51, 27))
        
    def disable_button(self):
        self.btn_ok.Disable()
        self.btn_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
        self.btn_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        self.text_open.SetValue(""),self.text_save.SetValue("")
        self.btn_save.Disable(), self.text_save.Disable()
        
    #----------------------Event handler (callback)----------------------#
    def on_batch(self, event):
        
        if self.checkbox_batch.GetValue() is True:
            dlg = wx.TextEntryDialog(self, (
                              "Write here a files group extension to convert\n"
                              "with batch mode. Do not include punctuation\n"
                              "marks '.' and blanks")
                                      )
            if dlg.ShowModal() == wx.ID_OK:
                self.sizer_4_staticbox.SetLabel('Import/Export Directory Media')
                self.btn_open.SetBackgroundColour(wx.Colour(255, 196, 143))
                self.btn_save.SetBackgroundColour(wx.Colour(255, 196, 143))
                diction_command["ext_input"] = "%s" % dlg.GetValue()
                dlg.Destroy()
                self.disable_button()
                diction_command["Batch"] = "group files directory"
                array.append('batch')
            else:
                self.checkbox_batch.SetValue(False)
                diction_command["ext_input"] = ""
                diction_command["Batch"] = "single file conversion"
                
                if 'batch' in array:
                    array.remove('batch')
                    
        elif self.checkbox_batch.GetValue() is False:
            self.sizer_4_staticbox.SetLabel("Import/Export Media")
            self.btn_open.SetBackgroundColour(wx.Colour(223, 220, 217))
            self.btn_save.SetBackgroundColour(wx.Colour(223, 220, 217))
            diction_command["ext_input"] = ""
            diction_command["Batch"] = "single file conversion"
            self.disable_button()
            
            if 'batch' in array:
                array.remove('batch')
                
    #------------------------------------------------------------------#
    def on_test(self, event):
        
        if self.checkbox_test.GetValue() is True:
            self.spin_ctrl_test.Enable(), self.label_secondi.Enable()
            diction_command["testing"] = "-t %s" % (self.spin_ctrl_test.GetValue())
            
        elif self.checkbox_test.GetValue() is False:
            self.spin_ctrl_test.Disable(), self.spin_ctrl_test.SetValue(10)
            self.label_secondi.Disable()
            diction_command["testing"] = ""
            
    #------------------------------------------------------------------#
    def enter_secondi(self, event):
        
        diction_command["testing"] = "-t %s" % (self.spin_ctrl_test.GetValue())
        
    #------------------------------------------------------------------#
    def on_open(self, event):
        
        dialfile = wx.FileDialog(self, "Importing movie", ".", "", "*.*", 
                                 wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        dialdir = wx.DirDialog(self, "Import movie directory for batch process")
        
        if self.checkbox_batch.GetValue() is False:
            if dialfile.ShowModal() == wx.ID_OK:
                self.disable_button()
                self.text_open.AppendText(dialfile.GetPath())
                self.btn_save.Enable(), self.text_save.Enable()
                self.radiobox_audio.Enable()
                diction_command["AudioCodec"] = "-acodec libvorbis"
                diction_command["Audioext"] = "ogg"
                diction_command["AudioBitrate"] = "-aq 3"
                diction_command["AudioChannel"] = "-ac 2"
                diction_command["AudioRate"] = "-ar 44100"
                self.radiobox_audio.SetSelection(0)
                dialfile.Destroy()
        else:
            if dialdir.ShowModal() == wx.ID_OK:
                    self.disable_button()
                    self.text_open.AppendText(dialdir.GetPath())
                    self.btn_save.Enable(), self.text_save.Enable()
                    self.radiobox_audio.Enable()
                    diction_command["AudioCodec"] = "-acodec libvorbis"
                    diction_command["Audioext"] = "ogg"
                    diction_command["AudioBitrate"] = "-aq 3"
                    diction_command["AudioChannel"] = "-ac 2"
                    diction_command["AudioRate"] = "-ar 44100"
                    self.radiobox_audio.SetSelection(0)
                    dialdir.Destroy()
                    
    #------------------------------------------------------------------#
    def enter_open(self, event):
        
        diction_command["InputDir"] = self.text_open.GetValue()
        
    #------------------------------------------------------------------#
    def on_save(self, event):
        
        ext = diction_command["Audioext"]
        
        if self.checkbox_batch.GetValue() is False:
            dialsave = wx.FileDialog(self, "Exporting Audio Track", "", "", 
                                     "%s files (*.%s)|*.%s" % (ext,ext,ext), 
                                     wx.SAVE | wx.OVERWRITE_PROMPT | 
                                     wx.FD_CHANGE_DIR
                                    )
            if dialsave.ShowModal() == wx.ID_OK:
                path = dialsave.GetPath()
                if path.endswith(ext) != True:
                    path = "%s.%s" % (path, ext)
                self.text_save.SetValue("")
                self.text_save.AppendText(path)
                self.enable_button()
                dialsave.Destroy()
        else:
            dialdir = wx.DirDialog(self, "Where do you want to save ?")
            
            if dialdir.ShowModal() == wx.ID_OK:
                    self.text_save.SetValue("")
                    self.text_save.AppendText(dialdir.GetPath())
                    self.enable_button()
                    dialdir.Destroy()
                    
    #------------------------------------------------------------------#
    def enter_save(self, event):
        
        diction_command["OutputDir"] = self.text_save.GetValue()
        
    #------------------------------------------------------------------#
    def choice_audio(self, event):
        
        audio = self.radiobox_audio.GetStringSelection()
        File = self.text_save.GetValue().strip(".%s" % diction_command["Audioext"])
        
        if audio == "Default":
            #self.text_save.SetValue("")
            diction_command["AudioCodec"] = "-acodec libvorbis"
            diction_command["Audioext"] = "ogg"
            diction_command["AudioBitrate"] = "-aq 3"
            diction_command["AudioChannel"] = "-ac 2"
            diction_command["AudioRate"] = "-ar 44100"

        elif audio == "Wav (Raw, No_MultiChannel)":
            self.audio_("wav", "Audio WAV Codec Parameters", 
                        "-acodec pcm_s16le", "wav")
        elif audio == "Flac (Lossless, No_MultiChannel)":
            self.audio_("flac", "Audio FLAC Codec Parameters", 
                        "-acodec flac", "flac")

        elif audio == "Aac (Lossy, MultiChannel)":
            self.audio_("aac", "Audio AAC Codec Parameters", 
                        "-acodec libfaac", "aac")

        elif audio == "Alac (Lossless, m4v, No_MultiChannel)":
            self.audio_("alac", "Audio ALAC (m4a) Codec Parameters", 
                        "-acodec alac", "m4a")

        elif audio == "Ac3 (Lossy, MultiChannel)":
            self.audio_("ac3", "Audio AC3 Codec Parameters", 
                        "-acodec ac3", "ac3")

        elif audio == "Ogg (Lossy, No_MultiChannel)":
            self.audio_("ogg", "Audio OGG Codec Parameters", 
                        "-acodec libvorbis", "ogg")

        elif audio == "Mp3 (Lossy, No_MultiChannel)": 
            self.audio_("mp3", "Audio MP3 Codec Parameters", 
                        "-acodec libmp3lame", "mp3")

        elif audio == "Copy audio source":
            # create instance FFProbe class:
            metadata = FFProbe(self.text_open.GetValue(), self.ffprobe_link) 
            # first execute a control for errors:
            if metadata.ERROR():
                wx.MessageBox("[FFprobe] Error:  %s" % (metadata.error), "ERROR",
                wx.ICON_ERROR, self)
                self.radiobox_audio.SetSelection(0)
                return
            # Proceed to the method call:
            audio_list = metadata.get_audio_codec_name()
            # ...and proceed to checkout:
            if audio_list == None:
                wx.MessageBox("There is no audio stream into imported video", 
                              'Warning', wx.ICON_EXCLAMATION, self)
                self.radiobox_audio.SetSelection(0)
                return

            elif len(audio_list) > 1:
                dlg = wx.SingleChoiceDialog(self, 
                        "The imported video contains multiple audio\n" 
                        "streams. Selecting which stream you want\n"
                        "to export between these:", "Audio codec to export",
                        audio_list, wx.CHOICEDLG_STYLE
                                            )
                if dlg.ShowModal() == wx.ID_OK:
                    if dlg.GetStringSelection() in audio_list:
                        codec_name = '%s' % dlg.GetStringSelection()
                        diction_command["Audioext"] = "%s" % codec_name
                        diction_command["AudioCodec"] = "-acodec copy"
                        diction_command["AudioRate"] = ""
                        diction_command["AudioChannel"] = ""
                        diction_command["AudioBitrate"] = ""
                    else:
                        self.radiobox_audio.SetSelection(0)
                else:
                    self.radiobox_audio.SetSelection(0)
                dlg.Destroy()
            else:
                diction_command["Audioext"] = "%s" % (audio_list[0])
                diction_command["AudioCodec"] = "-acodec copy"
                diction_command["AudioRate"] = ""
                diction_command["AudioChannel"] = ""
                diction_command["AudioBitrate"] = ""
                    
        if 'batch' not in array:
            self.text_save.SetValue("%s.%s" % (File, diction_command["Audioext"]))
            
    #--------------------------------------------------#
    def audio_(self, audio_type, title, codec, ext):
        """
        Run a dialogs for audio Choice parameters
        """
        audiodialog = audiodialogs.AudioSettings(self,audio_type,title)
        retcode = audiodialog.ShowModal()
        
        if retcode == wx.ID_OK:
            #self.text_save.SetValue("")
            data = audiodialog.GetValue()
            diction_command["AudioCodec"] = codec
            diction_command["Audioext"] = ext
            diction_command["AudioChannel"] = data[0]
            diction_command["AudioRate"] = data[1]
            diction_command["AudioBitrate"] = data[2]
        else:
            self.radiobox_audio.SetSelection(0)
        
        audiodialog.Destroy()
    #------------------------------------------------------------------#
    def on_close(self, event):
        self.parent.Show()
        #self.Destroy() # con ID_OK e ID_CANCEL non serve Destroy()
        event.Skip() # need if destroy from parent

    #------------------------------------------------------------------#
    def on_help(self, event):
        
        #wx.MessageBox("L'help contestuale é ancora in fase di sviluppo .")
        webbrowser.open('%s/09-Grabbing_audio.html' % self.helping)
        
    #------------------------------------------------------------------#
    def on_ok(self, event):
        
        logname = 'Videomass_AudioGrab.log'
        
        if self.ffmpeg_log == 'true':
            report = '-report'
        else:
            report = ''
            
        if 'batch' in array:
            command = ("-loglevel %s -vn %s %s %s %s %s %s" % (
                            self.loglevel_batch,
                            diction_command["AudioCodec"], 
                            diction_command["AudioBitrate"], 
                            diction_command["AudioChannel"], 
                            diction_command["AudioRate"], 
                            self.threads, diction_command["testing"])
                            )
            valupdate = self.update_dict()
            ending = Formula(self, valupdate[0], valupdate[1])
            
            if ending.ShowModal() == wx.ID_OK:
                
                status = control_errors(self.text_open.GetValue(),
                                        diction_command["ext_input"],
                                        command, logname
                                        )
                if  status[0] == 'no_error':
            
                    proc_batch_thread(status[2], self.text_open.GetValue(), 
                                    diction_command["Audioext"], 
                                    self.text_save.GetValue(), command, 
                                    None, None, logname, self.ffmpeg_link
                                    )
                    dial = ProgressDialog(self, 'Videomass Batch Process', 
                                    status[1], diction_command["ext_input"],
                                    diction_command["Audioext"],
                                    self.text_save.GetValue(), self.path_log, 
                                    self.command_log, logname
                                        )
        else:
            command = ("%s %s -i '%s' -loglevel %s -vn %s %s %s %s "
                    "%s %s -y '%s'" % (self.ffmpeg_link, report, 
                        self.text_open.GetValue(), self.loglevel_type, 
                        diction_command["AudioCodec"], 
                        diction_command["AudioBitrate"], 
                        diction_command["AudioChannel"], 
                        diction_command["AudioRate"], 
                        self.threads, diction_command["testing"], 
                        self.text_save.GetValue())
                            )
            valupdate = self.update_dict()
            ending = Formula(self, valupdate[0], valupdate[1])
            
            if ending.ShowModal() == wx.ID_OK:
                dial = proc_std(self, 'Videomass - Run single process', 
                                command, self.path_log, logname, 
                                self.command_log
                                )
    #------------------------------------------------------------------#
    def update_dict(self):
        """
        This method is required for update all diction_command
        dictionary values before send at epilogue
        """
        formula = (u"FORMULATIONS:\n\nConversion Mode:\nOutput Audio Format:\
                   \nAudio codec:\nAudio channel:\nAudio rate:\
                   \nAudio bit-rate:\nInput Path:\nOutput Path:\
                   \nFile Extension Group:\nTime Video Test (second):")
        dictions = ("\n\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s" % (
                   diction_command["Batch"],diction_command["Audioext"], 
                   diction_command["AudioCodec"],diction_command["AudioChannel"], 
                   diction_command["AudioRate"],diction_command["AudioBitrate"], 
                   diction_command["InputDir"],diction_command["OutputDir"], 
                   diction_command["ext_input"],diction_command["testing"]))
         
        return formula, dictions
