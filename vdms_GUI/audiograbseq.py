#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Wed Jul 31 23:58:02 2013 (last modified: Friday Aug 23 11:17 2013)
#
#
#########################################################
# Name: audiograbseq.py
# Porpose: user can choice grab audio parts/sequences on a video
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2015 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev (05) 20/07/2014
# Rev (06) 12/03/2015
# Rev (07)  30/04/2015 (improved management audio with ffprobe)
#########################################################

"""
this function is locate in menu bar/editing of videomass and is called:
'cut audio clip from a video'
"""

import wx
import os
import webbrowser
#import wx.lib.masked as masked # not work on macOSX
from os_processing import proc_std, FFProbe
from epilogue import Formula
import audiodialogs

dirname = os.path.expanduser('~/') # /home/user/

# create need list, dict:
diction_command = {"ext_input":"", "InputDir":"" ,"OutputDir":"", 
                "Pathname":"", "Dirname":"", "Filename":"", "Audioext":"ogg", 
                "Audio":"", "AudioCodec":"-acodec libvorbis", 
                "AudioChannel":"-ac 2", "AudioRate":"-ar 44100", 
                "AudioBitrate":"-aq 3", "cutstart":"", "cutlengh":""}

class AudiograbSeq(wx.Dialog):
    """
    This class show dialog for grabbing audio from video but extract an 
    sequence choice.
    """
    def __init__(self, parent, helping, ffmpeg_link, ffprobe_link,
                 ffmpeg_log, command_log, path_log, loglevel_type):

        wx.Dialog.__init__(self, parent, -1, style=wx.DEFAULT_DIALOG_STYLE)
        
        self.helping = helping
        self.ffmpeg_link = ffmpeg_link
        self.ffprobe_link = ffmpeg_link
        self.ffmpeg_log = ffmpeg_log
        self.command_log = command_log
        self.path_log = path_log
        self.loglevel_type = loglevel_type
        self.parent = parent
        self.parent.Hide()
        
        self.notebook_1 = wx.Notebook(self, wx.ID_ANY, style=0)
        self.notebook_1_pane_1 = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.start_hour_ctrl = wx.SpinCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
                                 "0", min=0, max=23, style=wx.TE_PROCESS_ENTER
                                           )
        self.start_minute_ctrl = wx.SpinCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
                                 "0", min=0, max=59, style=wx.TE_PROCESS_ENTER
                                           )
        self.start_second_ctrl = wx.SpinCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
                                 "0", min=0, max=59, style=wx.TE_PROCESS_ENTER
                                 )
        self.sizer_1_staticbox = wx.StaticBox(self.notebook_1_pane_1, wx.ID_ANY, 
                                                    ("Start Time (h:m:s)")
                                                )
        self.stop_hour_ctrl = wx.SpinCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
                                 "0", min=0, max=23, style=wx.TE_PROCESS_ENTER
                                           )
        self.stop_minute_ctrl = wx.SpinCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
                                 "0", min=0, max=59, style=wx.TE_PROCESS_ENTER
                                            )
        self.stop_second_ctrl = wx.SpinCtrl(self.notebook_1_pane_1, wx.ID_ANY, 
                                 "0", min=0, max=59, style=wx.TE_PROCESS_ENTER
                                             )
        self.sizer_6_staticbox = wx.StaticBox(self.notebook_1_pane_1, wx.ID_ANY, 
                                              ("Time Progress (h:m:s)")
                                              )
        self.btn_open = wx.Button(self.notebook_1_pane_1, wx.ID_OPEN, "")
        self.text_open = wx.TextCtrl(self.notebook_1_pane_1, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                    )
        self.btn_save = wx.Button(self.notebook_1_pane_1, wx.ID_SAVE, "")
        self.text_save = wx.TextCtrl(self.notebook_1_pane_1, wx.ID_ANY, "", 
                                    style=wx.TE_PROCESS_ENTER | wx.TE_READONLY
                                    )
        self.sizer_4_staticbox = wx.StaticBox(self.notebook_1_pane_1, 
                                                wx.ID_ANY, ("Import/Export Media Movie")
                                                )
        self.notebook_1_pane_2 = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.radiobox_audio = wx.RadioBox(self.notebook_1_pane_2, wx.ID_ANY, 
        ("Convert the sequence audio stream to extract:"), choices=[
                                  ("Default"),
                                  ("Wav (Raw, No_MultiChannel)"), 
                                  ("Flac (Lossless, No_MultiChannel)"),
                                  ("Aac (Lossy, MultiChannel)"), 
                                  ("Alac (Lossless, m4v, No_MultiChannel)"),
                                  ("Ac3 (Lossy, MultiChannel)"), 
                                  ("Ogg (Lossy, No_MultiChannel)"), 
                                  ("Mp3 (Lossy, No_MultiChannel)"), 
                                  ("Copy audio source")], majorDimension=0,
                                  style=wx.RA_SPECIFY_ROWS
                                  )
        btn_close = wx.Button(self, wx.ID_CANCEL, "Close")
        btn_help = wx.Button(self, wx.ID_HELP, "")
        self.btn_ok = wx.Button(self, wx.ID_OK, "Start")
        
        self.btn_save.Disable(), self.text_save.Disable()
        
        """ Reset dict values on memory """
        diction_command["InputDir"] = ""
        diction_command["OutputDir"] = ""
        
        #----------------------Properties ----------------------#
        self.SetTitle("Extract Audio Sequence")
        self.start_hour_ctrl.SetMinSize((45, 20))
        self.start_minute_ctrl.SetMinSize((45, 20))
        self.start_second_ctrl.SetMinSize((45, 20))
        self.start_hour_ctrl.SetToolTipString("Start sequence movie"
                                        "(time hours format)")
        self.start_minute_ctrl.SetToolTipString("Start sequence movie"
                                        "(time minute format)")
        self.start_second_ctrl.SetToolTipString("Start sequence movie"
                                        "(time second format)")
        self.stop_hour_ctrl.SetMinSize((45, 20))
        self.stop_minute_ctrl.SetMinSize((45, 20))
        self.stop_second_ctrl.SetMinSize((45, 20))
        self.stop_hour_ctrl.SetToolTipString("Sets the total amount of time "
                                        "progress, starting from the start "
                                        "time of extraction  "
                                              )
        self.stop_second_ctrl.SetToolTipString("Sets the total amount of time "
                                             "progress, starting from the start "
                                             "time of extraction  "
                                               )
        self.stop_minute_ctrl.SetToolTipString("Sets the total amount of time "
                                             "progress, starting from the start "
                                             "time of extraction  "
                                               )
        self.text_open.SetToolTipString("Imported path-name (read only)")
        self.text_save.SetToolTipString("Exported path-name (read only)")
        self.text_open.SetMinSize((230, 21))
        self.text_save.SetMinSize((230, 21))
        self.radiobox_audio.SetSelection(0)
        self.btn_ok.Disable()
        self.btn_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
        self.btn_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        
        self.init_hour = '00'
        self.init_minute = '00'
        self.init_second = '00'
        self.cut_hour = '00'
        self.cut_minute = '00'
        self.cut_second = '00'
        
        #----------------------Layout----------------------#
        grid_sizer_5 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_3 = wx.GridSizer(1, 3, 0, 0)
        sizer_2 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_6 = wx.GridSizer(3, 1, 0, 0)
        sizer_5 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_2 = wx.GridSizer(2, 1, 0, 0)
        self.sizer_4_staticbox.Lower()
        sizer_4 = wx.StaticBoxSizer(self.sizer_4_staticbox, wx.VERTICAL)
        grid_sizer_1 = wx.FlexGridSizer(2, 2, 0, 0)
        grid_sizer_4 = wx.GridSizer(2, 1, 0, 0)
        self.sizer_6_staticbox.Lower()
        sizer_6 = wx.StaticBoxSizer(self.sizer_6_staticbox, wx.HORIZONTAL)
        self.sizer_1_staticbox.Lower()
        sizer_1 = wx.StaticBoxSizer(self.sizer_1_staticbox, wx.HORIZONTAL)
        #sizer_1.Add(self.time_start, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5) # timer
        sizer_1.Add(self.start_hour_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_1.Add(self.start_minute_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_1.Add(self.start_second_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_4.Add(sizer_1, 1, wx.ALL | wx.EXPAND, 15)
        #sizer_6.Add(self.time_lengh, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_6.Add(self.stop_hour_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_6.Add(self.stop_minute_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_6.Add(self.stop_second_ctrl, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)
        grid_sizer_4.Add(sizer_6, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_2.Add(grid_sizer_4, 1, wx.EXPAND, 0)
        grid_sizer_1.Add(self.btn_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_1.Add(self.text_open, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_1.Add(self.btn_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        grid_sizer_1.Add(self.text_save, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_4.Add(grid_sizer_1, 1, wx.EXPAND, 0)
        grid_sizer_2.Add(sizer_4, 1, wx.ALL | wx.EXPAND, 15)
        self.notebook_1_pane_1.SetSizer(grid_sizer_2)
        sizer_5.Add(self.radiobox_audio, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 30)
        self.notebook_1_pane_2.SetSizer(sizer_5)
        sizer_2.Add(grid_sizer_6, 1, wx.EXPAND, 0)
        self.notebook_1.AddPage(self.notebook_1_pane_1, ("Time Sequence Management"))
        self.notebook_1.AddPage(self.notebook_1_pane_2, ("Audio Codec Selection"))
        grid_sizer_5.Add(self.notebook_1, 1, wx.ALL | wx.EXPAND, 5)
        grid_sizer_3.Add(btn_close, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer_3.Add(btn_help, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer_3.Add(self.btn_ok, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)
        grid_sizer_5.Add(grid_sizer_3, 1, wx.ALL | wx.EXPAND, 15)
        self.SetSizer(grid_sizer_5)
        grid_sizer_5.Fit(self)
        self.Layout()
        
        self.radiobox_audio.Disable()
        
        #----------------------Binding (EVT)----------------------#
        self.Bind(wx.EVT_SPINCTRL, self.start_hour, self.start_hour_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.start_minute, self.start_minute_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.start_second, self.start_second_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.stop_hour, self.stop_hour_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.stop_minute, self.stop_minute_ctrl)
        self.Bind(wx.EVT_SPINCTRL, self.stop_second, self.stop_second_ctrl)
        #self.Bind(wx.EVT_SPINCTRL, self.on_time_init, self.time_start)
        #self.Bind(wx.EVT_SPINCTRL, self.on_time_finish, self.time_lengh)
        self.Bind(wx.EVT_BUTTON, self.on_open, self.btn_open)
        self.Bind(wx.EVT_TEXT, self.enter_open, self.text_open)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.btn_save)
        self.Bind(wx.EVT_TEXT, self.enter_save, self.text_save)
        self.Bind(wx.EVT_RADIOBOX, self.choice_audio, self.radiobox_audio)
        self.Bind(wx.EVT_BUTTON, self.on_close, btn_close)
        self.Bind(wx.EVT_BUTTON, self.on_help, btn_help)
        self.Bind(wx.EVT_BUTTON, self.on_ok, self.btn_ok)

    def enable_button(self):
        self.btn_ok.Enable()
        self.btn_ok.SetBackgroundColour(wx.Colour(180, 214, 255))
        self.btn_ok.SetForegroundColour(wx.Colour(130, 51, 27))
        
    def disable_button(self):
        self.btn_ok.Disable()
        self.btn_ok.SetBackgroundColour(wx.Colour(223, 220, 217))
        self.btn_ok.SetForegroundColour(wx.Colour(34, 31, 30))
        self.text_open.SetValue(""),self.text_save.SetValue("")
        self.btn_save.Disable(), self.text_save.Disable()
        
    #----------------------Event handler (callback)----------------------#
    def start_hour(self, event):
        self.init_hour = '%s' % self.start_hour_ctrl.GetValue()
        if len(self.init_hour) == 1:
            self.init_hour = '0%s' % self.start_hour_ctrl.GetValue()
            
    #------------------------------------------------------------------#
    def start_minute(self, event):
        self.init_minute = '%s' % self.start_minute_ctrl.GetValue()
        if len(self.init_minute) == 1:
            self.init_minute = '0%s' % self.start_minute_ctrl.GetValue()
            
    #------------------------------------------------------------------#
    def start_second(self, event):
        self.init_second = '%s' % self.start_second_ctrl.GetValue()
        if len(self.init_second) == 1:
            self.init_second = '0%s' % self.start_second_ctrl.GetValue()
            
    #------------------------------------------------------------------#
    def stop_hour(self, event):
        self.cut_hour = '%s' % self.stop_hour_ctrl.GetValue()
        if len(self.cut_hour) == 1:
            self.cut_hour = '0%s' % self.stop_hour_ctrl.GetValue()
            
    #------------------------------------------------------------------#
    def stop_minute(self, event):
        self.cut_minute = '%s' % self.stop_minute_ctrl.GetValue()
        if len(self.cut_minute) == 1:
            self.cut_minute = '0%s' % self.stop_minute_ctrl.GetValue()
            
    #------------------------------------------------------------------#
    def stop_second(self, event):
        self.cut_second = '%s' % self.stop_second_ctrl.GetValue()
        if len(self.cut_second) == 1:
            self.cut_second = '0%s' % self.stop_second_ctrl.GetValue()
            
    #------------------------------------------------------------------#
    def on_open(self, event):
        
        dialfile = wx.FileDialog(self, "Importing Movie", ".", "", "*.*", 
                                 wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if dialfile.ShowModal() == wx.ID_OK:
            self.disable_button()
            self.text_open.AppendText(dialfile.GetPath())
            self.btn_save.Enable(), self.text_save.Enable()
            self.radiobox_audio.Enable()
            diction_command["AudioCodec"] = "-acodec libvorbis"
            diction_command["Audioext"] = "ogg"
            diction_command["AudioBitrate"] = "-aq 3"
            diction_command["AudioChannel"] = "-ac 2"
            diction_command["AudioRate"] = "-ar 44100"
            self.radiobox_audio.SetSelection(0)
            dialfile.Destroy()

    #------------------------------------------------------------------#
    def enter_open(self, event):
        
        diction_command["InputDir"] = self.text_open.GetValue()
        
    #------------------------------------------------------------------#
    def on_save(self, event):
        
        ext = diction_command["Audioext"]

        dialsave = wx.FileDialog(self, "Exporting Audio Track", "", "", 
                                 "%s files (*.%s)|*.%s" % (ext,ext,ext), 
                                 wx.SAVE | wx.OVERWRITE_PROMPT | 
                                 wx.FD_CHANGE_DIR
                                    )
        if dialsave.ShowModal() == wx.ID_OK:
            path = dialsave.GetPath()
            
            if path.endswith(ext) != True:
                path = "%s.%s" % (path, ext)
            self.text_save.SetValue("")
            self.text_save.AppendText(path)
            self.enable_button()
            dialsave.Destroy()
            
    #------------------------------------------------------------------#
    def enter_save(self, event):
        diction_command["OutputDir"] = self.text_save.GetValue()
        
    #------------------------------------------------------------------#
    def choice_audio(self, event):
        
        audio = self.radiobox_audio.GetStringSelection()
        File = self.text_save.GetValue().strip(".%s" % diction_command["Audioext"])
        
        if audio == "Default":
            #self.text_save.SetValue("")
            diction_command["AudioCodec"] = "-acodec libvorbis"
            diction_command["Audioext"] = "ogg"
            diction_command["AudioBitrate"] = "-aq 3"
            diction_command["AudioChannel"] = "-ac 2"
            diction_command["AudioRate"] = "-ar 44100"

        elif audio == "Wav (Raw, No_MultiChannel)":
            self.audio_("wav", "Audio WAV Codec Parameters", 
                        "-acodec pcm_s16le", "wav")
        elif audio == "Flac (Lossless, No_MultiChannel)":
            self.audio_("flac", "Audio FLAC Codec Parameters", 
                        "-acodec flac", "flac")

        elif audio == "Aac (Lossy, MultiChannel)":
            self.audio_("aac", "Audio AAC Codec Parameters", 
                        "-acodec libfaac", "aac")

        elif audio == "Alac (Lossless, m4v, No_MultiChannel)":
            self.audio_("alac", "Audio ALAC (m4a) Codec Parameters", 
                        "-acodec alac", "m4a")

        elif audio == "Ac3 (Lossy, MultiChannel)":
            self.audio_("ac3", "Audio AC3 Codec Parameters", 
                        "-acodec ac3", "ac3")

        elif audio == "Ogg (Lossy, No_MultiChannel)":
            self.audio_("ogg", "Audio OGG Codec Parameters", 
                        "-acodec libvorbis", "ogg")

        elif audio == "Mp3 (Lossy, No_MultiChannel)": 
            self.audio_("mp3", "Audio MP3 Codec Parameters", 
                        "-acodec libmp3lame", "mp3")

        elif audio == "Copy audio source":
            # create instance FFProbe class:
            metadata = FFProbe(self.text_open.GetValue(), self.ffprobe_link) 
            # first execute a control for errors:
            if metadata.ERROR():
                wx.MessageBox("[FFprobe] Error:  %s" % (metadata.error), "ERROR",
                wx.ICON_ERROR, self)
                self.radiobox_audio.SetSelection(0)
                return
            # Proceed to the method call:
            audio_list = metadata.get_audio_codec_name()
            # ...and proceed to checkout:
            if audio_list == None:
                wx.MessageBox("There is no audio stream into imported video ", 
                              'Warning', wx.ICON_EXCLAMATION, self)
                self.radiobox_audio.SetSelection(0)
                return

            elif len(audio_list) > 1:
                dlg = wx.SingleChoiceDialog(self, 
                        "The imported video contains multiple audio\n" 
                        "streams. Selecting which stream you want\n"
                        "to export between these:", "Audio codec to export",
                        audio_list, wx.CHOICEDLG_STYLE
                                            )
                if dlg.ShowModal() == wx.ID_OK:
                    if dlg.GetStringSelection() in audio_list:
                        codec_name = '%s' % dlg.GetStringSelection()
                        diction_command["Audioext"] = "%s" % codec_name
                        diction_command["AudioCodec"] = "-acodec copy"
                        diction_command["AudioRate"] = ""
                        diction_command["AudioChannel"] = ""
                        diction_command["AudioBitrate"] = ""
                    else:
                        self.radiobox_audio.SetSelection(0)
                else:
                    self.radiobox_audio.SetSelection(0)
                dlg.Destroy()
            else:
                diction_command["Audioext"] = "%s" % (audio_list[0])
                diction_command["AudioCodec"] = "-acodec copy"
                diction_command["AudioRate"] = ""
                diction_command["AudioChannel"] = ""
                diction_command["AudioBitrate"] = ""
        
        self.text_save.SetValue("%s.%s" % (File, diction_command["Audioext"]))
        
    #--------------------------------------------------#
    def audio_(self, audio_type, title, codec, ext):
        """
        Run a dialogs for audio Choice parameters
        """
        audiodialog = audiodialogs.AudioSettings(self,audio_type,title)
        retcode = audiodialog.ShowModal()
        
        if retcode == wx.ID_OK:
            #self.text_save.SetValue("")
            data = audiodialog.GetValue()
            diction_command["AudioCodec"] = codec
            diction_command["Audioext"] = ext
            diction_command["AudioChannel"] = data[0]
            diction_command["AudioRate"] = data[1]
            diction_command["AudioBitrate"] = data[2]
        else:
            self.radiobox_audio.SetSelection(0)

    #------------------------------------------------------------------#
    def on_close(self, event):
        self.parent.Show()
        event.Skip() # need if destroy from parent

    #------------------------------------------------------------------#
    def on_help(self, event):
        
        #wx.MessageBox("Spiacente, ma l'help contestuale é ancora in fase di "
                    #"sviluppato ...Abbiate pazienza !")
            
        webbrowser.open('%s/13-Tagliare_audio.html' % self.helping)

    #------------------------------------------------------------------#
    def on_ok(self, event):
        
        logname = 'Videomass_AudioGrabSequence.log'
        
        if self.ffmpeg_log == 'true':
            report = '-report'
        else:
            report = ''

        if self.text_open.GetValue() == "": # inputdir vuoto
            wx.MessageBox("No files selected for conversion !","Warning", 
                          wx.OK | wx.ICON_EXCLAMATION, self
                          )
        elif self.text_save.GetValue() == "": # outputdir vuoto
            wx.MessageBox("No path-name to be saved !", "Warning", wx.OK | 
                             wx.ICON_EXCLAMATION, self
                             )
        else:
            diction_command["cutstart"] = "%s:%s:%s" % (self.init_hour, self.init_minute, 
                                                    self.init_second
                                                    )
            diction_command["cutlengh"] = "%s:%s:%s" % (self.cut_hour, self.cut_minute, 
                                                    self.cut_second
                                                    )
            command = ("%s %s -i '%s' -ss %s -t %s -loglevel %s -vn "
                    "%s %s %s %s -y '%s'" % (self.ffmpeg_link, report, 
                    self.text_open.GetValue(), diction_command["cutstart"], 
                    diction_command["cutlengh"], self.loglevel_type, 
                    diction_command["AudioCodec"], diction_command["AudioBitrate"], 
                    diction_command["AudioChannel"], diction_command["AudioRate"], 
                    self.text_save.GetValue())
                            )
            formula = (u"FORMULATIONS:\n\nConversion Mode:\nStart cutting:\
                    \nduration sequence:\nOutput Audio Format:\nAudio codec:\
                    \nAudio channel:\nAudio rate:\nAudio bit-rate:\
                    \nInput Path:\nOutput Path:\nFile Extension Group:"
                    )
            dictions = ("\n\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s" % (
                    "Solo file singolo",diction_command["cutstart"], 
                    diction_command["cutlengh"],diction_command["Audioext"], 
                    diction_command["AudioCodec"],diction_command["AudioChannel"], 
                    diction_command["AudioRate"],diction_command["AudioBitrate"], 
                    diction_command["InputDir"],diction_command["OutputDir"], 
                    diction_command["ext_input"])
                         )
            ending = Formula(self,formula,dictions)
            
            if ending.ShowModal() == wx.ID_OK:
                dial = proc_std(self, 'Videomass - Run single process',command,
                                self.path_log,logname,self.command_log,)
